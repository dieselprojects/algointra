package com.dieselProjects.configurationCluster;

import java.io.Serializable;

import com.dieselProjects.supportCluster.Types.MACDStrategyType;
import com.tictactec.ta.lib.MAType;

public class DynMACDCfg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Indicator parameters:
	// ---------------------
	public int macdSimTicksParam;
	public int macdFastParam;
	public int macdSlowParam;
	public int macdSignalParam;
	public int macdTickBeforeParam;
	public double macdIndicaitonMargain;
	public MAType macdMATypeFastParam;
	public MAType macdMATypeSlowParam;
	public MAType macdMATypeSignalParam;

	// Indicator strategy:
	// -------------------
	public MACDStrategyType macdStrategyType = MACDStrategyType.CUSTOM2;
	public int macdNumberOfElements = 3;
	public double macdHistChangeBuy = -0.03; // -((4e-6) * Math.pow(avgValue, 2)
												// + 0.00275 * avgValue +
												// 0.08)/40
	public double macdHistChangeSell = 0.01; // (((4e-6) * Math.pow(avgValue, 2)
												// + 0.00275 * avgValue + 0.08)
												// * 0.75)/40;

	public DynMACDCfg(MACDCfg defaultMACDCfg) {

		this.macdSimTicksParam = defaultMACDCfg.macdSimTicksParam;
		this.macdFastParam = defaultMACDCfg.macdFastParam;
		this.macdSlowParam = defaultMACDCfg.macdSlowParam;
		this.macdSignalParam = defaultMACDCfg.macdSignalParam;
		this.macdTickBeforeParam = defaultMACDCfg.macdTickBeforeParam;
		this.macdIndicaitonMargain = defaultMACDCfg.macdIndicaitonMargain;
		this.macdMATypeFastParam = defaultMACDCfg.macdMATypeFastParam;
		this.macdMATypeSlowParam = defaultMACDCfg.macdMATypeSlowParam;
		this.macdMATypeSignalParam = defaultMACDCfg.macdMATypeSignalParam;
		this.macdStrategyType = defaultMACDCfg.macdStrategyType;
		this.macdNumberOfElements = defaultMACDCfg.macdNumberOfElements;
		this.macdHistChangeBuy = defaultMACDCfg.macdHistChangeBuy;
		this.macdHistChangeSell = defaultMACDCfg.macdHistChangeSell;

	}

}
