package com.dieselProjects.configurationCluster;

import java.io.Serializable;

import com.tictactec.ta.lib.MAType;

public class DynSRSICfg  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int srsiSimTicksParam = 30;
	public int srsiFastKParam = 6;
	public int srsiFastDParam = 14;
	public int srsiBuyParam = 40;
	public int srsiSellParam = 60;
	public MAType srsiMATypeDParam = MAType.Ema;

	public DynSRSICfg(SRSICfg defaultSRSICfg) {
		this.srsiSimTicksParam = defaultSRSICfg.srsiSimTicksParam;
		this.srsiFastKParam = defaultSRSICfg.srsiFastKParam;
		this.srsiFastDParam = defaultSRSICfg.srsiFastDParam;
		this.srsiBuyParam = defaultSRSICfg.srsiBuyParam;
		this.srsiSellParam = defaultSRSICfg.srsiSellParam;
		this.srsiMATypeDParam = defaultSRSICfg.srsiMATypeDParam;
	}

}
