package com.dieselProjects.configurationCluster;

import java.util.LinkedList;

import com.dieselProjects.supportCluster.Types.BBandsStrategyType;
import com.tictactec.ta.lib.MAType;

public class BBandsCfg {

	private static final BBandsCfg BBandsCfg = new BBandsCfg();

	public static BBandsCfg getInstance() {
		return BBandsCfg;
	}
	
	// Indicator parameters:
	// ---------------------
	public MAType bbandsMATypeParam = MAType.Ema;
	public int bbandsTimePeriod = 20;
	public int bbandsPeriodUp = 2;
	public int bbandsPeriodDown = 2;

	// Indicator strategy:
	// -------------------
	public BBandsStrategyType bbandsStrategyType = BBandsStrategyType.CUSTOM2;
	public int bbandsLookBack = 5;
	public double bbandsMargin = 0.2;
	
	public void print() {
		System.out.println("MACD configuration:");
		System.out.println("\tbbandsMATypeParam   : " + bbandsMATypeParam);
		System.out.println("-------------------------------");
	}

	public LinkedList<String> getValues() {

		LinkedList<String> list = new LinkedList<String>();

		list.addLast("MACD configuration:");
		list.addLast("bbandsMATypeFastParam   : " + bbandsMATypeParam);
		list.addLast("-------------------------------");
		return list;
	}

}
