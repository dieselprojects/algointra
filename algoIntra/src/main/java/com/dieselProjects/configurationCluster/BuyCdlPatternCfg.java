package com.dieselProjects.configurationCluster;

import com.dieselProjects.supportCluster.Types.CDLIndicators;

public class BuyCdlPatternCfg {

	private static final BuyCdlPatternCfg buyCdlPatternCfg = new BuyCdlPatternCfg();

	public static BuyCdlPatternCfg getInstance() {
		return buyCdlPatternCfg;
	}

	// number of ticks to check for patterns
	public int numOfPastTicks = 3;

	// does all patterns need to be found in the above period in order to
	// trigger the node
	public boolean requireAllPatterns = false;

	// array of relevant candlestick patterns
	public boolean[] patterns = new boolean[CDLIndicators.getSize()];

	
	
	public BuyCdlPatternCfg() {
		// default buy patterns:
		// doji star (16)
		patterns[5] = true;
		patterns[15] = true;
		patterns[16] = true;

		// hammer (23)
		patterns[23] = true;
	}

}
