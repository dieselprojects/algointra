package com.dieselProjects.configurationCluster;

import java.io.Serializable;

public class HoldSellCfg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final HoldSellCfg holdSellCfg = new HoldSellCfg();
	
	public static HoldSellCfg getInstance(){
		return holdSellCfg;
	}
	
	public double holdSellUntil = 1.005; // 0.5%
	
}
