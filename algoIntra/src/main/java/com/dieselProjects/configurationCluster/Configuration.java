package com.dieselProjects.configurationCluster;

import java.util.LinkedList;

import com.dieselProjects.supportCluster.Types.BrokerType;
import com.dieselProjects.supportCluster.Types.ConfirmationType;
import com.dieselProjects.supportCluster.Types.ProviderType;
import com.dieselProjects.supportCluster.Types.ReportType;
import com.dieselProjects.supportCluster.Types.TAIndicators;
import com.dieselProjects.supportCluster.Types.TradeType;

public class Configuration {

	// Singleton Implementation:
	// -------------------------

	private static final Configuration GeneralCFG = new Configuration();

	public static Configuration getInstance() {
		return GeneralCFG;
	}

	public static final int NUM_TA_INDICATORS = 3;
	
	// ----------------------------------- //
	// - General Configuration Variables - //
	// ----------------------------------- //
	// tradeType:
	// OFFLINE_SIMULATED:
	// create offline database with daily tick, iterate on the ticks to simulate
	// real trade
	// uses simulated broker
	// ONLINE_SIMULATED:
	// simulate online trade, request on time price and daily ticks
	// using online broker
	// ONLINE:
	// real trade - not yet implemented
	//
	// confirmationType:
	// AUTO_CONFIRMATION:
	// Auto confirm requests, can use simulated time delay and simulated rejects
	// SHELL_CONFIRMATION:
	// Prompt human interface shell confirm
	// GUI_CONFIRMATION:
	// Generate GUI with information about the stock and prompt button click
	// confirm
	// WEB_CONFIRMATION:
	// Generate web page with information about the stock and prompt button
	// click confirm
	// uses MySql database
	//
	// BrokerType
	// SIMULATED_BROKER:
	// Simulate broker functionality, can add random delay to simulate real
	// broker behavior
	// TWS_SIMULATED_BROKER:
	// Not in use - simulate TWS broker behavior
	// TWS_ONLINE_BROKER:
	// Use TWS online broker
	// ONLINE_BROKER:
	// Not in use
	//
	// providerType
	// YAHOO_PROVIDER:
	// uses yahoo csv API to for stock information
	// IB_PROVIDER:
	// uses IB data base as for stocks information
	// DB_PROVIDER:
	// uses internal data base for intraday trade
	//
	// simulatedSetupPhase: for simulated online trade, receive last
	// 'setupTicks' daily ticks for filtered stocks
	// initialFunds: initial trade funds in $

	
	// OFFLINE MODE (call yahoo/DB to get database)
	public TradeType tradeType = TradeType.OFFLINE_SIMULATED;
	public BrokerType brokerType = BrokerType.SIMULATED_BROKER;
	public ProviderType providerType = ProviderType.DB_PROVIDER;

	// Simulated Online (Currently get stocks only from yahoo) - good for long trading strategy
	//public TradeType tradeType = TradeType.ONLINE_SIMULATED;
	//public BrokerType brokerType = BrokerType.TWS_ONLINE_BROKER;
	//public ProviderType providerType = ProviderType.IB_PROVIDER;

	// Online Mode (Get stocks from TWS) - currently good for intraday trade
	//public TradeType tradeType = TradeType.ONLINE;
	//public BrokerType brokerType = BrokerType.TWS_ONLINE_BROKER;
	//public ProviderType providerType = ProviderType.IB_PROVIDER;
	
	
	public ConfirmationType confirmationType = ConfirmationType.AUTO_CONFIRMATION;
	public boolean simulatedSetupPhase = true;
	public Double initialFunds = 100000.0;
	public String twsHost = "127.0.0.1";
	public int twsPort = 4001;
	public int twsID = 0;

	// ------------------------ //
	// - Logger Configuration - //
	// ------------------------ //
	// reportType:
	// REPORT_SUMMERY:
	// Only report balance at the end of test
	// REPORT_TRANSACTION:
	// Same as REPORT_SUMMERY with additional transaction report
	// REPORT_TICKS:
	// Same as REPORT_TRANSACTION with additional ticks report
	// REPORT_ALL:
	// Report all
	// enableColor:
	// Use colors for some features
	// enableLogFile:
	// Store report in log file
	public ReportType reportType = ReportType.REPORT_TRANSACTION;
	public boolean enableColor = false;
	public boolean enableLogFile = false;
	public boolean enableStoreToFile = false;
	public boolean enablePrintToScreen = true;

	// ----------------------- //
	// - Trade Configuration - //
	// ----------------------- //
	// setupTicks - amount of ticks before trade starts
	// minGradeSell - minimum grade for stock sale
	// minGradeBuy - minimum grade for stock buy
	// minDailySamples - daily base indicators minimum ticks
	
	public Double maxPerToBuy = 100.0;
	//public Double maxPerToBuy = 50.0; // for 2 stocks
	//public Double maxPerToBuy = 33.0; // for 3 stocks
	//public Double maxPerToBuy = 25.0; // for 4 stocks

	public Integer setupTicks = 0;
	public Double minGradeSell = 1.0;
	public Double minGradeBuy = 1.0;
	public Integer minDailySamples = 0;

	// Simulation Configuration:
	// -------------------------
	// Configurations for simulated trade
	// startDate - simulated trade start date, for intraday dd_MM_yyyy
	// endDate - simulated trade end date
	// sample - delay between each trade cycle, for intraday trade 1M or 5M
	// simulatedDelay - simulated response delay
	//public String startDate = "1.6.2013";
	public String startDate = "25_08_2015";
	public String endDate = "12.6.2015";
	public String sample = "1M";
	public Boolean simulatedDelay = false;

	// FileIo Configuration:
	// ---------------------
	// allStocks - list of all available stocks
	// filteredStockFile - list of filtered stocks to trade with
	// storeDBFile - location of DB dump
	// logFile - location of log file
	public String allStocks = "src/main/resources/stockList.csv";
	public String filteredStockFile = "src/main/resources/filteredStocks.csv";
	public String storeDBFile = "src/main/resources/stockDB.DBF";
	public String logFile = "src/main/resources/stockLog.log";

	// DataBase configuration:
	// -----------------------
	// maximumNumOfSamples - store this amount of samples for each stock
	public Integer maximumNumOfSamples = 80;

	// ---------------------------- //
	// - indicators Configuration - //
	// ---------------------------- //

	// Determine which configuration is enabled:
	// [0] MACD
	// [1] SRSI
	// [2] BBANDS
	public Boolean TaIndicators[] = new Boolean[NUM_TA_INDICATORS];

	public MACDCfg macdCfg;
	public SRSICfg srsiCfg;
	public CdlIndicatorsCfg cdlIndicatorsCfg;

	public boolean showAllIndications = true;
	
	// ---------------------------------------- //
	// - Additional sub classes configuration - //
	// ---------------------------------------- //

	public OnlineTimeCfg onlineTimeCfg;
	public ScreenerCfg screenerCfg;
	public SimulateCfg simulateCfg;

	private Configuration() {

		System.out.println("Creating sub Configurations");
		macdCfg = MACDCfg.getInstance();
		srsiCfg = SRSICfg.getInstance();
		onlineTimeCfg = OnlineTimeCfg.getInstance();
		screenerCfg = ScreenerCfg.getInstance();
		simulateCfg = SimulateCfg.getInstance();

		TaIndicators[TAIndicators.MACD.getMask()] = false;
		TaIndicators[TAIndicators.SRSI.getMask()] = false;
		TaIndicators[TAIndicators.BBANDS.getMask()] = true;
	}

	public void print() {
		System.out.println("-----------------------------------");
		System.out.println("--   ALGO TRADER CONFIGURATION   --");
		System.out.println("-----------------------------------\n\n");
		System.out.println("General configuration:");
		System.out.println("\ttradeType: " + tradeType);
		System.out.println("\tinitialFunds: " + initialFunds);
		if (tradeType == TradeType.OFFLINE_SIMULATED) {
			System.out.println("\tConfiguration for offline simulated trade:");
			System.out.println("\t\tusing simulatedBroker");
			System.out.println("\t\tstartDate     : " + startDate);
			System.out.println("\t\tendDate       : " + endDate);
			System.out.println("\t\tsample        : " + sample);
			System.out.println("\t\tsimulatedDelay: " + simulatedDelay);
		}
		if (tradeType == TradeType.ONLINE_SIMULATED) {
			System.out.println("\tConfiguration for online simulated trade:");
			System.out.println("\t\tsimulatedSetupPhase: "
					+ simulatedSetupPhase);
			System.out.println("\t\tBroker Type: " + brokerType);
			System.out.println("\t\tproviderType: " + providerType);
		}
		System.out.println("\tconfirmationType: " + confirmationType);

		System.out.println("\tLogging configuration:");
		System.out.println("\t\treportType: " + reportType);
		System.out.println("\t\tenableColor: " + enableColor);
		System.out.println("\t\tenableLogFile: " + enableLogFile);

		System.out.println("-------------------------------");
		System.out.println("Trade configuration:");
		System.out.println("\tsetupTicks      : " + setupTicks);
		System.out.println("\tminGradeSell    : " + minGradeSell);
		System.out.println("\tminGradeBuy     : " + minGradeBuy);
		System.out.println("\tminDailySamples : " + minDailySamples);

		System.out.println("-------------------------------");
		System.out.println("Data base configuration:");
		System.out.println("\tmaximumNumOfSamples : " + maximumNumOfSamples);
		System.out.println("-------------------------------");

		System.out.println("Indicators enable configuration:");
		System.out.println("\tMACD: "
				+ TaIndicators[TAIndicators.MACD.getMask()]);
		System.out.println("\tSRSI: "
				+ TaIndicators[TAIndicators.SRSI.getMask()]);
		System.out.println("\tBBANDS: "
				+ TaIndicators[TAIndicators.BBANDS.getMask()]);
		onlineTimeCfg.print();
		macdCfg.print();
		srsiCfg.print();
		System.out.println("----------------------------");
		System.out.println("--   END CONFIGURATION    --");
		System.out.println("----------------------------\n\n");
	}

	public LinkedList<String> getValues() {

		LinkedList<String> list = new LinkedList<String>();

		list.addLast("Test configuration:");
		list.addLast("Time frame: " + startDate + " - " + endDate);
		list.addAll(macdCfg.getValues());
		list.addAll(srsiCfg.getValues());
		list.addAll(cdlIndicatorsCfg.getValues());
		return list;
	}

}
