package com.dieselProjects.configurationCluster;

public class StopLostOngoingCfg {

	private static final StopLostOngoingCfg stopLostOngoingCfg = new StopLostOngoingCfg();
	
	public static StopLostOngoingCfg getInstance(){
		return stopLostOngoingCfg;
	}
	
	public double stopLostFactor = 0.998;
	
}
