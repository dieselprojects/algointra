package com.dieselProjects.configurationCluster;

public class HoldBuyCfg {

	private static final HoldBuyCfg holdBuyCfg = new HoldBuyCfg();
	
	public static HoldBuyCfg getInstance() {
		return holdBuyCfg;
	}
	
	public Integer minHoldBuy = 1;
	public Integer maxHoldBuy = 6;

	
}
