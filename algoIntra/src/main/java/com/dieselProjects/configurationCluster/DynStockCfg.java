package com.dieselProjects.configurationCluster;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.supportCluster.Types.DynAlgoType;
import com.dieselProjects.supportCluster.Types.OPType;

public class DynStockCfg implements Serializable{

	/*
	 * This class can hold dynamic configuration for stock algorithm 
	 */
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Generic algorithm configuration
	public List<DynAlgoType> phase1List;
	public List<Object> phase1Cfg;
	
	public List<DynAlgoType> phase2List;
	public List<Object> phase2Cfg;
	
	public List<DynAlgoType> phase3List;
	public List<Object> phase3Cfg;
	
	public List<DynAlgoType> superPhaseList;
	public List<Object> superPhaseCfg;

	
	public DynStockCfg(OPType opType){
		
		// phases:
		// no phase 1 yet
		// phase 2: macdBBands
		// phase 3: for sell holdSell
		// superPhase: stopLost
		
		phase1List = new LinkedList<DynAlgoType>();
		
		phase2List = new LinkedList<DynAlgoType>();
		phase2List.add(DynAlgoType.MACD_BBANDS_ALGO);
		
		phase3List = new LinkedList<DynAlgoType>();
		if (opType == OPType.SELL)
			phase3List.add(DynAlgoType.HOLD_SELL_ALGO);
		
		superPhaseList = new LinkedList<DynAlgoType>();
		if (opType == OPType.SELL)
			superPhaseList.add(DynAlgoType.STOP_LOST_ALGO);
	
		
		// sub configuration:
		phase2Cfg = new LinkedList<Object>();
		phase2Cfg.add(new DynMACDCfg(MACDCfg.getInstance()));
		phase2Cfg.add(new DynBBandsCfg(BBandsCfg.getInstance()));

		phase3Cfg = new LinkedList<Object>();
		phase3Cfg.add(new DynHoldSellCfg(HoldSellCfg.getInstance()));
	
		superPhaseCfg = new LinkedList<Object>();
		superPhaseCfg.add(new DynStopLostCfg(StopLostCfg.getInstance()));
		
	}
	
}
