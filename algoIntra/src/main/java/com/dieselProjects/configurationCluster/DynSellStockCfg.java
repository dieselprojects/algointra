package com.dieselProjects.configurationCluster;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.supportCluster.Types.DynSellMainNodeType;
import com.dieselProjects.supportCluster.Types.DynSellStopLostNodeType;
import com.dieselProjects.supportCluster.Types.DynSellSupportNodeType;
import com.dieselProjects.supportCluster.Types.MACDStrategyType;
import com.dieselProjects.supportCluster.Types.StockModeType;

public class DynSellStockCfg implements Serializable {

	private static final String TAG = "DynSellStockCfg";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<DynSellStopLostNodeType> stopLostNodeTypes;
	public List<Object> stopLostCfgNodes;

	public List<DynSellMainNodeType> mainSellNodeTypes;
	public List<Object> mainSellCfgNodes;

	public List<DynSellSupportNodeType> supportSellNodeTypes;
	public List<Object> supportSellCfgNodes;

	public StockModeType stockModeType = StockModeType.UP;

	// public StockModeType stockModeType = StockModeType.UP_AND_DOWN;

	public DynSellStockCfg(boolean isDefault) {

		stopLostNodeTypes = new LinkedList<DynSellStopLostNodeType>();
		stopLostCfgNodes = new LinkedList<Object>();

		mainSellNodeTypes = new LinkedList<DynSellMainNodeType>();
		mainSellCfgNodes = new LinkedList<Object>();

		supportSellNodeTypes = new LinkedList<DynSellSupportNodeType>();
		supportSellCfgNodes = new LinkedList<Object>();

		if (isDefault == true) {
			// Default algorithm for sell:
			// stopLost || (macd || bbands) && holdSell

			stopLostNodeTypes.add(DynSellStopLostNodeType.STOP_LOST_NODE);
			stopLostCfgNodes.add(new DynStopLostCfg(StopLostCfg.getInstance()));

			mainSellNodeTypes.add(DynSellMainNodeType.SIMPLE_MACD_NODE);
			mainSellNodeTypes.add(DynSellMainNodeType.SIMPLE_BBANDS_NODE);
			mainSellCfgNodes.add(new DynMACDCfg(MACDCfg.getInstance()));
			mainSellCfgNodes.add(new DynBBandsCfg(BBandsCfg.getInstance()));

			supportSellNodeTypes.add(DynSellSupportNodeType.HOLD_SELL_NODE);
			supportSellCfgNodes.add(new DynHoldSellCfg(HoldSellCfg
					.getInstance()));

		} else {
			switch (stockModeType) {
			case UP:

				// Same as default algorithm for sell:
				// stopLost || (macd || bbands) && holdSell
				stopLostNodeTypes.add(DynSellStopLostNodeType.STOP_LOST_NODE);
				stopLostCfgNodes.add(new DynStopLostCfg(StopLostCfg
						.getInstance()));

				// stopLostNodeTypes
				// .add(DynSellStopLostNodeType.STOP_LOST_ONGOING_ALGO);
				// stopLostCfgNodes.add(new DynStopLostOngoingCfg(
				// StopLostOngoingCfg.getInstance()));

				mainSellNodeTypes.add(DynSellMainNodeType.SIMPLE_MACD_NODE);
				mainSellNodeTypes.add(DynSellMainNodeType.SIMPLE_BBANDS_NODE);
				mainSellNodeTypes
						.add(DynSellMainNodeType.STOPLOST_ONGOING_NODE);
				mainSellNodeTypes.add(DynSellMainNodeType.CDL_PATTERN_NODE);

				mainSellCfgNodes.add(new DynMACDCfg(MACDCfg.getInstance()));
				mainSellCfgNodes.add(new DynBBandsCfg(BBandsCfg.getInstance()));
				mainSellCfgNodes.add(new DynStopLostOngoingCfg(
						StopLostOngoingCfg.getInstance()));
				mainSellCfgNodes.add(new DynSellCdlPatternCfg(SellCdlPatternCfg
						.getInstance()));

				supportSellNodeTypes.add(DynSellSupportNodeType.HOLD_SELL_NODE);
				supportSellCfgNodes.add(new DynHoldSellCfg(HoldSellCfg
						.getInstance()));
				break;
			case UP_AND_DOWN:

				// Configuration:
				// stopLost(more sensitive) || (macd(default)) && holdSell

				stopLostNodeTypes.add(DynSellStopLostNodeType.STOP_LOST_NODE);

				DynStopLostCfg stopLostCfg = new DynStopLostCfg(
						StopLostCfg.getInstance());
				stopLostCfg.stopLostPer = 0.998;
				stopLostCfgNodes.add(stopLostCfg);

				mainSellNodeTypes.add(DynSellMainNodeType.SIMPLE_MACD_NODE);
				// mainSellNodeTypes.add(DynSellMainNodeType.SIMPLE_BBANDS_ALGO);

				DynMACDCfg macdCfg = new DynMACDCfg(MACDCfg.getInstance());
				macdCfg.macdStrategyType = MACDStrategyType.DEFAULT;

				mainSellCfgNodes.add(macdCfg);
				// mainSellCfgNodes.add(new
				// DynBBandsCfg(BBandsCfg.getInstance()));

				supportSellNodeTypes.add(DynSellSupportNodeType.HOLD_SELL_NODE);

				DynHoldSellCfg holdSellCfg = new DynHoldSellCfg(
						HoldSellCfg.getInstance());
				holdSellCfg.holdSellUntil = 1.004;

				supportSellCfgNodes.add(holdSellCfg);
				break;
			case DOWN:
				break;
			case DOWN_AND_UP:
				break;
			case NEUTRAL:
				break;
			default:
				System.err.println(TAG + " Stock mode " + stockModeType
						+ " not supported");
				System.exit(1);
				break;
			}
		}
	}
}
