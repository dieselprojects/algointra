package com.dieselProjects.configurationCluster;

public class DynHoldBuyCfg {

	public Integer minHoldBuy = 3;
	public Integer maxHoldBuy = 6;
	
	
	public DynHoldBuyCfg(HoldBuyCfg defaultCfg){
		this.minHoldBuy = defaultCfg.minHoldBuy;
		this.maxHoldBuy = defaultCfg.maxHoldBuy;
	}
}
