package com.dieselProjects.configurationCluster;

public class ScreenerCfg {

	private static final ScreenerCfg screenerCfg = new ScreenerCfg();
	
	public static ScreenerCfg getInstance(){
		return screenerCfg;
	}
	
	public boolean useFilterAtStart = false;
	public long filteringVolume = 1000000;
	public double filteringPrice = 1;
	
}
