package com.dieselProjects.configurationCluster;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.supportCluster.Types.BBandsStrategyType;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;
import com.dieselProjects.supportCluster.Types.DynBuySupportNodeType;
import com.dieselProjects.supportCluster.Types.MACDStrategyType;
import com.dieselProjects.supportCluster.Types.StockModeType;

public class DynBuyStockCfg implements Serializable {

	private static final String TAG = "DynBuyStockCfg";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<DynBuyMainNodeType> mainBuyNodeTypes;
	public List<Object> mainCfgNodes;

	public List<DynBuySupportNodeType> supportBuyNodeTypes;
	public List<Object> supportCfgNodes;

	public StockModeType stockModeType = StockModeType.UP;

	// public StockModeType stockModeType = StockModeType.UP_AND_DOWN;

	public DynBuyStockCfg(boolean isDefault) {
		mainBuyNodeTypes = new LinkedList<DynBuyMainNodeType>();
		mainCfgNodes = new LinkedList<Object>();

		supportBuyNodeTypes = new LinkedList<DynBuySupportNodeType>();
		supportCfgNodes = new LinkedList<Object>();

		if (isDefault == true) {
			// default algorithm for buy:
			// (macd || bbands) && buyHold
			mainBuyNodeTypes.add(DynBuyMainNodeType.SIMPLE_MACD_NODE);
			mainBuyNodeTypes.add(DynBuyMainNodeType.SIMPLE_BBANDS_NODE);

			mainCfgNodes.add(new DynMACDCfg(MACDCfg.getInstance()));
			mainCfgNodes.add(new DynBBandsCfg(BBandsCfg.getInstance()));

			supportBuyNodeTypes.add(DynBuySupportNodeType.HOLD_BUY_NODE);
			supportCfgNodes.add(new DynHoldBuyCfg(HoldBuyCfg.getInstance()));

		} else {

			switch (stockModeType) {
			case UP:
				// same as default algorithm for buy:
				// (macd || bbands) && holdBuy
				mainBuyNodeTypes.add(DynBuyMainNodeType.SIMPLE_MACD_NODE);
				mainBuyNodeTypes.add(DynBuyMainNodeType.SIMPLE_BBANDS_NODE);
				mainBuyNodeTypes.add(DynBuyMainNodeType.CDL_PATTERN_NODE);

				mainCfgNodes.add(new DynMACDCfg(MACDCfg.getInstance()));
				mainCfgNodes.add(new DynBBandsCfg(BBandsCfg.getInstance()));
				mainCfgNodes.add(new DynBuyCdlPatternCfg(BuyCdlPatternCfg
						.getInstance()));

				supportBuyNodeTypes.add(DynBuySupportNodeType.HOLD_BUY_NODE);
				supportCfgNodes
						.add(new DynHoldBuyCfg(HoldBuyCfg.getInstance()));
				break;
			case UP_AND_DOWN:
				// stock go up and than down, support one major up and one down:
				// (macd || bbands) with default parameters
				mainBuyNodeTypes.add(DynBuyMainNodeType.SIMPLE_MACD_NODE);
				// mainBuyNodeTypes.add(DynBuyMainNodeType.SIMPLE_BBANDS_ALGO);

				DynMACDCfg macdCfg = new DynMACDCfg(MACDCfg.getInstance());
				macdCfg.macdStrategyType = MACDStrategyType.DEFAULT;

				DynBBandsCfg bbandsCfg = new DynBBandsCfg(
						BBandsCfg.getInstance());
				bbandsCfg.bbandsStrategyType = BBandsStrategyType.DEFAULT;

				mainCfgNodes.add(macdCfg);
				// mainCfgNodes.add(bbandsCfg);

				// supportBuyNodeTypes.add(DynBuySupportNodeType.HOLD_BUY_ALGO);
				// supportCfgNodes
				// .add(new DynHoldBuyCfg(HoldBuyCfg.getInstance()));
				break;
			case DOWN:
				break;
			case DOWN_AND_UP:
				break;
			case NEUTRAL:
				break;
			default:
				System.err.println(TAG + " Stock mode " + stockModeType
						+ " not supported");
				System.exit(1);
				break;
			}

		}

	}

}
