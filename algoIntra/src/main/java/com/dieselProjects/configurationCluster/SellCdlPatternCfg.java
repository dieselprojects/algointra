package com.dieselProjects.configurationCluster;
import com.dieselProjects.supportCluster.Types.CDLIndicators;

public class SellCdlPatternCfg {

	private static final SellCdlPatternCfg sellCdlPatternCfg = new SellCdlPatternCfg();

	public static SellCdlPatternCfg getInstance() {
		return sellCdlPatternCfg;
	}

	// number of ticks to check for patterns
	public int numOfPastTicks = 3;

	// does all patterns need to be found in the above period in order to
	// trigger the node
	public boolean requireAllPatterns = false;

	// array of relevant candlestick patterns
	public boolean[] patterns = new boolean[CDLIndicators.getSize()];


	public SellCdlPatternCfg() {
		// default buy patterns:
		
		// 2 crowss
		patterns[0] = true;
		
		// 3 black crows
		patterns[1] = true;
		
		// advance block
		patterns[8] = true;

		// dark cloud cover
		patterns[14] = true;
	}

}
