package com.dieselProjects.configurationCluster;

import java.io.Serializable;


public class DynStopLostCfg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public double stopLostPer = 0.995;
	
	public DynStopLostCfg(StopLostCfg defaultStopLostCfg){
		this.stopLostPer = defaultStopLostCfg.stopLostPer;
	}
	
}
