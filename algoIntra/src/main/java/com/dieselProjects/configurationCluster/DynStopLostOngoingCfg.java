package com.dieselProjects.configurationCluster;

import java.io.Serializable;

public class DynStopLostOngoingCfg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public double stopLostFactor = 0.998;
	
	public DynStopLostOngoingCfg(StopLostOngoingCfg defaultCfg){
		this.stopLostFactor = defaultCfg.stopLostFactor;
	}
}
