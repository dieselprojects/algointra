package com.dieselProjects.configurationCluster;

import java.util.LinkedList;

import com.dieselProjects.supportCluster.Types.MACDStrategyType;
import com.tictactec.ta.lib.MAType;

public class MACDCfg {

	private static final MACDCfg MACDCfg = new MACDCfg();

	public static MACDCfg getInstance() {
		return MACDCfg;
	}

	// Indicator parameters:
	// ---------------------
	public int macdSimTicksParam = 30;
	public int macdFastParam = 12;
	public int macdSlowParam = 26;
	public int macdSignalParam = 9;
	public int macdTickBeforeParam = 3;
	public double macdIndicaitonMargain = 0.1;
	public MAType macdMATypeFastParam = MAType.Ema;
	public MAType macdMATypeSlowParam = MAType.Ema;
	public MAType macdMATypeSignalParam = MAType.Ema;

	// Indicator strategy:
	// -------------------
	public MACDStrategyType macdStrategyType = MACDStrategyType.CUSTOM2;
	public int macdNumberOfElements = 3;
	public double macdHistChangeBuy = -0.03; // -((4e-6) * Math.pow(avgValue, 2) + 0.00275 * avgValue + 0.08)/40
	public double macdHistChangeSell = 0.01; // (((4e-6) * Math.pow(avgValue, 2) + 0.00275 * avgValue + 0.08) * 0.75)/40;
	


	public void print() {
		System.out.println("MACD configuration:");
		System.out.println("\tmacdSimTicksParam     : " + macdSimTicksParam);
		System.out.println("\tmacdFastParam         : " + macdFastParam);
		System.out.println("\tmacdSlowParam         : " + macdSlowParam);
		System.out.println("\tmacdSignalParam       : " + macdSignalParam);
		System.out.println("\tmacdTickBeforeParam   : " + macdTickBeforeParam);
		System.out
				.println("\tmacdIndicaitonMargain : " + macdIndicaitonMargain);
		System.out.println("\tmacdMATypeFastParam   : " + macdMATypeFastParam);
		System.out.println("\tmacdMATypeSlowParam   : " + macdMATypeSlowParam);
		System.out
				.println("\tmacdMATypeSignalParam : " + macdMATypeSignalParam);
		System.out.println("-------------------------------");
	}

	public LinkedList<String> getValues() {

		LinkedList<String> list = new LinkedList<String>();

		list.addLast("MACD configuration:");
		list.addLast("macdSimTicksParam     : " + macdSimTicksParam);
		list.addLast("macdFastParam         : " + macdFastParam);
		list.addLast("macdSlowParam         : " + macdSlowParam);
		list.addLast("macdSignalParam       : " + macdSignalParam);
		list.addLast("macdTickBeforeParam   : " + macdTickBeforeParam);
		list.addLast("macdIndicaitonMargain : " + macdIndicaitonMargain);
		list.addLast("macdMATypeFastParam   : " + macdMATypeFastParam);
		list.addLast("macdMATypeSlowParam   : " + macdMATypeSlowParam);
		list.addLast("macdMATypeSignalParam : " + macdMATypeSignalParam);
		list.addLast("-------------------------------");
		return list;
	}

}
