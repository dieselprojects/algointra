package com.dieselProjects.configurationCluster;

public class PerformanceMonitorCfg {

	private static final PerformanceMonitorCfg performanceMonitorCfg = new PerformanceMonitorCfg();

	public static PerformanceMonitorCfg getInstance() {
		return performanceMonitorCfg;
	}

	public int numOfTradeMin2 = 4;
	public int numOfTradeMin1 = 7;
	public int numOfTradeMax1 = 12;
	public int numOfTradeMax2 = 15;

	// Trade grades:
	// -------------

	public double successTradeGrade = 2.0;
	public double earnTradeGrade = 10.0; // multiply by % earned

	public double failTradeGrade = -3.0;
	public double lossTradeGrade = 15.0; // multiply by % of loss

	// Global day trade grades (per stock) :
	// -------------------------------------

	public double holdingAtDayEndGrade = -15.0; // deducted from final grade

	public double numTradesInside1 = 15.0; // number of trades are inside min1
											// and max1
	public double numTradesInside2 = 10.0; // number of trades are inside min2
											// and max2

	public double excessEarning = 10.0;
	public double lackEarning = -15.0;
}
