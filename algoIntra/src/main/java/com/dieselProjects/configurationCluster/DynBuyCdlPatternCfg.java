package com.dieselProjects.configurationCluster;

import java.io.Serializable;

public class DynBuyCdlPatternCfg extends CdlIndicatorsCfg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int numOfPastTicks = 3;
	public boolean requireAllPatterns = false;
	public boolean[] patterns;
	
	
	public DynBuyCdlPatternCfg(BuyCdlPatternCfg defaultCfg) {
		this.numOfPastTicks = defaultCfg.numOfPastTicks;
		this.requireAllPatterns = defaultCfg.requireAllPatterns;
		this.patterns = defaultCfg.patterns;
		this.cdlIndicators = defaultCfg.patterns;
	}
}
