package com.dieselProjects.configurationCluster;

import java.util.LinkedList;

import com.tictactec.ta.lib.MAType;

public class SRSICfg {

	private static final SRSICfg SRSICfg = new SRSICfg();

	public static SRSICfg getInstance() {
		return SRSICfg;
	}

	public int srsiSimTicksParam = 30;
	public int srsiFastKParam = 6;
	public int srsiFastDParam = 14;
	public int srsiBuyParam = 40;
	public int srsiSellParam = 60;
	public MAType srsiMATypeDParam = MAType.Ema;


	public void print() {
		System.out.println("SRSI configuration:");
		System.out.println("\tsrsiSimTicksParam : " + srsiSimTicksParam);
		System.out.println("\tsrsiFastKParam    : " + srsiFastKParam);
		System.out.println("\tsrsiFastDParam    : " + srsiFastDParam);
		System.out.println("\tsrsiBuyParam      : " + srsiBuyParam);
		System.out.println("\tsrsiSellParam     : " + srsiSellParam);
		System.out.println("\tsrsiMATypeDParam  : " + srsiMATypeDParam);
		System.out.println("-------------------------------");
	}

	public LinkedList<String> getValues() {

		LinkedList<String> list = new LinkedList<String>();
		list.addLast("SRSI configuration:");
		list.addLast("srsiSimTicksParam	: " + srsiSimTicksParam);
		list.addLast("srsiFastKParam	: " + srsiFastKParam);
		list.addLast("srsiFastDParam	: " + srsiFastDParam);
		list.addLast("srsiBuyParam		: " + srsiBuyParam);
		list.addLast("srsiSellParam		: " + srsiSellParam);
		list.addLast("srsiMATypeDParam	: " + srsiMATypeDParam);
		list.addLast("-------------------------------");

		return list;
	}
}
