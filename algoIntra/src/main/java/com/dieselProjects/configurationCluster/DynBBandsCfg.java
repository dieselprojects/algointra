package com.dieselProjects.configurationCluster;

import java.io.Serializable;

import com.dieselProjects.supportCluster.Types.BBandsStrategyType;
import com.tictactec.ta.lib.MAType;

public class DynBBandsCfg  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MAType bbandsMATypeParam = MAType.Ema;
	public int bbandsTimePeriod = 20;
	public int bbandsPeriodUp = 2;
	public int bbandsPeriodDown = 2;
	public BBandsStrategyType bbandsStrategyType = BBandsStrategyType.CUSTOM2;
	public int bbandsLookBack = 3;
	public double bbandsMargin = 0.05;

	public DynBBandsCfg(BBandsCfg defaultBBandsCfg) {
		this.bbandsMATypeParam = defaultBBandsCfg.bbandsMATypeParam;
		this.bbandsTimePeriod = defaultBBandsCfg.bbandsTimePeriod;
		this.bbandsPeriodUp = defaultBBandsCfg.bbandsPeriodUp;
		this.bbandsPeriodDown = defaultBBandsCfg.bbandsPeriodDown;
		this.bbandsStrategyType = defaultBBandsCfg.bbandsStrategyType;
		this.bbandsLookBack = defaultBBandsCfg.bbandsLookBack;
		this.bbandsMargin = defaultBBandsCfg.bbandsMargin;
	}

}
