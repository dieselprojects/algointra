package com.dieselProjects.configurationCluster;

import java.util.HashMap;
import java.util.LinkedList;

import com.dieselProjects.beans.Tick;
import com.dieselProjects.supportCluster.SimulateTimeMan;

public class SimulateCfg {

	private static final SimulateCfg SimulateCFG = new SimulateCfg();

	public static SimulateCfg getInstance() {
		return SimulateCFG;
	}

	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	
	// hash table for ticker linked list:
	public HashMap<String,LinkedList<Tick>> testerArray = new HashMap<String,LinkedList<Tick>>();

	public int getEndOfTest(){
		int endOfTest = 0;
		for (String ticker : testerArray.keySet()) {
			endOfTest = testerArray.get(ticker).size();
		}
		return endOfTest;
	}
}
