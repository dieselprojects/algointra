package com.dieselProjects.configurationCluster;

import java.io.Serializable;

public class DynHoldSellCfg  implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public double holdSellUntil = 1.008; // 0.8%

	public DynHoldSellCfg(HoldSellCfg defaultHoldSellCfg){
		this.holdSellUntil = defaultHoldSellCfg.holdSellUntil;
	}
}
