package com.dieselProjects.supportCluster;

import org.rosuda.JRI.Rengine;

public class RController {
	
	private static final RController rController = new RController ();

	public static RController getInstance() {
		return rController;
	}
	public Rengine re;

	public boolean startR() {
		String[] Rargs = { "--vanilla" };
		re = new Rengine(Rargs, false, null);
		re.eval("require(quantmod)");
		if (re.equals(null)) {
			return false;
		} else {
			return true;
		}
	}

	public boolean endR() {
		re.end();
		return true;
	}

}
