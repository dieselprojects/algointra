package com.dieselProjects.supportCluster;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class OnlineTimeMan {
	
	private static final OnlineTimeMan onlineTimeMan = new OnlineTimeMan ();

	public static OnlineTimeMan getInstance() {
		return onlineTimeMan;
	}
	
	public Calendar getTime() {
		return new GregorianCalendar();
	}
	
	public String getTimeString() {

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return df.format(new GregorianCalendar());
	}

	
}
