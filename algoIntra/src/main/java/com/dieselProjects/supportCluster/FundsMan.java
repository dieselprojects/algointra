package com.dieselProjects.supportCluster;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.PositionBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.Types.OPType;

public class FundsMan {

	private static final String TAG = "FundMan";

	private static final FundsMan funds = new FundsMan();

	public static FundsMan getInstance() {
		return funds;
	}

	Configuration configuration;
	Logger logger;

	private Double initialFunds;
	private Double currentFunds;
	private Double investedFunds;
	Double currentInvestedValue = 0.0;
	Double successPosition = 0.0;
	Double failedPosition = 0.0;

	public boolean noNewBidsAllowed = false;

	private FundsMan() {

		// Configuration:
		configuration = Configuration.getInstance();
		System.out.println("__Constructor__ FundsMan, initial Funds: "
				+ configuration.initialFunds);

		// Setting initial Funds
		this.initialFunds = configuration.initialFunds;
		this.currentFunds = configuration.initialFunds;
		this.investedFunds = 0.0;

		// Logger:
		logger = Logger.getInstance();

	}

	public Double getCurrentFunds() {
		return currentFunds;
	}

	public void setCurrentFunds(Double currentFunds) {
		this.currentFunds = currentFunds;
	}

	public Double getInitialFunds() {
		return initialFunds;
	}

	public void setInitialFunds(Double initialFunds) {
		this.initialFunds = initialFunds;
	}

	public Double getTotalFunds() {
		return currentFunds + investedFunds;
	}

	public Double getProfit() {
		return ((currentFunds + currentInvestedValue) / initialFunds - 1.0) * 100.0;
	}

	public int getAmount(double stockPrice, double percentage) {

		if (percentage > 100 || percentage < 0) {
			System.err.println("% error");
			System.exit(1);
		}

		int amount = (int) Math.floor(initialFunds * (percentage / 100)
				/ stockPrice);

		if (amount * stockPrice > currentFunds) {
			amount = (int) Math.floor(currentFunds / stockPrice);
		}

		return amount;
	}

	public boolean updateFunds(OPType opType, double price, int amount) {

		switch (opType) {
		case BUY:

			if (price * amount > getCurrentFunds()) {

				return false;

			} else {

				currentFunds -= price * amount;
				investedFunds += price * amount;
				return true;

			}
		case SELL:
			currentFunds += price * amount;
			investedFunds -= price * amount;
			return true;
		default:
			System.err.println(TAG + "Not valid opType");
			System.exit(1);
			return false;
		}
	}
	
	public boolean updateFunds(Bid bid) {

		switch (bid.opType) {
		case BUY:

			if (bid.price * bid.amount > getCurrentFunds()) {

				return false;

			} else {

				currentFunds -= bid.price * bid.amount;
				investedFunds += bid.price * bid.amount;
				return true;

			}
		case SELL:
			currentFunds += bid.price * bid.amount;
			investedFunds -= bid.price * bid.amount;
			return true;
		default:
			System.err.println(TAG + "Not valid opType");
			System.exit(1);
			return false;
		}
	}
	
	

	// Sell stocks, By amount:
	// -----------------------
	public boolean sellStock(String ticker, Double price, int amount,
			Double buyPrice) {

		if (buyPrice * amount > (investedFunds + 1.0)) {

			String errorTransaction = new String(
					"__ERROR__ sell more than invested funds, Ticker: "
							+ ticker + ", BuyPrice: " + buyPrice + ", Amount: "
							+ amount);
			System.err.println(errorTransaction);
			logger.addEvent(errorTransaction);

			return false;

		} else {

			String lastTransaction;

			setCurrentFunds(getCurrentFunds() + price * amount);
			investedFunds -= buyPrice * amount;

			if (price > buyPrice) {

				lastTransaction = new String("__Sell Profit__ Ticker: "
						+ ticker + ", Price: " + price + ", BuyPrice: "
						+ buyPrice + ", Amount: " + amount + "Total Profit: "
						+ (price - buyPrice) * amount);

				successPosition += 1;

			} else {

				lastTransaction = new String("__Sell Loss__ Ticker: " + ticker
						+ ", Price: " + price + ", BuyPrice: " + buyPrice
						+ ", Amount: " + amount + "Total Profit: "
						+ (price - buyPrice) * amount);

				failedPosition += 1;

			}

			logger.addEvent(lastTransaction);

			return true;

		}
	}

	// ------------- //
	// - GetStatus - //
	// ------------- //
	// Returns String with Current Funds, InvestedFunds, and InitialFunds

	public String getStatus(List<StockBean> stocks) {

		Iterator<StockBean> iterator = stocks.iterator();
		while (iterator.hasNext()) {
			StockBean stock = iterator.next();
			Iterator<PositionBean> posIterator = stock.positions.iterator();
			while (posIterator.hasNext()) {
				PositionBean position = posIterator.next();
				currentInvestedValue += stock.getPrice() * position.amount;
			}
		}

		String statusString = "\nFunds Manager, Report:\n";
		statusString += "----------------------\n";
		String profitString = "";

		statusString += "Inital funds: " + initialFunds + "\n";
		statusString += "Current funds: " + currentFunds + "\n";
		statusString += "Invested funds: " + investedFunds + "\n";
		statusString += "CurrentInvestedValue " + currentInvestedValue + "\n";
		statusString += "Total funds: " + currentInvestedValue + currentFunds
				+ "\n";

		profitString = (getProfit() > 0) ? new DecimalFormat("##.##")
				.format(getProfit()) : "("
				+ new DecimalFormat("##.##").format(getProfit()) + ")";
		statusString += "Profit: " + profitString + "%\n";

		statusString += "Number of success positions: " + successPosition
				+ "\n";
		statusString += "Number of failed positions: " + failedPosition + "\n";
		statusString += "Success rate: "
				+ (successPosition / (successPosition + failedPosition)) + "\n";

		return statusString;
	}

}
