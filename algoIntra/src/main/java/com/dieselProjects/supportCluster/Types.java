package com.dieselProjects.supportCluster;

public class Types {

	// ------------------------- //
	// -- Configuration types -- //
	// ------------------------- //

	public enum TradeType {
		OFFLINE_SIMULATED, ONLINE_SIMULATED, ONLINE
	}

	public enum ConfirmationType {
		AUTO_CONFIRMATION, SHELL_CONFIRMATION, GUI_CONFIRMATION, WEB_CONFIRMATION
	}

	public enum BrokerType {
		SIMULATED_BROKER, TWS_SIMULATED_BROKER, TWS_ONLINE_BROKER, ONLINE_BROKER
	}

	public enum ProviderType {
		YAHOO_PROVIDER, IB_PROVIDER, DB_PROVIDER
	}

	public enum ReportType {
		NO_REPORT, REPORT_TRANSACTION, REPORT_TICKS, REPORT_ALL
	}

	// Stock state:
	// ------------

	public enum StockStateType {
		ACTIVE, PENDING_BUY, PENDING_SELL, HOLDING
	}

	public enum PositionStateType {
		HOLDING, PENDING_SELL
	}

	// ---------------------- //
	// -- Controller types -- //
	// ---------------------- //

	public enum OPType {
		NONE(0), STRONG_BUY(1), BUY(2), STRONG_SELL(3), SELL(4);

		private final int mask;
		private final static int SIZE = 5;

		private OPType(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	// ------------------ //
	// -- Broker types -- //
	// ------------------ //

	public enum BrokerTickType {
		DailyTick, PriceTick
	}

	// --------------------- //
	// -- Indicators type -- //
	// --------------------- //

	public enum IndicatorsType {
		BUY(0), SELL(1), STOPLOST(2), HOLDSELL(3), HOLDBUY(4), STOPLOSTONGOING(
				5);

		private final int mask;
		private static final int SIZE = 6;

		private IndicatorsType(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	public enum CDLResultType {
		BUYSELL, BUYONLY, SELLONLY, CHANGEDIRECTION
	}

	public enum TAIndicators {
		MACD(0), SRSI(1), BBANDS(2);

		private final int mask;
		private static final int SIZE = 3;

		private TAIndicators(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	public enum CDLIndicators {
		CS0(0), CS1(1), CS2(2), CS3(3), CS4(4), CS5(5), CS6(6), CS7(7), CS8(8), CS9(
				9), CS10(10), CS11(11), CS12(12), CS13(13), CS14(14), CS15(15), CS16(
				16), CS17(17), CS18(18), CS19(19), CS20(20), CS21(21), CS22(22), CS23(
				23), CS24(24), CS25(25), CS26(26), CS27(27), CS28(28), CS29(29), CS30(
				30), CS31(31), CS32(32), CS33(33), CS34(34), CS35(35), CS36(36), CS37(
				37), CS38(38), CS39(39), CS40(40), CS41(41), CS42(42), CS43(43), CS44(
				44), CS45(45), CS46(46), CS47(47), CS48(48), CS49(49), CS50(50), CS51(
				51), CS52(52), CS53(53), CS54(54), CS55(55), CS56(56), CS57(57), CS58(
				58), CS59(59), CS60(60);

		private final int mask;
		private final static int SIZE = 61;

		private CDLIndicators(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	// ------------------------------- //
	// -- Indicators strategy types -- //
	// ------------------------------- //

	public enum MACDStrategyType {
		DEFAULT(0), CUSTOM1(1), CUSTOM2(2);

		private final int mask;
		private static final int SIZE = 3;

		private MACDStrategyType(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	public enum BBandsStrategyType {
		DEFAULT(0), CUSTOM1(1), CUSTOM2(2);

		private final int mask;
		private static final int SIZE = 3;

		private BBandsStrategyType(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	// ------------------------------------- //
	// -- Dynamic Algorithm configuration -- //
	// ------------------------------------- //

	public enum DynBuyMainNodeType {
		SIMPLE_MACD_NODE(0), SIMPLE_BBANDS_NODE(1), CDL_PATTERN_NODE(2);

		private final int mask;
		private static final int SIZE = 3;

		private DynBuyMainNodeType(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	public enum DynBuySupportNodeType {
		HOLD_BUY_NODE
	}

	public enum DynSellMainNodeType {
		SIMPLE_MACD_NODE(0), SIMPLE_BBANDS_NODE(1), STOPLOST_ONGOING_NODE(2), CDL_PATTERN_NODE(
				3);

		private final int mask;
		private static final int SIZE = 4;

		private DynSellMainNodeType(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}

		public static int getSize() {
			return SIZE;
		}
	}

	public enum DynSellSupportNodeType {
		HOLD_SELL_NODE
	}

	public enum DynSellStopLostNodeType {
		STOP_LOST_NODE, STOP_LOST_ONGOING_NODE
	}

	public enum DynAlgoType {
		MACD_BBANDS_NODE, HOLD_SELL_NODE, STOP_LOST_NODE,
	}

	public enum StockModeType {
		UP, STRONG_UP, UP_AND_DOWN, DOWN_AND_UP, DOWN, STRONG_DOWN, NEUTRAL, NOISY_NEUTRAL
	}

	// ------------------------------------ //
	// -- Tick value type (low,open,etc) -- //
	// ------------------------------------ //

	public enum TickValueType {
		OPEN, LOW, HIGH, CLOSE
	}

}
