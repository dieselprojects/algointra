package com.dieselProjects.supportCluster;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.configurationCluster.PerformanceMonitorCfg;
import com.dieselProjects.supportCluster.Types.OPType;

public class PerformanceMonitor {

	/*
	 * Description: monitor all algorithm results and provide grade for
	 * performance monitoring
	 */

	private static final PerformanceMonitor performanceMonitor = new PerformanceMonitor();

	public static PerformanceMonitor getInstance() {
		return performanceMonitor;
	}

	private static final String TAG = "PerformanceMonitor ";

	PerformanceMonitorCfg pmCfg = PerformanceMonitorCfg.getInstance();
	Configuration cfg = Configuration.getInstance();

	private double grade;

	/*
	 * buyBids - algorithm confirmed buy bid transactions, null on iteration
	 * without bids
	 */
	private HashMap<String, List<Bid>> buyBids;

	/*
	 * trades - number of trades for each stock, need at list one trade for this
	 * property to be graded
	 */
	private HashMap<String, Integer> trades;

	/*
	 * all stocks initial prices
	 */
	private HashMap<String, Double> stockBuyHoldEarning;
	private HashMap<String, Double> stockAlgoEarning;

	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");

	public PerformanceMonitor() {
		this.grade = 0.0;
		buyBids = new HashMap<String, List<Bid>>();
		trades = new HashMap<String, Integer>();
		stockBuyHoldEarning = new HashMap<String, Double>();
		stockAlgoEarning = new HashMap<String, Double>();

		Iterator<Entry<String, LinkedList<Tick>>> simStockItr = cfg.simulateCfg.testerArray
				.entrySet().iterator();

		while (simStockItr.hasNext()) {
			Entry<String, LinkedList<Tick>> nextEntry = simStockItr.next();

			// calculate buy & hold earning
			Double buyHoldEarning = nextEntry.getValue().getLast().close()
					/ nextEntry.getValue().getFirst().close();
			stockBuyHoldEarning.put(nextEntry.getKey(), buyHoldEarning);

			// Initial values for algorithm earning
			stockAlgoEarning.put(nextEntry.getKey(), 1.0);

		}
	}

	public double getGrade() {
		return grade;
	}

	public void gradeBids(List<Bid> bids, Calendar date) {

		List<Bid> sellBids = new LinkedList<Bid>();

		// parse buy/sell bids
		Iterator<Bid> bidsItr = bids.iterator();
		while (bidsItr.hasNext()) {
			Bid nextBid = bidsItr.next();

			switch (nextBid.opType) {
			case BUY:

				Bid newBid = new Bid(nextBid);
				if (buyBids.containsKey(nextBid.ticker)) {

					buyBids.get(newBid.ticker).add(newBid);
					trades.put(newBid.ticker, trades.get(newBid.ticker) + 1);

				} else {
					List<Bid> newBidsList = new LinkedList<Bid>();
					newBidsList.add(newBid);
					buyBids.put(newBid.ticker, newBidsList);

					Integer numOfTrades = new Integer(1);
					trades.put(newBid.ticker, numOfTrades);

				}
				break;

			case SELL:

				sellBids.add(nextBid);
				break;

			default:
				System.err.println(TAG + "not a valid op code in bid for "
						+ nextBid.ticker);
				System.exit(1);
				break;

			}
		}

		// match sell bid to buy bid, grade according to performance:
		Iterator<Bid> sellBidItr = sellBids.iterator();

		while (sellBidItr.hasNext()) {

			Bid nextSellBid = new Bid(sellBidItr.next());
			double tradeGrade = 0.0;

			if (!buyBids.containsKey(nextSellBid.ticker)) {
				System.err.println(TAG
						+ " no matching buy bids for sell bid in "
						+ nextSellBid.ticker);
				System.exit(1);
			}

			// Iterate on all buy bids with the same ticker as sell bid, each
			// one will be graded:
			List<Bid> stockBuyBids = buyBids.get(nextSellBid.ticker);
			Iterator<Bid> nextStkBidItr = stockBuyBids.iterator();
			while (nextStkBidItr.hasNext()) {

				Bid nextBuyBid = nextStkBidItr.next();

				// grade according to performance here:
				tradeGrade += grade(nextBuyBid, nextSellBid);

				
				// update earning for transaction:
				Double nextStockAlgoEarning = nextSellBid.price
						/ nextBuyBid.price - 1.0;
				stockAlgoEarning.put(nextSellBid.ticker,
						stockAlgoEarning.get(nextSellBid.ticker)
								+ nextStockAlgoEarning);
				
				// update buy/sell bids amount and remove buy bids beans if
				// necessary
				if (nextSellBid.amount > nextBuyBid.amount) {

					nextSellBid.amount -= nextBuyBid.amount;
					nextBuyBid.amount = 0;

					if (!nextStkBidItr.hasNext()) {
						System.err.println(TAG + "Invalid sell amount for "
								+ nextSellBid.ticker);
						System.exit(1);
					}

				} else {

					double sellBidAmount = nextSellBid.amount;
					nextBuyBid.amount -= sellBidAmount;
					break;

				}

			}

			System.out.println(TAG + "Ticker " + nextSellBid.ticker + " Time "
					+ sdf.format(date.getTime()) + " Grade " + tradeGrade);
			grade += tradeGrade;
		}

		System.out.println(TAG + "TOTAL for Time " + sdf.format(date.getTime())
				+ " Grade " + grade);

		// remove all empty buy bids:
		Iterator<Entry<String, List<Bid>>> itr = buyBids.entrySet().iterator();
		while (itr.hasNext()) {
			List<Bid> stockBuyBids = itr.next().getValue();
			for (int i = 0; i < stockBuyBids.size(); i++) {
				if (stockBuyBids.get(i).amount == 0)
					stockBuyBids.remove(i);
			}
		}
	}

	public void gradeEOT(List<Bid> bids, Calendar date) {

		// Grades for not holding anything at EOT:
		// ---------------------------------------
		grade += bids.size() * pmCfg.holdingAtDayEndGrade;
		System.out.println(TAG + "Number of trades at EOT " + bids.size()
				+ " grade " + grade);

		// Grades for number of transaction per stock:
		// --------------------------------------------
		Iterator<Entry<String, Integer>> numItr = trades.entrySet().iterator();
		while (numItr.hasNext()) {
			Entry<String, Integer> entry = numItr.next();
			String ticker = entry.getKey();
			Integer stockTrades = entry.getValue();

			// number of trades per stock:
			if ((stockTrades > pmCfg.numOfTradeMin1)
					&& (stockTrades < pmCfg.numOfTradeMax1)) {
				grade += pmCfg.numTradesInside1;
			} else if ((stockTrades > pmCfg.numOfTradeMin2)
					&& (stockTrades < pmCfg.numOfTradeMax2)) {
				grade += pmCfg.numTradesInside2;
			}
			System.out.println(TAG + "Number of trades for " + ticker + " "
					+ stockTrades + " Grade " + grade);

			// Grades for excess stock earning:
			// --------------------------------
			// TODO: add support for multiple stocks
			Iterator<Entry<String, Double>> earningItr = stockBuyHoldEarning
					.entrySet().iterator();
			while (earningItr.hasNext()) {
				Entry<String, Double> nextEntry = earningItr.next();
				Double buyHoldEarning = nextEntry.getValue();
				Double algoEarning = stockAlgoEarning.get(nextEntry.getKey());

				if (algoEarning > buyHoldEarning) {
					grade += pmCfg.excessEarning
							* (algoEarning / buyHoldEarning - 1.0);
				} else if (algoEarning < buyHoldEarning) {
					grade += pmCfg.lackEarning
							* (1.0 - algoEarning / buyHoldEarning);
				}

				System.out.println(TAG + "Earning for " + nextEntry.getKey()
						+ " buyHold " + buyHoldEarning + " Algo " + algoEarning
						+ " Grade " + grade);
			}
		}

		System.out.println(TAG + "Final grade " + grade);
	}

	private double grade(Bid buyBid, Bid sellBid) {

		double grade = 0.0;

		// success/fail
		if (buyBid.price < sellBid.price) {
			grade += pmCfg.successTradeGrade;
			grade += (sellBid.price / buyBid.price - 1) * 100.0
					* pmCfg.earnTradeGrade;
		} else {
			grade += pmCfg.failTradeGrade;
			grade += (sellBid.price / buyBid.price - 1) * 100.0
					* pmCfg.lossTradeGrade;
		}

		return grade;
	}

}
