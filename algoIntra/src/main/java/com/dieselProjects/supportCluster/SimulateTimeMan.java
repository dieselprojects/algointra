package com.dieselProjects.supportCluster;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

import com.dieselProjects.configurationCluster.Configuration;

public class SimulateTimeMan {
	private static final SimulateTimeMan simulateTimeMan = new SimulateTimeMan();

	public static SimulateTimeMan getInstance() {
		return simulateTimeMan;
	}

	public Integer iteration = 0;
	Configuration configuration = Configuration.getInstance();
	public LinkedList<Calendar> simulatedDates = new LinkedList<Calendar>();

	public Calendar getTime() {
		return simulatedDates.get(iteration);
	}

	public String getTimeString() {

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return df.format(getTime().getTime());

	}

	public String getTimeString(String ticker, int i) {

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return df.format(configuration.simulateCfg.testerArray.get(ticker).get(
				i).date.getTime());

	}

}
