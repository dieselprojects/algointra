package com.dieselProjects.supportCluster;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.dieselProjects.beans.TimeInst;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.configurationCluster.OnlineTimeCfg;

public class advTimeMan {

	Configuration cfg = Configuration.getInstance();
	
	public boolean waitUntilNextTick(){
		
		TimeInst nextTime = getNextTickTime();
		try {
				
			if (OnlineTimeCfg.fullSimulatedOnlineMode == true)
				Thread.sleep(nextTime.timeInMs);

		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		return nextTime.dailyTick;
	}
	
	public String getCurrentTime(){
				
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy mm dd HH:mm:ss");
		Calendar calendar = new GregorianCalendar();
		
		return sdf.format(calendar.getTime());
		
	}
	
	public String printTime(Calendar time){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		return sdf.format(time.getTime());
	
	}
	
	// Return number of milliseconds until next iteration time
	public TimeInst getNextTickTime(){
		
		// get current time in milliseconds:
		Long currTimeMillis = Calendar.getInstance().getTimeInMillis();
		
		
		// Close trade time:
		// -----------------
		Calendar tradeCloseTime = GregorianCalendar.getInstance();
		String[] tradeCloseString;
		
		// Open trade time:
		// ----------------
		Calendar tradeOpenTime = GregorianCalendar.getInstance();
		String[] tradeOpenString;
		
		tradeOpenString = OnlineTimeCfg.algoTradeStart.split(":");
		tradeCloseTime.set(Calendar.HOUR_OF_DAY,Integer.parseInt(tradeOpenString[0]));
		tradeCloseTime.set(Calendar.MINUTE,Integer.parseInt(tradeOpenString[1]));
		
		
		// Check for halfDay:
		String[] halfDayStringArr = OnlineTimeCfg.HalfDay;
		boolean halfDayFound = false;
		
		for (String nextHalfDay : halfDayStringArr){
			
			String[] nextHalfDayParse = nextHalfDay.split("\\.");
			Integer month   = Integer.parseInt(nextHalfDayParse[1]) - 1;
			Integer day		= Integer.parseInt(nextHalfDayParse[0]);
			
			if ( (Calendar.getInstance().get(Calendar.MONTH) == month) &
					(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == day)){
				
				// This is half day configure closeTrade accordingly:
				tradeCloseString = OnlineTimeCfg.halfDayClose.split(":");	
				tradeCloseTime.set(Calendar.HOUR_OF_DAY,Integer.parseInt(tradeCloseString[0]));
				tradeCloseTime.set(Calendar.MINUTE,Integer.parseInt(tradeCloseString[1]));
				halfDayFound = true;
				break;
			}
			
		}
		
		if (halfDayFound == false){
			
			// Normal trade day:
			tradeCloseString = OnlineTimeCfg.tradeClose.split(":");	
			tradeCloseTime.set(Calendar.HOUR_OF_DAY,Integer.parseInt(tradeCloseString[0]));
			tradeCloseTime.set(Calendar.MINUTE,Integer.parseInt(tradeCloseString[1]));
		
		}
		
		// Trade day is over
		// calculate next trade day:
		Calendar nextTradeDayTime = GregorianCalendar.getInstance();
		
		// Check if trade week is over(FRIDAY,SATURDAY):
		if (
				nextTradeDayTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
				||
				nextTradeDayTime.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
			){
		
			// Trade week is over
			nextTradeDayTime.add(Calendar.DAY_OF_MONTH, 2);
			nextTradeDayTime.set(Calendar.HOUR_OF_DAY, 17);
			nextTradeDayTime.set(Calendar.MINUTE, 0);
			nextTradeDayTime.set(Calendar.SECOND, 0);
			
			System.out.println("Waiting for next trade date: " + printTime(nextTradeDayTime));
			System.out.println("next tick is price tick: " + printTime(nextTradeDayTime));
			return new TimeInst(false,nextTradeDayTime.getTimeInMillis() - currTimeMillis);
			
		}
		
		// checking if next day is holiday:
		boolean nextDayIsHoliday = false;
		for ( String holidayString : OnlineTimeCfg.TradeClose ){
			
			String[] holidayDate = holidayString.split("\\.");
			
			if (
					(nextTradeDayTime.get(Calendar.MONTH) == (Integer.parseInt(holidayDate[1])-1)) 
					&&
					(nextTradeDayTime.get(Calendar.DAY_OF_MONTH) == Integer.parseInt(holidayDate[0]))
				){
				// Next day is a holiday, adding one day
				nextTradeDayTime.add(Calendar.DAY_OF_MONTH, 1);
				nextDayIsHoliday = true; 
			}

		}
		
		if ( nextDayIsHoliday == true ){
			
			System.out.println("Waiting for next trade date: " + printTime(nextTradeDayTime));
			System.out.println("next tick is price tick: " + printTime(nextTradeDayTime));
			System.out.println("Skipping trade day/s close due to holiday/s");
			return new TimeInst(false,nextTradeDayTime.getTimeInMillis() - currTimeMillis);

		}
		
		// Check if trade day is over: 
		if (currTimeMillis > tradeCloseTime.getTimeInMillis()){
			
			// Wait for next trade day, not a daily tick
			if (OnlineTimeCfg.fullSimulatedOnlineMode == true){
				
				System.out.println("Waiting for next trade date: " + printTime(nextTradeDayTime));
				System.out.println("next tick is price tick: " + printTime(nextTradeDayTime));
				return new TimeInst(false,nextTradeDayTime.getTimeInMillis() - currTimeMillis);
			
			}
			
		// Trade day in not over, check if trade day had not begin yet:
		} else if ( currTimeMillis < tradeOpenTime.getTimeInMillis()){
			
			// Trade day had not begin yet (this can happen only when executing program for the first time
			if (OnlineTimeCfg.fullSimulatedOnlineMode == true){
				
				System.out.println("Waiting for next trade date: " + printTime(tradeOpenTime));
				System.out.println("next tick is price tick" );
				System.out.println("first time executing, time might not be accurate");
				
				return new TimeInst(false,tradeOpenTime.getTimeInMillis() - currTimeMillis);

			}
		}
				
		// Inside trade day
		// get amount of minutes until next iteration
		String[] nextIterationString = OnlineTimeCfg.algoItration.split(":");
		Integer nextIterationMinutes = Integer.parseInt(nextIterationString[0])*60 + 
				Integer.parseInt(nextIterationString[1]);
		
		// Calculate time until next iteration:
		Calendar nextTime = GregorianCalendar.getInstance();
		nextTime.add(Calendar.MINUTE, nextIterationMinutes);
		
	
		// Check if next iteration should occur before trade day is over 
		if ( nextTime.getTime().compareTo(tradeCloseTime.getTime()) < 0 ){
			
			if (OnlineTimeCfg.fullSimulatedOnlineMode == true){
					
				System.out.println("Waiting for next trade date: " + printTime(tradeOpenTime));
				System.out.println("next tick is price tick");
				System.out.println("next iteration should occur before trade day is over");
				return new TimeInst(false,nextTime.getTimeInMillis() - currTimeMillis);
		
			}
			
		} else {
			
			// next iteration is after day trade is close, waiting until daily tick time, indication daily tick
			String[] dailyTickTimeString = OnlineTimeCfg.dailyTickTime.split(":");
			
			Calendar nextDailyTime = GregorianCalendar.getInstance();
			nextDailyTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dailyTickTimeString[0]));
			nextDailyTime.set(Calendar.MINUTE, Integer.parseInt(dailyTickTimeString[1]));
			
			if (OnlineTimeCfg.fullSimulatedOnlineMode == true){
				
				System.out.println("next tick time: " + printTime(nextDailyTime));
				System.out.println("next tick is daily tick");
				return new TimeInst(true,nextDailyTime.getTimeInMillis() - currTimeMillis);
			
			}
			
		}
		
		System.err.println("__ERR__ in online time manager");
		System.exit(1);		
		return new TimeInst();
	}
}
