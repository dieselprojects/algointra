package com.dieselProjects.supportCluster;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.dieselProjects.beans.Tick;
import com.dieselProjects.configurationCluster.Configuration;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class Screener {

    /*
     * Basic implementation:
     * Start at the beginning of each test, 
     * for each ticker in Res/companylist.csv:
     *  1. check if stock has volume above MIN_VOLUME_FOR_TRADE
     *  2. check if stock price above MIN_STOCK_PRICE_FOR_TRADE
     * if all true append to Res/filteredStocks.csv 
     */

    private static final String COMPANY_LIST            = "Res/companylist.csv"     ;
    private static final String FILTERED_COMPANY_LIST   = "Res/filteredStocks.csv"  ;
    private static final int    REQ_INTERVAL            = 250                       ;


    Configuration configuration = Configuration.getInstance();

    // Filtered Indicators:
    Long minVolume;
    Double minPrice;

    public void screenStocks(){

        CSVReader reader;
        int cnt = 0;
        String nextStockTicker = null;
        String[] requestPricesArray = new String[REQ_INTERVAL];
        ArrayList<String> filteredTickers = new ArrayList<String>();

        try {
            reader = new CSVReader(new FileReader(COMPANY_LIST));
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                nextStockTicker = nextLine[0];

                // Internal loop each 250 tickers send yahoo prices/volumes request
                requestPricesArray[cnt] = nextStockTicker;
                cnt+=1;
                if (cnt == REQ_INTERVAL){
                    cnt = 0;
                    ArrayList<Tick> stockTicks = requestPrices(requestPricesArray);
                    for (Tick nextTick : stockTicks){
                        if ((nextTick.volume >= configuration.screenerCfg.filteringVolume) &&
                                (nextTick.close() >= configuration.screenerCfg.filteringPrice)){
                            filteredTickers.add(nextTick.ticker);
                        }
                    }
                }
            } 

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Write result to file:
        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(FILTERED_COMPANY_LIST), '\t');
            // feed in your array (or convert your data to an array)
            String[] writeFilteredArray = new String[filteredTickers.size()];
            filteredTickers.toArray(writeFilteredArray);
            writer.writeNext(writeFilteredArray);
            writer.close(); 
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }	

    public ArrayList<Tick> requestPrices(String[] tickers) {

        // create a request for yahoo with all given ticker - up to 150 tickers at a time
        // send request for yahoo for current prices or daily prices
        // Called should not send a request for more than 100 tickers
        // Added implementation for simulated ticks (dailyTicks only)

        ArrayList<Tick> returnedTickList = new ArrayList<Tick>();

        String requestOperands = "sl1v"; // Ticker, latest price, volume

        String tickerString = "";
        for (String nextTicker : tickers){
            tickerString += "+" +nextTicker;
        }
        // remove first '+'
        tickerString = tickerString.substring(1);

        try {

            // Generate yahoo price call:
            // -------------------------- 

            URL yahooPricesCall;
            yahooPricesCall = new URL("http://finance.yahoo.com/d/quotes.csv?s=" + tickerString + "&f=" + requestOperands + "&ignore=.csv");

            URLConnection yc = yahooPricesCall.openConnection();

            // Read Input Stream:
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

            String pricesLine = "";
            while ( (pricesLine = in.readLine()) != null ){
                String[] splitString = pricesLine.split(",");

                // Current request time
                Calendar currentTime = new GregorianCalendar();

                Tick nextTick;
                if ( (splitString[0] == "N/A") || (splitString[1] == "N/A") || (splitString[2] == "N/A") ){
                    System.out.println("Recived request with N/A");
                } else {
                    try {
                        String ticker = splitString[0].replace("\"", "");
                        nextTick = new Tick(ticker,
                                Double.valueOf(splitString[1]),
                                0.0, 0.0, 0.0, Integer.valueOf(splitString[2]),
                                currentTime);

                        // Add to returned List
                        returnedTickList.add(nextTick);
                    } catch (Exception e){
                        System.out.println("Screener error " + splitString[0] + " " 
                                + splitString[1] + " "
                                + splitString[2] );
                    }
                }
            }
        } catch (MalformedURLException e) {			
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);				
        }

        return returnedTickList;
    }
}
