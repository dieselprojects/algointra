package com.dieselProjects.supportCluster;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.supportCluster.Types.OPType;

public class webConnection {

	// Database CONSTANTS:
	// -------------------
	private static String DATABASE = "algoTraderMySQL";
	private String TABLE_PENDING = "StocksPending";
	public String TABLE_EXECUTED = "StocksExecute";

	// Table CONSTANSTS:
	// -----------------
	private static final String OPERATION = "'Buy','Sell'";

	String nextStmt = null;

	private Connection connect = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	static final String CONNECTION = "jdbc:mysql://localhost/" + DATABASE
			+ "?user=root&password=idan1980";

	public void createMySQL() {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/",
					"idanhahn", "idan1980");

			// If Exist delete previous DataBase by the name "DATABASE"

			// Creating DataBase:
			// ------------------

			nextStmt = "CREATE DATABASE " + DATABASE;
			preparedStatement = connect.prepareStatement(nextStmt);
			preparedStatement.execute();

			// Creating Table Pending:
			// -----------------------

			nextStmt = "CREATE TABLE "
					+ DATABASE
					+ "."
					+ TABLE_PENDING
					+

					// General Stock Parameters:
					// -------------------------

					" (Ticker varchar(15) NOT NULL UNIQUE, "
					+ "Price float UNSIGNED NOT NULL, "
					+ "Amount integer UNSIGNED NOT NULL, "
					+ "Operation ENUM("
					+ OPERATION
					+ ") NOT NULL,"
					+ "Grade float UNSIGNED, "
					+ "GraphURLLines1 varchar(60) NOT NULL, "
					+ "GraphURLLines2 varchar(60) NOT NULL, "
					+ "GraphURLSticks1 varchar(60) NOT NULL, "
					+ "GraphURLSticks2 varchar(60) NOT NULL, "
					+

					// Additional Sell Parameters:
					// ---------------------------

					"BuyPrice float UNSIGNED, "
					+ "BuyAmount integer UNSIGNED, " + "PRIMARY KEY (Ticker)"
					+ ")";

			preparedStatement = connect.prepareStatement(nextStmt);
			preparedStatement.execute();

			// Create Table Executed:
			// ----------------------

			nextStmt = "CREATE TABLE "
					+ DATABASE
					+ "."
					+ TABLE_EXECUTED
					+

					// General Stock Parameters:
					// -------------------------

					" (Ticker varchar(15) NOT NULL UNIQUE, "
					+ "PriceRange float UNSIGNED , "
					+ "AmountToEXE integer UNSIGNED NOT NULL, "
					+ "Approve BIT NOT NULL, " + "PRIMARY KEY (Ticker)" + ")";

			preparedStatement = connect.prepareStatement(nextStmt);
			preparedStatement.execute();

		} catch (ClassNotFoundException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
			destroyMySQL();
			System.exit(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			destroyMySQL();
			System.exit(1);
		}

	}

	public void destroyMySQL() {

		String nextStmt = null;

		try {

			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/",
					"idanhahn", "idan1980");

			nextStmt = "DROP DATABASE " + DATABASE;
			preparedStatement = connect.prepareStatement(nextStmt);
			preparedStatement.execute();

			connect.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);

		}
	}

	public void sendBidRequestMySQL(Bid nextBid) {

		try {

			connect = DriverManager.getConnection(CONNECTION);
			connect.createStatement();

			//if (nextBid.bidType == OPType.SELL) {
			//	preparedStatement = connect
			//			.prepareStatement("insert into "
			//					+ TABLE_PENDING
			//					+ " (Ticker, Price, Amount, Operation, Grade, GraphURLLines1, GraphURLLines2, GraphURLSticks1, "
			//					+ "GraphURLSticks2, BuyPrice, BuyAmount) values (?,?,?,?,?,?,?,?,?,?,?)");
			//} else {
			//	preparedStatement = connect
			//			.prepareStatement("insert into "
			//					+ TABLE_PENDING
			//					+ " (Ticker, Price, Amount, Operation, Grade, GraphURLLines1, GraphURLLines2, GraphURLSticks1, "
			//					+ "GraphURLSticks2) values (?,?,?,?,?,?,?,?,?)");
			//}

			//preparedStatement.setString(1, nextBid.ticker);
			//preparedStatement.setFloat(2, nextBid.price.floatValue());
			//preparedStatement.setInt(3, nextBid.amount);
			//preparedStatement.setString(4, nextBid.bidType.toString());
			//preparedStatement.setFloat(5, nextBid.stock.grade.floatValue());
			//preparedStatement.setString(6, "TBD");
			//preparedStatement.setString(7, "TBD");
			//preparedStatement.setString(8, "TBD");
			//preparedStatement.setString(9, "TBD");
			//if (nextBid.bidType == OPType.SELL) {
			//	preparedStatement.setFloat(10,
			//			new Float(nextBid.stock.buyPrice));
			//	preparedStatement.setInt(11, nextBid.stock.investedAmount);
			//}

			preparedStatement.executeUpdate();
			connect.close();

		} catch (Exception e) {
			System.err.println(e.toString());
			destroyMySQL();
			System.exit(1);

		}
	}

	public void sendDeleteBidRequest(Bid nextBid) {
		try {

			connect = DriverManager.getConnection(CONNECTION);
			connect.createStatement();

			preparedStatement = connect.prepareStatement("DELETE FROM "
					+ TABLE_EXECUTED + " WHERE Ticker ='" + nextBid.ticker
					+ "'");

			preparedStatement.executeUpdate();
			connect.close();

		} catch (Exception e) {
			System.err.println(e.toString());
			destroyMySQL();
			System.exit(1);

		}

	}

	public int getTableSize(String TABLE) {

		int size = 0;

		try {
			connect = DriverManager.getConnection(CONNECTION);
			String nextStmt = "SELECT * from " + TABLE;

			preparedStatement = connect.prepareStatement(nextStmt);
			resultSet = preparedStatement.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			destroyMySQL();
			System.exit(1);
		}

		try {

			if (resultSet.last()) {
				size = resultSet.getRow();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			destroyMySQL();
			System.exit(1);
		}

		try {
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			destroyMySQL();
			System.exit(1);
		}

		return size;

	}

	public void sendMail() {
		final String username = "idanhahn@gmail.com";
		final String password = "ghsi1980";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		System.out.println();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-email@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("idanalgotrader@gmail.com"));
			message.setSubject("Testing Sync");
			message.setText("message," + "\n\n"
					+ dateFormat.format(cal.getTime()));

			// Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}
