package com.dieselProjects.controllerCluster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.brokerCluster.TWSBroker;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.confirmation.AutoConfirm;
import com.dieselProjects.confirmation.HumanIF;
import com.dieselProjects.confirmation.HumanShellConfirm;
import com.dieselProjects.confirmation.WindowHumanConfirm;
import com.dieselProjects.confirmation.webConfirm;
import com.dieselProjects.dataBase.DataBaseController;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.SimulateTimeMan;
import com.dieselProjects.supportCluster.TimeMan;
import com.dieselProjects.supportCluster.webConnection;
import com.dieselProjects.supportCluster.Types.ConfirmationType;

public class TWSAlgoTrade {

	// ------------------- //
	// - Class Variables - //
	// ------------------- //

	// Controller cluster Components:
	// ------------------------------
	TWSController twsController;
	public DataBaseController dataBaseController;

	// Algorithm Cluster components:
	// -----------------------------

	// Broker cluster Components:
	// --------------------------

	public TWSBroker broker;

	// Support cluster components:
	// ---------------------------

	FundsMan funds;
	HumanIF humanConfirm;
	TimeMan timeMan;
	SimulateTimeMan simTimeMan;
	Configuration configuration;
	Logger logger;
	webConnection webConnection;

	// --------------- //
	// - Constructor - //
	// --------------- //

	public TWSAlgoTrade() {

		// Support modules:
		// ----------------

		System.out.println("__Constructor__ TWS AlgoTrade");

		System.out.println("Creating Configuration Tree");
		configuration = Configuration.getInstance();

		switch (configuration.confirmationType) {
		case AUTO_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Auto confirmation");
			humanConfirm = new AutoConfirm();
			break;
		case SHELL_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Shell confirmation");
			humanConfirm = new HumanShellConfirm();
			break;
		case GUI_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, GUI confirmation");
			humanConfirm = new WindowHumanConfirm();
			break;
		case WEB_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, Web Confirmation");
			humanConfirm = new webConfirm();
			webConnection = new webConnection();
			break;
		default:
			System.err.println("Not valid confirmationType selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}

		System.out.println("Creating Time Manager Module");
		timeMan = new TimeMan();

		System.out.println("Creating Simulate Time Manager Module");
		simTimeMan = SimulateTimeMan.getInstance();

		// Broker Cluster:
		// ---------------
		System.out.println("Creating Simulated Broker Cluster");
		broker = TWSBroker.getInstance();

		System.out.println("Creating Logger Module");
		logger = Logger.getInstance();

		System.out
				.println("Creating Funds Manager Module with Initial funds of "
						+ configuration.initialFunds);
		funds = FundsMan.getInstance();

		// Controller Cluster:
		// -------------------
		System.out.println("Creating DataBase Controller");
		dataBaseController = new DataBaseController("DataBaseController");

		System.out.println("Creating simulate controller");
		twsController = new TWSController(dataBaseController, broker,
				humanConfirm, funds, logger);

		System.out.println("Finished Simulated AlgoTrade Constructor");
	}

	public void Start() {

		logger.addEvent("*********************************************");
		logger.addEvent("**    Simulatd ALGOTRADER - SETUP PHASE   **");
		logger.addEvent("********************************************");

		Setup();

		logger.addEvent("*******************************************");
		logger.addEvent("**  Simulated ALGOTRADER - TRADE PHASE   **");
		logger.addEvent("*******************************************");

		Trade();

		logger.addEvent("********************************************");
		logger.addEvent("**   Simulated ALGOTRADER - FINAL PHASE   **");
		logger.addEvent("********************************************");

		Final();

	}

	// --------- //
	// - SETUP - //
	// --------- //

	private void Setup() {
		// Register all activated stocks for Real time bars
	
		
		List<String> tickers = broker.getAllTickers(true);

		List<StockBean> stockBeanArray = new ArrayList<StockBean>();
		Iterator<String> itr = tickers.iterator();
		while (itr.hasNext()){
			stockBeanArray.add(new StockBean(itr.next()));
		}
		
		dataBaseController.addStockBeans(stockBeanArray);
		
		//RegisterTWS reg = new RegisterTWS(broker,new LinkedList<String>(tickers));
		//Thread regThread = new Thread(reg);
		//regThread.start();
		broker.registerRealTimeBars(tickers);
	}

	// ----------------- //
	// - Trade phase - //
	// ----------------- //

	private void Trade() {

		
	}

	// --------- //
	// - FINAL - //
	// --------- //

	private void Final() {

		// for web confirmation, destroy SQL database
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION) {
			webConnection.destroyMySQL();
		}

		if (configuration.enablePrintToScreen == true)
			System.out.println(logger.printAll(dataBaseController
					.getAllStockBean()));
	}

	// Convert Tick to stockBean:
	// --------------------------

	private StockBean convertDailyTickToBean(Tick tick) {
		StockBean bean = new StockBean(tick);
		return bean;
	}

	private List<StockBean> convertDailyTickToBean(List<Tick> tickArr) {

		List<StockBean> beanArr = new ArrayList<StockBean>(tickArr.size());

		for (Tick nextTick : tickArr) {
			beanArr.add(convertDailyTickToBean(nextTick));
		}

		return beanArr;
	}
}
