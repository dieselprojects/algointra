package com.dieselProjects.controllerCluster;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

import com.dieselProjects.brokerCluster.TWSBroker;

public class RegisterTWS implements Runnable {

	TWSBroker broker;
	LinkedList<String> tickers;
	
	
	public RegisterTWS(TWSBroker broker,LinkedList<String> tickers){
		this.broker = broker;
		this.tickers = tickers;
	}
	
	
	@Override
	public void run() {
		registerRealTimeBars(tickers);
	}

	public void registerRealTimeBars(LinkedList<String> tickers){
	
		broker.registerRealTimeBars(tickers);
				
	}
}
