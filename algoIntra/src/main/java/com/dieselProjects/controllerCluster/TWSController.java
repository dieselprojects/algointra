package com.dieselProjects.controllerCluster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.dieselProjects.algoCluster.AlgorithmCore;
import com.dieselProjects.algoCluster.CreateBidsBuyBasic;
import com.dieselProjects.algoCluster.CreateBidsIF;
import com.dieselProjects.algoCluster.CreateBidsSellBasic;
import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.brokerCluster.TWSBroker;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.confirmation.HumanIF;
import com.dieselProjects.dataBase.DataBaseController;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.OnlineTimeMan;

public class TWSController {

	DataBaseController databaseController;
	TWSBroker broker;
	AlgorithmCore algorithmCore;
	CreateBidsIF createBidsBuy;
	CreateBidsIF createBidsSell;
	HumanIF humanIF;
	FundsMan funds;
	OnlineTimeMan onlineTimeMan = OnlineTimeMan.getInstance();
	
	Logger logger;
	Configuration configuration;

	public TWSController(DataBaseController databaseController, TWSBroker broker,
			HumanIF humanIF, FundsMan funds, Logger logger) {

		System.out.println("__Constructor__ Trade Abstract");
		this.databaseController = databaseController;
		this.broker = broker;
		this.humanIF = humanIF;
		this.funds = funds;
		this.logger = logger;
		configuration = Configuration.getInstance();
		createBidsBuy = new CreateBidsBuyBasic();
		createBidsSell = new CreateBidsSellBasic();
		broker.twsController = this;
		broker.ticksCollector.twsController = this;
		
	}

	public List<StockBean> startIteration(List<Tick> tickList){

		// Get next tick data from broker:
		databaseController.updateBeanPrices(tickList);	
		
		// Exec algorithm on stocks with new data:
		List<StockBean> stocksArray = databaseController.getAllStockBean();
		algorithmCore = new AlgorithmCore(stocksArray,createBidsBuy,createBidsSell);
		List<Bid> algoBidList = algorithmCore.start(onlineTimeMan.getTime());

		// send human confirm:
		List<Bid> humanConfirmedBids = humanIF.getConfirmation(algoBidList);
		
		// Send algorithm bid list to broker for exec:
		List<Bid> doneBids = broker.placeBids(humanConfirmedBids);
		
		List<StockBean> returnStockList = new ArrayList<StockBean>();
		
		Iterator<Bid> iterator = doneBids.iterator();
		while(iterator.hasNext()){
			returnStockList.add(iterator.next().stock);
		}
		
		if (stocksArray.get(0).numOfSamples == 4){
			System.out.println("4 samples");
		}
	
		return returnStockList;
		
	}
}
