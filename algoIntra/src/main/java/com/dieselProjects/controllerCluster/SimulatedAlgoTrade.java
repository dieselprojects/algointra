package com.dieselProjects.controllerCluster;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.brokerCluster.SimulatedBroker;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.confirmation.AutoConfirm;
import com.dieselProjects.confirmation.HumanIF;
import com.dieselProjects.confirmation.HumanShellConfirm;
import com.dieselProjects.confirmation.WindowHumanConfirm;
import com.dieselProjects.confirmation.webConfirm;
import com.dieselProjects.dataBase.DataBaseController;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.SimulateTimeMan;
import com.dieselProjects.supportCluster.TimeMan;
import com.dieselProjects.supportCluster.webConnection;
import com.dieselProjects.supportCluster.Types.ConfirmationType;

public class SimulatedAlgoTrade {

	// ------------------- //
	// - Class Variables - //
	// ------------------- //

	// Controller cluster Components:
	// ------------------------------
	SimController simController;
	public DataBaseController dataBaseController;

	// Algorithm Cluster components:
	// -----------------------------


	// Broker cluster Components:
	// --------------------------

	public SimulatedBroker broker;

	// Support cluster components:
	// ---------------------------

	FundsMan funds;
	HumanIF humanConfirm;
	TimeMan timeMan;
	SimulateTimeMan simTimeMan;
	Configuration configuration;
	Logger logger;
	webConnection webConnection;

	// --------------- //
	// - Constructor - //
	// --------------- //

	public SimulatedAlgoTrade() {

		// Support modules:
		// ----------------

		System.out.println("__Constructor__ Simulated AlgoTrade");

		System.out.println("Creating Configuration Tree");
		configuration = Configuration.getInstance();

		switch (configuration.confirmationType) {
		case AUTO_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Auto confirmation");
			humanConfirm = new AutoConfirm();
			break;
		case SHELL_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Shell confirmation");
			humanConfirm = new HumanShellConfirm();
			break;
		case GUI_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, GUI confirmation");
			humanConfirm = new WindowHumanConfirm();
			break;
		case WEB_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, Web Confirmation");
			humanConfirm = new webConfirm();
			webConnection = new webConnection();
			break;
		default:
			System.err.println("Not valid confirmationType selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}

		System.out.println("Creating Time Manager Module");
		timeMan = new TimeMan();

		System.out.println("Creating Simulate Time Manager Module");
		simTimeMan = SimulateTimeMan.getInstance();

		// Broker Cluster:
		// ---------------
		System.out.println("Creating Simulated Broker Cluster");
		broker = SimulatedBroker.getInstance();

		System.out.println("Creating Logger Module");
		logger = Logger.getInstance();

		System.out
				.println("Creating Funds Manager Module with Initial funds of "
						+ configuration.initialFunds);
		funds = FundsMan.getInstance();

		// Controller Cluster:
		// -------------------
		System.out.println("Creating DataBase Controller");
		dataBaseController = new DataBaseController("DataBaseController");

		System.out.println("Creating simulate controller");
		simController = new SimController(dataBaseController, broker,
				humanConfirm, funds, logger);

		System.out.println("Finished Simulated AlgoTrade Constructor");
	}

	public void SimulationStarter() {

		logger.addEvent("*********************************************");
		logger.addEvent("**    Simulatd ALGOTRADER - SETUP PHASE   **");
		logger.addEvent("********************************************");

		Setup();

		logger.addEvent("*******************************************");
		logger.addEvent("**  Simulated ALGOTRADER - TRADE PHASE   **");
		logger.addEvent("*******************************************");

		Trade();

		logger.addEvent("********************************************");
		logger.addEvent("**   Simulated ALGOTRADER - FINAL PHASE   **");
		logger.addEvent("********************************************");

		Final();

	}

	// --------- //
	// - SETUP - //
	// --------- //

	private void Setup() {
		// Add StocksBean to DataBase and add prices,

		// Simulated Algo Setup phase:
		// ---------------------------

		// At this point there's full broker database ready.
		// a. Call Broker and get all stored Tickers.?? not sure if it's should
		// be in the broker class
		// b. use tickers to Call Broker and get DailyTicks
		// c. AdvIteration
		// d. call sub method Convert Tick to StockBean
		// e. Use ConvertMethod to create ArrayList of StockBean
		// f. Use ArrayList of StockBeans to push into the DataBase
		// g. use addStockBean to #setupIteration to GeneralDataBase.

		// ------------------------------------- //
		// -- Specific Additional Setup Tasks -- //
		// ------------------------------------- //

		// Add WebConnection Interface:
		// ----------------------------
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION) {
			webConnection.sendMail();
			webConnection.createMySQL();
		}

		// -------------------------------- //
		// -- End Additional Setup Tasks -- //
		// -------------------------------- //

		// a. Call Broker and get all stored Tickers.?? not sure if it's should
		// be in the broker class
		List<String> allTickerArr = broker.getAllTickers(false);

		// b. use tickers to Call Broker and get DailyTicks
		List<Tick> firstTickArr = broker.getTicks(allTickerArr);

		// c. AdvIteration
		// Broker.advIteration();

		// d. call sub method Convert Tick to StockBean
		// e. Use ConvertMethod to create ArrayList of StockBean

		List<StockBean> newStockBeanArr = convertDailyTickToBean(firstTickArr);

		// f. Use ArrayList of StockBeans to push into the DataBase

		dataBaseController.addStockBeans(newStockBeanArr);

		// g. use addStockBean to add all.

		for (int i = 1; i < Configuration.getInstance().setupTicks; i++) {

			List<Tick> dailyTickArr = broker.getTicks(broker
					.getAllTickers(false));
			dataBaseController.updateBeanPrices(dailyTickArr);

			broker.advIteration();

		}

	}

	// ----------------- //
	// - Trade phase - //
	// ----------------- //

	private void Trade() {

		while (broker.advIteration()) {

			timeMan.waitUntilNextTick();
			String tradePhase = "--------------------------------------" + "\n"
					+ "--    IN TRADE PHASE, ITERATION "
					+ broker.getIteration() + "  --" + "\n" + "--  "
					+ simTimeMan.getTimeString() + "  --" + "\n"
					+ "--------------------------------------" + "\n";
			logger.addEvent(tradePhase);
			// System.out.println(tradePhase);

			// getting and updating dataBase for the next tick
			dataBaseController.updateBeanPrices(
					broker.getTicks(
							dataBaseController.getAllStocksBean()
					)
			);

			simController.isEOT = broker.getEOT();
			
			// after updating dataBase starting iteration with all stock beans
			dataBaseController.addStockBeans(
					simController.startIteration(
							dataBaseController.getAllStockBean()
					)
			);

		}
	}

	// --------- //
	// - FINAL - //
	// --------- //

	private void Final() {

		// for web confirmation, destroy SQL database
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION) {
			webConnection.destroyMySQL();
		}

		if (configuration.enablePrintToScreen == true)
			System.out.println(logger.printAll(dataBaseController
					.getAllStockBean()));
	}

	// Convert Tick to stockBean:
	// --------------------------

	private StockBean convertDailyTickToBean(Tick tick) {
		StockBean bean = new StockBean(tick);
		return bean;
	}

	private List<StockBean> convertDailyTickToBean(List<Tick> tickArr) {

		List<StockBean> beanArr = new ArrayList<StockBean>(tickArr.size());

		for (Tick nextTick : tickArr) {
			beanArr.add(convertDailyTickToBean(nextTick));
		}

		return beanArr;
	}

}
