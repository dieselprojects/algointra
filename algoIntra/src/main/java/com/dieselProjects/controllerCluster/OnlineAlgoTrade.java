package com.dieselProjects.controllerCluster;

import java.util.ArrayList;
import com.dieselProjects.brokerCluster.BrokerIF;
import com.dieselProjects.brokerCluster.TWSBroker;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.confirmation.AutoConfirm;
import com.dieselProjects.confirmation.HumanIF;
import com.dieselProjects.confirmation.HumanShellConfirm;
import com.dieselProjects.confirmation.WindowHumanConfirm;
import com.dieselProjects.confirmation.webConfirm;
import com.dieselProjects.dataBase.DataBaseController;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.advTimeMan;
import com.dieselProjects.supportCluster.webConnection;

public class OnlineAlgoTrade {

	// ------------------- //
	// - Class Variables - //
	// ------------------- //

	// Controller cluster components:
	// ------------------------------

	OnlineController onlineController;
	DataBaseController dataBaseController;

	// Algorithm Cluster components:
	// -----------------------------


	// Broker cluster Components:
	// --------------------------

	// SimulatedBroker Broker;
	BrokerIF broker;

	// Support cluster components:
	// ---------------------------

	FundsMan funds;
	HumanIF humanConfirm;
	advTimeMan timeMan;
	Configuration configuration;
	Logger logger;
	webConnection webConnection;

	// --------------- //
	// - Constructor - //
	// --------------- //

	public OnlineAlgoTrade() {

		System.out.println("__Constructor__ Online algotrader");

		System.out.println("Creating Configuration Tree");
		configuration = Configuration.getInstance();

		switch (configuration.confirmationType) {
		case AUTO_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Auto confirmation");
			humanConfirm = new AutoConfirm();
			break;
		case SHELL_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Shell confirmation");
			humanConfirm = new HumanShellConfirm();
			break;
		case GUI_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, GUI confirmation");
			humanConfirm = new WindowHumanConfirm();
			break;
		case WEB_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, Web Confirmation");
			humanConfirm = new webConfirm();
			webConnection = new webConnection();
			break;
		default:
			System.err.println("Not valid confirmationType selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}

		System.out.println("Creating Time Manager Module");
		timeMan = new advTimeMan();

		System.out.println("Creating Logger Module");
		logger = Logger.getInstance();

		System.out
				.println("Creating Funds Manager Module with Initial funds of "
						+ configuration.initialFunds);
		funds = FundsMan.getInstance();

		System.out.println("Creating Broker module, TWS Broker");
		broker = TWSBroker.getInstance();

		// Controller Cluster:
		// -------------------

		System.out.println("Creating DataBase Controller");
		dataBaseController = new DataBaseController("DataBaseController");

		System.out.println("Creating Online Controller");
		onlineController = new OnlineController(dataBaseController, broker,
				humanConfirm, funds, logger);

		System.out.println("Finished Simulated AlgoTrade Constructor");

	}

	public void AlgoStarter() {

		System.out.println("___Start AlgoTrader___");

		System.out.println("******************************************");
		System.out.println("**    ONLINE ALGOTRADER - SETUP PHASE   **");
		System.out.println("******************************************");
		logger.addEvent("__ONLINE ALGOTRADER__ AlgoStarter: setup phase");

		Setup();

		System.out.println("****************************************");
		System.out.println("**  ONLINE ALGOTRADER - TRADE PHASE   **");
		System.out.println("****************************************");
		logger.addEvent("__ONLINE ALGOTRADER__ AlgoStarter: trade phase");

		Trade();

	}

	// --------- //
	// - Setup - //
	// --------- //

	// For intra day trade assuming no need to load data base
	private void Setup() {
		System.out.println("Entering setup phase Online Controller");
		System.out.println("Currentlly nothing is done");
		System.out.println("Exitng setup phase Online Controller");
		
		System.out.println("Debug - remove");
		ArrayList<String> tickers = new ArrayList<String>();
		tickers.add("AAPL");
		tickers.add("FSL");
		tickers.add("AMZN");
		tickers.add("FB");
		tickers.add("INTC");
		tickers.add("DIS");
		tickers.add("MSFT");
		tickers.add("ACCL");
		tickers.add("ACUR");
		tickers.add("YPRO");
		tickers.add("ADVS");
		tickers.add("ATRM");
		tickers.add("ALIM");
		tickers.add("ALCO");
		tickers.add("ALKS");
		tickers.add("ADHD");
		tickers.add("YHOO");
		tickers.add("ARMH");
		tickers.add("ANAT");
		
		broker.getTicks(tickers);
		
		
		System.out.println("Until here");
		
	}

	// --------- //
	// - Trade - //
	// --------- //

	public void Trade() {

		boolean isDailyTick;

		while (true) {

			isDailyTick = timeMan.waitUntilNextTick();

			String isDailyTickString = (isDailyTick == true) ? "DailyTick"
					: "priceTick";

			String nextEventStr = "\nTRADE PHASE, " + timeMan.getCurrentTime()
					+ " , " + isDailyTickString + "\n";
			logger.addEvent(nextEventStr);

			if (isDailyTick == true) {

				// TODO: need to think what to do here, probably store and
				// reset data base for next day trade
			} else {

				// Actual trade here
				
				String tradePhaseStr = "--------------------------------------"
						+ "\n" + "--    IN TRADE PHASE, ITERATION "
						+ "ADD TIME" + "  --" + "\n"
						+ "--------------------------------------" + "\n";
				System.out.println(tradePhaseStr);
				dataBaseController.addStockBeans(onlineController
						.startIteration());

			}

		}

	}
}
