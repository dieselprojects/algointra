package com.dieselProjects.controllerCluster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.dieselProjects.algoCluster.AlgorithmCore;
import com.dieselProjects.algoCluster.AlgorithmCoreSim;
import com.dieselProjects.algoCluster.CreateBidsBuyBasic;
import com.dieselProjects.algoCluster.CreateBidsIF;
import com.dieselProjects.algoCluster.CreateBidsSellBasic;
import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.brokerCluster.BrokerIF;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.confirmation.HumanIF;
import com.dieselProjects.dataBase.DataBaseController;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.PerformanceMonitor;
import com.dieselProjects.supportCluster.SimulateTimeMan;

public class SimController {

	Configuration configuration;

	DataBaseController databaseController;
	BrokerIF broker;

	AlgorithmCore algorithmCore;
	CreateBidsIF createBidsBuy;
	CreateBidsIF createBidsSell;
	
	HumanIF humanIF;
	FundsMan funds;
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	Logger logger;
	PerformanceMonitor performanceMonitor = PerformanceMonitor.getInstance();
	
	// only for selling all stocks at the end of test
	public boolean isEOT = false;
	
	

	public SimController(DataBaseController databaseController,
			BrokerIF broker, HumanIF humanIF, FundsMan funds, Logger logger) {

		System.out.println("__Constructor__ Trade Abstract");
		this.databaseController = databaseController;
		this.broker = broker;
		this.humanIF = humanIF;
		this.funds = funds;
		this.logger = logger;
		configuration = Configuration.getInstance();
		
		createBidsBuy = new CreateBidsBuyBasic();
		createBidsSell = new CreateBidsSellBasic();
	
	}

	/*
	 * startIteration()
	 * 
	 * Return: Collection all updated StockBeans
	 * 
	 * Description and phases: startIteration is the main entry point for the
	 * algorithm, it purpose is to apply the algorithm on all stocks receives
	 * grades,create human interface stock buy request and receive reply, create
	 * bids, apply bids receive bid reply and update stocks dataBase.
	 * 
	 * Main phases: 1. get Updated StockBeans (ticks are already inside them) 2.
	 * Apply Algorithm and receives Suggested Bids 3. Human Interface, this
	 * needs to be in a separate thread but!!! no collations 4. Broker request,
	 * this is the most dangerous part need to think what to do here Ideas,
	 * stock state, if it's in pending mode, don't apply algorithm so no
	 */

	public List<StockBean> startIteration(List<StockBean> stocks) {

		AlgorithmCoreSim simAlgorithmCore = new AlgorithmCoreSim(stocks);
		simAlgorithmCore.start(simTimeMan.getTime()); 
		
		
		algorithmCore = new AlgorithmCore(stocks,createBidsBuy,createBidsSell);
				
		List<Bid> algoBidList = algorithmCore.start(simTimeMan.getTime());

		// List<Bid> algoBidList = algorithm.start(simTimeMan.getTime());

		if (isEOT == true) {
			algoBidList.addAll(algorithmCore.sellAll(stocks,simTimeMan.getTime()));
		}

		// send human confirm:
		List<Bid> humanConfirmedBids = humanIF.getConfirmation(algoBidList);

		// Send algorithm bid list to broker for exec:
		List<Bid> doneBids = broker.placeBids(humanConfirmedBids);

		performanceMonitor.gradeBids(doneBids, simTimeMan.getTime());
		
		if (isEOT == true)
			performanceMonitor.gradeEOT(doneBids, simTimeMan.getTime());
		
		
		List<StockBean> returnStockList = new ArrayList<StockBean>();

		Iterator<Bid> iterator = doneBids.iterator();
		while (iterator.hasNext()) {
			// return only the updated stockBeans, not sure if this is correct
			// because dataBase will only store trades stocks
			returnStockList.add(iterator.next().stock);
		}

		return returnStockList;

	}

}
