package com.dieselProjects.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.dieselProjects.beans.Bid;

public class LineChart extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Main Panel
	private JPanel mainPanel = null;
	
	// HeadLineLabel
	private JLabel headLineLB = null;
	
	
	// Return Button
	private JButton returnButton = null;
	
		
	// Chart panel
	private ChartPanel chartPanel = null;
	
	
	private JDialog parentDialog = null;
	
	
	public LineChart(Bid currentBid, DialogHumanConfirmManual parentDialog){
		
		this.parentDialog = parentDialog;
		
		// -------------------------- //
		// - Creating the New Frame - //
		// -------------------------- //
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		

        
        // ------------------------------------ //
        // - Main Panel SubComponent Creation - //
        // ------------------------------------ //
        
        // Panel
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        
        // Label
        headLineLB = new JLabel("LineChart For: " + currentBid.ticker);
        headLineLB.setFont(new Font("Impact",Font.BOLD,20) );
       
        // Button
        returnButton = new JButton("Return");
        returnButton.addActionListener(this);
        
        // LineChart
        chartPanel = createLineChart(currentBid);
        
        // -------------------------- //
        // - Placing All Components - //
        // -------------------------- //
        
        addComp(mainPanel,headLineLB,0,0,0,0,GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL);
        addComp(mainPanel,chartPanel,0,0,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE);
        addComp(mainPanel,returnButton,0,0,0,0,GridBagConstraints.SOUTHWEST,GridBagConstraints.NONE);
        
        
        setContentPane(mainPanel);
		setSize(800,600);
		
	}
	
	
	public void actionPerformed(ActionEvent e){
		if (e.getSource() == returnButton){
			
			// Return To priviuse Frame and dispose of this current Frame
			parentDialog.setVisible(true);
			this.dispose();
			
			
		}
	}
	
	private ChartPanel createLineChart(Bid currentBid){
		// Creating the Chart:
		// -------------------
		
		// XYSeries:
		// ---------
		
		XYSeries dataForLine = new XYSeries("LineChart");
		for (int i = 0 ; i < currentBid.stock.ticks.size() ; i++){
			dataForLine.add(i, currentBid.stock.ticks.get(i).close());
		}
		
		// DataSet:
		// --------
		XYDataset xyDataset = new XYSeriesCollection(dataForLine);
		
		
		// Chart:
		// ------
		JFreeChart chart = ChartFactory.createXYLineChart(
				"Line Chart for: "+ currentBid.ticker,
				"Iteration",
				"Price",
				xyDataset,
				PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);
	
		
		// ----------------------------- //
		// - Chart Panel and rendering - //
		// ----------------------------- //		
				
		chartPanel = new ChartPanel(chart);

		final XYPlot plot = chart.getXYPlot();		
		chart.setBackgroundPaint(Color.white);
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();    
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());  
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        return chartPanel;
			
	}
	
	
	private void addComp(JPanel panel, JComponent comp, int xPos, int yPos, int compWidth, int compHeight, int place, int stretch){
		
		GridBagConstraints gridConstraints = new GridBagConstraints();
		
		gridConstraints.gridx 		= xPos;
		gridConstraints.gridy 		= yPos;
		gridConstraints.gridwidth 	= compWidth;
		gridConstraints.gridheight	= compHeight;
		gridConstraints.weightx		= 100;
		gridConstraints.weighty		= 100;
		gridConstraints.insets		= new Insets(5,5,5,5);
		gridConstraints.anchor		= place;
		gridConstraints.fill		= stretch;
		
		panel.add(comp,gridConstraints);
		
	}

}
