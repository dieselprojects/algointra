package com.dieselProjects.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;
import org.jfree.data.xy.XYDataset;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.configurationCluster.Configuration;




public class CandleSticksChart extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Configuration configuration;
	
	
	// Main Panel
	private JPanel mainPanel = null;
	
	// HeadLineLabel
	private JLabel headLineLB = null;
	
	
	// Return Button
	private JButton returnButton = null;
	
		
	// Chart panel
	private ChartPanel chartPanel = null;
	
	
	private JDialog parentDialog = null;
	
	
	public CandleSticksChart(Bid currentBid, DialogHumanConfirmManual parentDialog){
		
		// Setting up Variables:
		// ---------------------
		
		this.parentDialog = parentDialog;
		
		this.configuration = Configuration.getInstance();
		
		
		
		// -------------------------- //
		// - Creating the New Frame - //
		// -------------------------- //
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		

        
        // ------------------------------------ //
        // - Main Panel SubComponent Creation - //
        // ------------------------------------ //
        
        // Panel
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        
        // Label
        headLineLB = new JLabel("ChandleSticks Chart For: " + currentBid.ticker);
        headLineLB.setFont(new Font("Impact",Font.BOLD,20) );
       
        // Button
        returnButton = new JButton("Return");
        returnButton.addActionListener(this);
        
        // LineChart
        chartPanel = createCandleSticks(currentBid);
        
        // -------------------------- //
        // - Placing All Components - //
        // -------------------------- //
        
        addComp(mainPanel,headLineLB,0,0,0,0,GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL);
        addComp(mainPanel,chartPanel,0,0,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE);
        addComp(mainPanel,returnButton,0,0,0,0,GridBagConstraints.SOUTHWEST,GridBagConstraints.NONE);
        
        
        setContentPane(mainPanel);
		setSize(800,600);
		
	}
	
	
	public void actionPerformed(ActionEvent e){
		if (e.getSource() == returnButton){
			
			// Return To previous Frame and dispose of this current Frame
			parentDialog.setVisible(true);
			this.dispose();
			
			
		}
	}
	
	
	// --------------------- //
	// - CreateCandleStick - //
	// --------------------- //
	
	private ChartPanel createCandleSticks(Bid currentBid){
		
		// Variables:
		// ----------
		DateAxis 			dateAxis 	= new DateAxis("Date");
		NumberAxis			pricesAxis	= new NumberAxis("Price");
		CandlestickRenderer	renderer	= new CandlestickRenderer();
		XYDataset			dataset;
		
		
		// Creating DataSet:
		// -----------------
        
		//This is the dataset we are going to create
        DefaultOHLCDataset resultDataSet = null;
        //This is the data needed for the dataset
        OHLCDataItem[] data;

        //This is where we go get the data, replace with your own data source
        
        // Getting Data and creating Dates from currentBid.Stock:
        
        //DateFormat df = new SimpleDateFormat("y-M-d");
        
        
        
        
        // Creating First Date Item from start of simulation:
        // --------------------------------------------------
        
        // Parsing First Date from Confiruation
        String[] dateParse = configuration.startDate.split("\\.");
        int day 	= Integer.parseInt(dateParse[0]);
        int month 	= Integer.parseInt(dateParse[1]);
        int year	= Integer.parseInt(dateParse[2]);
        
        Calendar incDate = new GregorianCalendar(year,month,day);
    
        
    	// Creating ArrayList to store All Candlestick Items: 
    	ArrayList<OHLCDataItem> dataItems = new ArrayList<OHLCDataItem>();
        
        for (int i = 0 ; i < currentBid.stock.ticks.size(); i++){
	
        	// Creating OHLCData Item: 
        	Date 	date 		= incDate.getTime();
        	double 	open 		= currentBid.stock.ticks.get(i).open();
        	double 	high     	= currentBid.stock.ticks.get(i).high();
        	double 	low      	= currentBid.stock.ticks.get(i).low();
        	double 	close    	= currentBid.stock.ticks.get(i).close();
        	double 	volume   	= currentBid.stock.ticks.get(i).volume();
        	
        	OHLCDataItem item = new OHLCDataItem(date, open, high, low, close, volume);
        	
        	// Insert Item to list:
        	dataItems.add(item);   
        	
        	// Increment Calendar Item:
        	incDate.add(Calendar.DAY_OF_YEAR, 1);
        }
        
        // Finally:
        data = dataItems.toArray(new OHLCDataItem[dataItems.size()]);
               
        
        //Create a dataset, an Open, High, Low, Close dataset
        resultDataSet = new DefaultOHLCDataset(currentBid.ticker, data);		
		
        // (dataSet is XYDataSet)
        dataset = resultDataSet;
		
        
        XYPlot mainPlot = new XYPlot(dataset, dateAxis, pricesAxis, renderer);
        
        //Do some setting up, see the API Doc
        renderer.setSeriesPaint(0, Color.BLACK);
        renderer.setDrawVolume(false);
        pricesAxis.setAutoRangeIncludesZero(false);
        //domainAxis.setTimeline( SegmentedTimeline.newMondayThroughFridayTimeline() );

        //Now create the chart and chart panel
        JFreeChart chart = new JFreeChart(currentBid.ticker, null, mainPlot, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(600, 300));
     
        return chartPanel;
			
	}
	

	
	private void addComp(JPanel panel, JComponent comp, int xPos, int yPos, int compWidth, int compHeight, int place, int stretch){
		
		GridBagConstraints gridConstraints = new GridBagConstraints();
		
		gridConstraints.gridx 		= xPos;
		gridConstraints.gridy 		= yPos;
		gridConstraints.gridwidth 	= compWidth;
		gridConstraints.gridheight	= compHeight;
		gridConstraints.weightx		= 100;
		gridConstraints.weighty		= 100;
		gridConstraints.insets		= new Insets(5,5,5,5);
		gridConstraints.anchor		= place;
		gridConstraints.fill		= stretch;
		
		panel.add(comp,gridConstraints);
		
	}

}
