package com.dieselProjects.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.SimulateTimeMan;
import com.dieselProjects.supportCluster.Types.IndicatorsType;
import com.dieselProjects.supportCluster.Types.OPType;

public class GeneralChart extends JPanel {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private static final String TAG = "GeneralChart ";

	Configuration configuration = Configuration.getInstance();
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	Logger logger = Logger.getInstance();

	StockBean stock;
	String title;

	// Price Volume graph:
	// -------------------
	TimeSeries priceGraph;
	TimeSeriesCollection priceDataSet;
	TimeSeries volumeGraph;
	TimeSeriesCollection volumeDataSet;

	// Buy dots:
	// ---------
	TimeSeries buyGraph = new TimeSeries("buy");
	TimeSeriesCollection buyDataSet = new TimeSeriesCollection();

	// Sell dots:
	// ----------
	TimeSeries sellGraph = new TimeSeries("sell");
	TimeSeriesCollection sellDataSet = new TimeSeriesCollection();

	DefaultListModel<String> buySellList = new DefaultListModel<String>();

	// Trades:
	LinkedList<IndicatorType> trades = new LinkedList<IndicatorType>();
	// Actual indication that could result in a trade:
	LinkedList<IndicatorType> indicatorList = new LinkedList<IndicatorType>();
	// all indicators regardless of stock state:
	LinkedList<IndicatorType> simulatedIndicatorList = new LinkedList<IndicatorType>();
	// stop lost sell Indicators
	LinkedList<IndicatorType> stopLostIndicatorList = new LinkedList<IndicatorType>();

	// Bottom Tables:
	// --------------
	// Table bottom middle Indicator buy/Sell
	DefaultListModel<String> indicators = new DefaultListModel<String>();
	// Table bottom left, buy/sell
	LinkedList<Double> buyList = new LinkedList<Double>();
	LinkedList<Integer> buyListIndex = new LinkedList<Integer>();
	LinkedList<Double> sellList = new LinkedList<Double>();
	LinkedList<Integer> sellListIndex = new LinkedList<Integer>();

	@SuppressWarnings({ "unchecked", "static-access" })
	public LinkedList<Date>[] indicatorsBuyList = new LinkedList[configuration.NUM_TA_INDICATORS + 61];
	@SuppressWarnings({ "unchecked", "static-access" })
	public LinkedList<Date>[] indicatorsSellList = new LinkedList[configuration.NUM_TA_INDICATORS + 61];

	public GeneralChart(StockBean stock) {

		this.title = stock.ticker;
		this.stock = stock;
		// stock price:
		priceGraph = new TimeSeries(stock.ticker);

		priceDataSet = new TimeSeriesCollection();
		priceDataSet.addSeries(priceGraph);

		volumeGraph = new TimeSeries(stock.ticker);

		for (int i = 0; i < configuration.simulateCfg.getEndOfTest(); i++) {

			Minute minute = new Minute(configuration.simulateCfg.testerArray
					.get(title).get(i).date.getTime());

			try {

				priceGraph.add(minute, configuration.simulateCfg.testerArray
						.get(title).get(i).close());
				volumeGraph.add(minute, configuration.simulateCfg.testerArray
						.get(title).get(i).volume);

			} catch (SeriesException e) {

				System.out.println("catched exception ploting graph");

				Calendar fixedDate = configuration.simulateCfg.testerArray.get(
						title).get(i).date;
				fixedDate.add(Calendar.MINUTE, 1);
				minute = new Minute(fixedDate.getTime());

				priceGraph.add(minute, configuration.simulateCfg.testerArray
						.get(title).get(i).close());
				volumeGraph.add(minute, configuration.simulateCfg.testerArray
						.get(title).get(i).volume);

			}

		}

		volumeDataSet = new TimeSeriesCollection();
		volumeDataSet.addSeries(volumeGraph);

		for (int i = 0; i < (Configuration.NUM_TA_INDICATORS + 61); i++) {
			indicatorsBuyList[i] = new LinkedList<Date>();
			indicatorsSellList[i] = new LinkedList<Date>();
		}
	}

	public JFreeChart buildPriceVolumeChart(String title) {
		JFreeChart chart = ChartFactory.createTimeSeriesChart(title, "Date",
				"Price", priceDataSet, true, true, false);
		chart.setBackgroundPaint(Color.white);
		XYPlot plot = chart.getXYPlot();

		// Price chart:
		// -------------------
		NumberAxis rangeAxis1 = (NumberAxis) plot.getRangeAxis();
		rangeAxis1.setLowerMargin(0.40); // to leave room for volume bars
		DecimalFormat format = new DecimalFormat("00.00");
		rangeAxis1.setNumberFormatOverride(format);

		XYItemRenderer renderer1 = plot.getRenderer();
		renderer1.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
				new SimpleDateFormat("HH:mm:ss d-MMM-yyyy"), new DecimalFormat(
						"0.00")));

		// Buy/Sell Dots:
		// --------------
		if (!buyGraph.isEmpty()) {
			buyDataSet.addSeries(buyGraph);
			XYItemRenderer rendererBuy = new XYLineAndShapeRenderer(false, true);
			rendererBuy.setBasePaint(Color.green);
			rendererBuy.setBaseItemLabelGenerator(new SeriesLabelGen());
			rendererBuy.setBaseItemLabelPaint(Color.GREEN);
			rendererBuy.setBaseItemLabelsVisible(true);
			plot.setRenderer(2, rendererBuy);
			plot.setDataset(2, buyDataSet);
			plot.mapDatasetToRangeAxis(1, 2);
		}
		if (!sellGraph.isEmpty()) {
			sellDataSet.addSeries(sellGraph);
			XYItemRenderer rendererSell = new XYLineAndShapeRenderer(false,
					true);
			rendererSell.setBasePaint(Color.RED);
			rendererSell.setBaseItemLabelGenerator(new SeriesLabelGen());
			rendererSell.setBaseItemLabelPaint(Color.MAGENTA);
			rendererSell.setBaseItemLabelsVisible(true);

			plot.setRenderer(3, rendererSell);
			plot.setDataset(3, sellDataSet);
			plot.mapDatasetToRangeAxis(1, 3);
			plot.getDomainAxis().setTickLabelPaint(new Color(0, 100, 0, 0));
		}

		// Volume chart:
		// -------------
		NumberAxis rangeAxis2 = new NumberAxis("Volume");
		rangeAxis2.setUpperMargin(1.00); // to leave room for price line
		plot.setRangeAxis(1, rangeAxis2);
		plot.setDataset(1, volumeDataSet);
		plot.mapDatasetToRangeAxis(1, 1);
		XYBarRenderer renderer2 = new XYBarRenderer(0.20);
		renderer2.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
				new SimpleDateFormat("HH:mm:ss d-MMM-yyyy"), new DecimalFormat(
						"0,000.00")));
		plot.setRenderer(1, renderer2);

		new LinkedList<Double>();
		new LinkedList<Integer>();

		// Actual buy/Sell Indicator markers:
		// ----------------------------------
		for (int i = 0; i < indicatorList.size(); i++) {

			ValueMarker marker = new ValueMarker(priceDataSet.getXValue(0,
					indicatorList.get(i).index), Color.BLACK, new BasicStroke(
					0.7f));
			if (indicatorList.get(i).opType == OPType.BUY)
				marker.setPaint(Color.CYAN);
			else
				marker.setPaint(Color.MAGENTA);

			marker.setLabel(new String(i + " "
					+ String.valueOf(indicatorList.get(i).indicator)));
			marker.setLabelAnchor(RectangleAnchor.CENTER);
			marker.setLabelTextAnchor(TextAnchor.CENTER_LEFT);
			plot.addDomainMarker(marker);

		}

		// Additional post transaction Indicator markers:
		// ----------------------------------------------
		for (int i = 0; i < simulatedIndicatorList.size(); i++) {

			float dash1[] = { 10.0f };
			ValueMarker marker = new ValueMarker(priceDataSet.getXValue(0,
					simulatedIndicatorList.get(i).index), Color.BLACK,
					new BasicStroke(0.5f, BasicStroke.CAP_BUTT,
							BasicStroke.JOIN_MITER, 1.0f, dash1, 0.0f));

			if (simulatedIndicatorList.get(i).opType == OPType.BUY)
				marker.setPaint(Color.CYAN);
			else
				marker.setPaint(Color.MAGENTA);

			plot.addDomainMarker(marker);

		}

		// StopLost sell Indicator marker
		for (int i = 0; i < stopLostIndicatorList.size(); i++) {

			ValueMarker stopLostMarker = new ValueMarker(
					priceDataSet.getXValue(0,
							stopLostIndicatorList.get(i).index),
					Color.DARK_GRAY, new BasicStroke(2.0f));
			plot.addDomainMarker(stopLostMarker);
		}

		// Trades:
		for (int i = 0; i < trades.size(); i++) {

			ValueMarker marker = new ValueMarker(priceDataSet.getXValue(0,
					trades.get(i).index), Color.BLACK, new BasicStroke(1.8f));
			if (trades.get(i).opType == OPType.BUY)
				marker.setPaint(ChartColor.DARK_GREEN);
			else
				marker.setPaint(ChartColor.DARK_RED);
			plot.addDomainMarker(marker);
		}

		return chart;
	}

	// Main chart frame:
	// -----------------
	public void showPriceVolumeChart() {

		this.setLayout(new GridLayout(2, 1));

		// Top row:
		this.add(new ChartPanel(buildPriceVolumeChart(title)),
				BorderLayout.PAGE_START);

		// Bottom row:
		JPanel bottomRow = new JPanel(new GridLayout(1, 3));

		// BuySell ( 2X1 ):
		JScrollPane listScroller = new JScrollPane(generateBuySellTable()); // JTABLE
		bottomRow.add(listScroller, BorderLayout.WEST);

		// Indicators triggered:
		JScrollPane indicatorTable = new JScrollPane(generateIndicatorsTable());
		bottomRow.add(indicatorTable, BorderLayout.CENTER);

		// Configuration (2X3)
		JScrollPane cfgListScroller = generateConfigurationTable();
		bottomRow.add(cfgListScroller, BorderLayout.EAST);

		this.add(bottomRow);

		// Frame:
		JFrame frame = new JFrame(title + " Price/Volume");
		frame.getContentPane().add(this, BorderLayout.CENTER);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(screenSize.width, screenSize.height);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// Bottom left (buy sell transaction):
	// ------------------------------------
	public JTable generateBuySellTable() {
		String[] columnNames = { "Index", "Date", "Op", "Price", "Amount",
				"Profit" };
		String[][] tableData = new String[buySellList.size()][columnNames.length];
		int indexBuy = 0;
		int indexSell = 0;
		OPType[] buySellArr = new OPType[buySellList.size()];

		for (int i = 0; i < buySellList.size(); i++) {
			String[] splitedString = buySellList.getElementAt(i).split(" ");
			tableData[i][0] = (splitedString[1].equals("BUY")) ? String
					.valueOf(indexBuy++) : String.valueOf(indexSell++);
			if (splitedString[1].equals("BUY")) {
				buySellArr[i] = OPType.BUY;
			} else {
				buySellArr[i] = OPType.SELL;
			}
			for (int j = 0; j < columnNames.length - 1; j++)
				tableData[i][j + 1] = splitedString[j];
		}

		JTable returnedTable = new JTable(tableData, columnNames);
		CustomCellRenderer tableRenderer = new CustomCellRenderer();
		tableRenderer.bidArray = buySellArr;

		for (int i = 0; i < columnNames.length; i++) {
			returnedTable.getColumnModel().getColumn(i)
					.setCellRenderer(tableRenderer);
		}

		return returnedTable;

	}

	// Bottom middle (indication):
	// ---------------------------
	public JTable generateIndicatorsTable() {

		String[] columnNames = { "Index", "Date", "Op", "Indicator", "Decision" };
		String[][] tableData = new String[indicators.size()][columnNames.length];
		int index = 0;
		OPType[] buySellArr = new OPType[indicators.size()];
		int[] decisionArr = new int[indicators.size()];

		for (int i = 0; i < indicators.size(); i++) {
			String[] splitedString = indicators.getElementAt(i).split(" ");
			String[] fullSplitStr = new String[columnNames.length];
			if (splitedString.length == 3) {
				for (int j = 0; j < splitedString.length; j++)
					fullSplitStr[j] = splitedString[j];
				fullSplitStr[3] = "FALSE";
				splitedString = fullSplitStr;
			}

			tableData[i][0] = String.valueOf(index++);
			if (splitedString[1].equals("BUY")) {
				buySellArr[i] = OPType.BUY;
				if ((splitedString[3] != null)
						&& (splitedString[3].equals("TRUE"))) {
					decisionArr[i] = 1;
				}

			} else {
				buySellArr[i] = OPType.SELL;
				if ((splitedString[3] != null)
						&& (splitedString[3].equals("TRUE"))) {
					decisionArr[i] = -1;
				}
			}
			for (int j = 0; j < columnNames.length - 1; j++)
				tableData[i][j + 1] = (splitedString[j] != null) ? splitedString[j]
						: "FALSE";

		}

		JTable returnedTable = new JTable(tableData, columnNames);
		CustomCellRenderer tableRenderer = new CustomCellRenderer();

		tableRenderer.bidArray = buySellArr;
		tableRenderer.decisionArr = decisionArr;

		for (int i = 0; i < columnNames.length; i++) {
			returnedTable.getColumnModel().getColumn(i)
					.setCellRenderer(tableRenderer);
		}
		return returnedTable;
	}

	// Bottom Left table (Configuration):
	// ----------------------------------
	public JScrollPane generateConfigurationTable() {

		// Create list data:
		DefaultListModel<String> cfgListModel = new DefaultListModel<String>();

		// Get test results:

		// final test results
		// Stock performance
		// Success rates

		ArrayList<StockBean> stockList = new ArrayList<StockBean>();
		stockList.add(stock);

		String finalResultString = logger.finalReport(stockList);
		String[] finalResultArr = finalResultString.split("\\n");

		cfgListModel.addElement("Number of success positions");

		for (String nextString : finalResultArr) {
			cfgListModel.addElement(nextString);
		}

		JList<String> cfgList = new JList<String>(cfgListModel);

		cfgList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		JScrollPane listScroller = new JScrollPane(cfgList);

		return listScroller;
	}

	private class CustomCellRenderer extends DefaultTableCellRenderer {

		private static final long serialVersionUID = 1L;
		OPType[] bidArray;
		int[] decisionArr;

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {

			Component rendererComp = super.getTableCellRendererComponent(table,
					value, isSelected, hasFocus, row, column);

			if (bidArray[row].equals(OPType.BUY)) {
				if (decisionArr != null) {
					if (decisionArr[row] == 1)
						rendererComp.setBackground(Color.GREEN);
					else
						rendererComp.setBackground(Color.CYAN);
				} else
					rendererComp.setBackground(Color.GREEN);

			} else {
				if (decisionArr != null) {
					if (decisionArr[row] == -1)
						rendererComp.setBackground(Color.RED);
					else
						rendererComp.setBackground(Color.MAGENTA);
				} else
					rendererComp.setBackground(Color.RED);

			}

			return rendererComp;
		}

	}

	private class SeriesLabelGen extends StandardXYItemLabelGenerator {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public String generateLabel(XYDataset dataset, int series, int item) {

			return String.valueOf(item);
		}
	}

	private class IndicatorType {

		public Integer index;
		public String indicator;
		public OPType opType;
		public boolean isSimulated;

		public IndicatorType(Integer index, String indicator, OPType opType) {
			this(index, indicator, opType, false);
		}

		public IndicatorType(Integer index, String indicator, OPType opType,
				boolean isSimulated) {
			this.index = index;
			this.indicator = indicator;
			this.opType = opType;
			this.isSimulated = isSimulated;
		}
	}

	// Add buy sell point
	public void addTransaction(Bid bid, Calendar date) {

		Minute minute = new Minute(date.getTime());
		double profit = 0.0;
		if (bid.opType == OPType.BUY) {
			buyGraph.add(minute, bid.price);
		} else {
			sellGraph.add(minute, bid.price);
			profit = bid.price
					* bid.amount
					- (double) buyGraph.getDataItem(
							buyGraph.getItems().size() - 1).getValue()
					* bid.amount;

		}

		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		// DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		Calendar adjDate = (Calendar) date.clone();
		adjDate.add(Calendar.HOUR_OF_DAY, -7);

		// TODO: add additional information regarding buy and sell
		buySellList.addElement(new String(df.format(adjDate.getTime()) + " "
				+ bid.opType + " " + bid.price + " " + bid.amount + " "
				+ profit));

		// add to trades marker list:
		trades.add(new IndicatorType(simTimeMan.iteration, "Trade", bid.opType));

		// update indicators table:
		String lastElement = indicators.remove(indicators.getSize() - 1);
		lastElement += "TRUE";
		indicators.addElement(lastElement);

	}

	// Add Indicator point
	public void addIndicator(Calendar date, IndicatorsType type,
			String indicator, boolean isSimulated) {

		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		Calendar adjDate = (Calendar) date.clone();
		adjDate.add(Calendar.HOUR_OF_DAY, -7);

		// indicator table:
		switch (type) {
		case BUY:
			if (isSimulated == false)
				indicators.addElement(new String(df.format(adjDate.getTime())
						+ " BUY (" + indicator + ") "));
			break;
		case SELL:
			if (isSimulated == false)
				indicators.addElement(new String(df.format(adjDate.getTime())
						+ " SELL (" + indicator + ") "));
			break;
		case STOPLOST:
			if (isSimulated == false)
				indicators.addElement(new String(df.format(adjDate.getTime())
						+ " SELL (STOP_LOST) "));
			break;
		case STOPLOSTONGOING:
			if (isSimulated == false)
				indicators.addElement(new String(df.format(adjDate.getTime())
						+ " SELL (STOP_LOST_ONGOING) "));
			break;
		default:
			System.err.println(TAG + " no a valid indicators type for "
					+ stock.ticker);
			System.exit(1);
			break;
		}

		if (isSimulated == true) {

			// TODO: remove null as a method to provoke error
			simulatedIndicatorList.add(new IndicatorType(simTimeMan.iteration,
					indicator, (type == IndicatorsType.BUY) ? OPType.BUY
							: (type == IndicatorsType.SELL) ? OPType.SELL
									: null, isSimulated));

		} else {

			if (type != IndicatorsType.STOPLOST) {
				// Add index / indicator type / operation to Indicator list for
				// plot
				indicatorList.add(new IndicatorType(simTimeMan.iteration,
						indicator, (type == IndicatorsType.BUY) ? OPType.BUY
								: (type == IndicatorsType.SELL) ? OPType.SELL
										: null, isSimulated));
			} else {
				// stop lost
				stopLostIndicatorList.add(new IndicatorType(
						simTimeMan.iteration, indicator, OPType.SELL,
						isSimulated));
			}
		}
	}
}
