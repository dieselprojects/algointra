package com.dieselProjects.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

import com.dieselProjects.beans.Bid;

public class DialogHumanConfirmManual extends JDialog implements ActionListener{

	// Class Variables:
	// ----------------
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Bid answeredBid = null;
	private Bid currentBid = null;
	
	public Bid getAnswer(){ return answeredBid;}
	
	
	// Swing Components:
	// -----------------
	
	// Main Panel:
	// -----------
	
	private JPanel mainPanel = null;
	

	// General Info :
	// --------------
	
	private JPanel generalInfoPanel = null;
	
	private Border generalInfoBorder = null;
	
	private JLabel stockTickerLB1 = null;
	private JLabel stockTickerLB2 = null;

	private JLabel stockPriceLB1 = null;
	private JLabel stockPriceLB2 = null;

	private JLabel stockOpLB1 = null;
	private JLabel stockOpLB2 = null;

	private JLabel stockGradeLB1 = null;
	private JLabel stockGradeLB2 = null;

	private JLabel stockAmountLB1 = null;
	private JTextField stockAmountTF1= null;

	private JPanel indicatorsPanel = null;
	private JList<String> indicatorsList = null;
	
	private JPanel chartsPanel = null;
	private JButton linesButton = null;
	private JButton candleStickButton = null;
	
	
	private JPanel  confirmationButtonsPanel = null;
	private JButton yesButton = null;
	private JButton noButton = null;
	
	

	
	// Constructor:
	// ------------
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DialogHumanConfirmManual(final Bid requestedBid){
		
		// Local Variables:
		// ----------------
		
		currentBid = requestedBid;
		currentBid.approvedHuman = false;
		
		
		// ---------------- //
		// - GUI Creation - //
		// ---------------- //		
		
		// Frame, not use if needed
		//Dialog mainFrame = new Dialog(new Frame());
		
		// -------------------------- //
		// - Main Dialog properties - //
		// -------------------------- //
		
		setTitle("Confirmation Dialog");
	
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setResizable(false);
		
		// -------------- //
		// - Main Panel - //
		// -------------- //
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());

		//mainPanel.add(new JLabel("Confirmation Dialog"));
		
		
		// ---------------------- //
		// - General Info Panel - //
		// ---------------------- //
		
		// General Info "Panel" consist of: 
		// a. StockTicker, 2 labels
		// b. StockPrice, 2 labels
		// c. StockOperation 2 labels
		// d. stockGrade, 2 labels
		// e. If Sell, Profit 2 Labels
		// f. If Sell Bought Price 2 Labels
		// g. Border
		// h. Border Label
		
		
		// GeneralInfo panel:
		// ------------------
		generalInfoPanel = new JPanel();
		generalInfoPanel.setLayout(new GridBagLayout());
		
		
		// StockTicker Labels:
		// -------------------
		stockTickerLB1 = new JLabel("Ticker");
		stockTickerLB1.setFont(new Font("Impact",Font.BOLD,14));
		addComp(generalInfoPanel,stockTickerLB1,0,0,1,1,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
		stockTickerLB2 = new JLabel(requestedBid.ticker);
		addComp(generalInfoPanel,stockTickerLB2,0,2,2,1,GridBagConstraints.WEST,GridBagConstraints.NONE);

		
		// stockPrice Label:
		// -----------------
		stockPriceLB1 = new JLabel("Price");
		stockPriceLB1.setFont(new Font("Impact",Font.BOLD,14));
		addComp(generalInfoPanel,stockPriceLB1,1,0,1,1,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
		stockPriceLB2 = new JLabel( Double.toString(requestedBid.price) );
		addComp(generalInfoPanel,stockPriceLB2,1,2,2,1,GridBagConstraints.WEST,GridBagConstraints.NONE);

		
		// stockOpretation label:
		// ----------------------
		stockOpLB1 = new JLabel("Operation");
		stockOpLB1.setFont(new Font("Impact",Font.BOLD,14));
		addComp(generalInfoPanel,stockOpLB1,2,0,1,1,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
		stockOpLB2 = new JLabel(requestedBid.opType.toString());
		addComp(generalInfoPanel,stockOpLB2,2,2,2,1,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
		// stockGrade Label:
		// -----------------
		stockGradeLB1 = new JLabel("Grade");
		stockGradeLB1.setFont(new Font("Impact",Font.BOLD,14));
		addComp(generalInfoPanel,stockGradeLB1,3,0,1,1,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
		stockGradeLB2 = new JLabel(Double.toString(requestedBid.stock.currentGrade));
		addComp(generalInfoPanel,stockGradeLB2,3,2,2,1,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
			
		// For Sell Method Only:
		// ---------------------
		
		// Profit Label:
		// -------------
		
		// BoughtPrice Label:
		// ------------------
		
		
		
		// stockAmount Label:
		// ------------------
		stockAmountLB1 = new JLabel("Amount");
		stockAmountLB1.setFont(new Font("Impact",Font.BOLD,14));
		addComp(generalInfoPanel,stockAmountLB1,4,0,1,1,GridBagConstraints.EAST,GridBagConstraints.NONE);
		
		stockAmountTF1 = new JTextField(Integer.toString(requestedBid.amount) , 5);
		stockAmountTF1.setHorizontalAlignment(JTextField.CENTER);
		addComp(generalInfoPanel,stockAmountTF1,4,2,5,1,GridBagConstraints.EAST,GridBagConstraints.CENTER);		
		
		// Border:
		// -------
		generalInfoBorder = BorderFactory.createTitledBorder("General Info");
		
		generalInfoPanel.setBorder(generalInfoBorder);
		
		
		addComp(mainPanel,generalInfoPanel,0,0,5,4,GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL);	
		
		
		// -------------------- //
		// - Indicators table - //
		// -------------------- //
		
		indicatorsPanel = new JPanel();
		indicatorsPanel.setLayout(new GridBagLayout());
		
		Border indictorBorder = BorderFactory.createTitledBorder("Indicators List");

		
		ArrayList<String> indicatorsArr = new ArrayList<String>( requestedBid.stock.indicatorsTable.keySet() );
		indicatorsList = new JList(indicatorsArr.toArray());
		indicatorsList.setCellRenderer(new ListCellRenderer(){

			@Override
			public Component getListCellRendererComponent(JList list,
					Object value, int index, boolean isSelected, boolean cellHasFucos) {
				
				JLabel nextLabel = new JLabel(value.toString());
				nextLabel.setFont(new Font("David",Font.BOLD,14));
				
				if ( requestedBid.stock.indicatorsTable.get(value.toString() ) ){
					nextLabel.setForeground(Color.GREEN);
				} else{
					nextLabel.setForeground(Color.RED);
				}

				return nextLabel;
			}
			
		});
		
		indicatorsPanel.setBorder(indictorBorder);		
		addComp(indicatorsPanel,indicatorsList,0,0,0,0,GridBagConstraints.CENTER,GridBagConstraints.VERTICAL);
		
		addComp(mainPanel,indicatorsPanel,0,0,0,0,GridBagConstraints.WEST,GridBagConstraints.NONE);	
		
		
		// ------------------ //
		// - Charts buttons - //
		// ------------------ //
		
		chartsPanel = new JPanel();
		
		// Line graph Button:
		linesButton = new JButton("Line Chart");
		linesButton.addActionListener(this);
		addComp(chartsPanel,linesButton,0,0,0,0,GridBagConstraints.EAST,GridBagConstraints.NONE);
		
		// CandleStick graph Button:
		candleStickButton = new JButton("CandleStick Chart");
		candleStickButton.addActionListener(this);
		addComp(chartsPanel,candleStickButton,1,0,0,0,GridBagConstraints.EAST,GridBagConstraints.NONE);
				
		
		addComp(mainPanel,chartsPanel,0,0,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE);
		
		// ------------------------ //
		// - Confirmation Buttons - //
		// ------------------------ //
		
		confirmationButtonsPanel = new JPanel();
		
		// Button Confirm:
		// ---------------
		yesButton = new JButton("Confirm");
		yesButton.addActionListener(this);
		addComp(confirmationButtonsPanel,yesButton,0,0,0,0,GridBagConstraints.EAST,GridBagConstraints.NONE);

		
		// Button disAprove:
		// -----------------
	    noButton = new JButton("No");
		noButton.addActionListener(this);
		addComp(confirmationButtonsPanel,noButton,0,0,0,0,GridBagConstraints.WEST,GridBagConstraints.NONE);
		
		addComp(mainPanel,confirmationButtonsPanel,0,0,0,0,GridBagConstraints.SOUTHEAST,GridBagConstraints.NONE);
		
		
		// ---------------- //
		// - Final LayOut - //
		// ---------------- //
		getContentPane().add(mainPanel);		
		pack();
		setSize(800,600);
		setVisible(true);
		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		
		
		// ------------------ //
		// - Confirm Button - //
		// ------------------ //
		
		if (yesButton == e.getSource()) {
			currentBid.approvedHuman = true;
			answeredBid = currentBid;
			setVisible(false);
			//TODO: add closing
			dispose();
		}
		
		
		// --------------- //
		// - Deny Button - //
		// --------------- //
		
		if (noButton == e.getSource()){
			currentBid.approvedBroker = false;
			answeredBid = currentBid;
			setVisible(false);
			dispose();
		}
		
		
		// --------------------- //
		// - Line Chart Button - //
		// --------------------- //
		
		if (linesButton == e.getSource()){
			// Plot Stock Chart using, currentBid.stock

	        LineChart lineChart = new LineChart(currentBid,this);
	        setVisible(false);
	        lineChart.setVisible(true);
		}
		
		
		// ----------------------- //
		// - Candle Stick Button - //
		// ----------------------- //
		
		if (candleStickButton == e.getSource()){
			// Plot CandleStick
			
			CandleSticksChart candleStickChart = new CandleSticksChart(currentBid,this);
			setVisible(false);
			candleStickChart.setVisible(true);
		}
		
	}
	
	
	private void addComp(JPanel panel, JComponent comp, int xPos, int yPos, int compWidth, int compHeight, int place, int stretch){
		
		GridBagConstraints gridConstraints = new GridBagConstraints();
		
		gridConstraints.gridx 		= xPos;
		gridConstraints.gridy 		= yPos;
		gridConstraints.gridwidth 	= compWidth;
		gridConstraints.gridheight	= compHeight;
		gridConstraints.weightx		= 100;
		gridConstraints.weighty		= 100;
		gridConstraints.insets		= new Insets(5,5,5,5);
		gridConstraints.anchor		= place;
		gridConstraints.fill		= stretch;
		
		panel.add(comp,gridConstraints);
		
	}
	
	

}
