package com.dieselProjects.indicatorCluster;

import java.util.Calendar;

import org.apache.commons.lang3.ArrayUtils;

import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.CdlIndicatorsCfg;
import com.dieselProjects.supportCluster.SimulateTimeMan;
import com.dieselProjects.supportCluster.Types.CDLIndicators;
import com.dieselProjects.supportCluster.Types.IndicatorsType;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.TickValueType;

public class CdlIndicators {

	// Use TA-Lib Buy/Bullish Indicators:
	// CDL3INSIDE Three Inside Up // Weak
	// CDL3OUTSIDE Three Outside Up // GOOD
	// CDL3STARSINSOUTH Three Stars In The South // Strongest
	// CDL3WHITESOLDIERS Three Advancing White Soldiers// Strong
	// CDLABANDONEDBABY Abandoned Baby // Strong

	CdlIndicatorsCfg cdlIndicatorsCfg;
	StockBean stock;
	Calendar date;
	
	public CdlIndicators(StockBean stock, CdlIndicatorsCfg cdlIndicatorsCfg, Calendar date){
		this.cdlIndicatorsCfg = cdlIndicatorsCfg;
		this.stock = stock;
		this.date = date;
	}
	
	
	public int[] start() {

		// Indicators list:
		CdlGenericIndicator genericIndicator = new CdlGenericIndicator(cdlIndicatorsCfg);

		int startIndex = 0;
		int endIndex = stock.numOfSamples - 1;
		int size = stock.numOfSamples;
		double[] openPrices = stock.getListAsArray(TickValueType.OPEN);
		double[] highPrices = stock.getListAsArray(TickValueType.HIGH);
		double[] lowPrices = stock.getListAsArray(TickValueType.LOW);
		double[] closePrices = stock.getListAsArray(TickValueType.CLOSE);

		ArrayUtils.reverse(openPrices);
		ArrayUtils.reverse(highPrices);
		ArrayUtils.reverse(lowPrices);
		ArrayUtils.reverse(closePrices);

		// Three Inside Up:
		int[] cdlResult = new int[cdlIndicatorsCfg.NUM_CDL_INDICATORS];

		for (int i = 0; i < cdlIndicatorsCfg.NUM_CDL_INDICATORS; i++) {
			if (cdlIndicatorsCfg.cdlIndicators[i] == true) {
				cdlResult[i] = genericIndicator.indicate(
						cdlIndicatorsCfg.cdlPatternName[i],
						CDLIndicators.values()[i], openPrices, highPrices,
						lowPrices, closePrices, startIndex, endIndex, size);
			}
		}

		return cdlResult;
	}

}
