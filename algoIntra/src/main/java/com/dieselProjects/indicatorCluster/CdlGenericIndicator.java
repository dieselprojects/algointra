package com.dieselProjects.indicatorCluster;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.dieselProjects.configurationCluster.CdlIndicatorsCfg;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.Types.CDLIndicators;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class CdlGenericIndicator {

	public static final int NUM_PARAMS = 9;
	public static final int NUM_ADDITIONAL_PARAMS = 1;
	CdlIndicatorsCfg cdlIndicatorsCfg;

	public CdlGenericIndicator(CdlIndicatorsCfg cdlIndicatorsCfg) {
		this.cdlIndicatorsCfg = cdlIndicatorsCfg;
	}

	public int indicate(String cdlMethodName, CDLIndicators indicatorType,
			double[] openPrices, double[] highPrices, double[] lowPrices,
			double[] closePrices, int startIndex, int endIndex, int size) {

		RetCode retCode;
		MInteger outBegIdx = new MInteger();
		MInteger outNBElement = new MInteger();

		int[] outResult = new int[size];

		try {
			Class<?> coreGeneric = Class.forName("com.tictactec.ta.lib.Core");
			Object coreObj = coreGeneric.newInstance();
			Class<?> argType[] = null;
			Method method = null;
			if (additionalParamsNeeded(indicatorType)) {
				argType = new Class[NUM_PARAMS + NUM_ADDITIONAL_PARAMS];
				argType[0] = int.class;
				argType[1] = int.class;
				argType[2] = double[].class;
				argType[3] = double[].class;
				argType[4] = double[].class;
				argType[5] = double[].class;
				argType[6] = double.class; // this is the additional param
				argType[7] = MInteger.class;
				argType[8] = MInteger.class;
				argType[9] = int[].class;

				method = coreGeneric.getMethod(cdlMethodName, argType);

				Object[] argList = new Object[NUM_PARAMS
						+ NUM_ADDITIONAL_PARAMS];

				argList[0] = startIndex;
				argList[1] = endIndex;
				argList[2] = openPrices;
				argList[3] = highPrices;
				argList[4] = lowPrices;
				argList[5] = closePrices;
				argList[6] = getAdditionalParam(indicatorType);
				argList[7] = outBegIdx;
				argList[8] = outNBElement;
				argList[9] = outResult;

				Object retObj = method.invoke(coreObj, argList);
				retCode = (RetCode) retObj;

				if (retCode != RetCode.Success) {
					System.err
							.println("Failed on candle stick pattern recognition "
									+ cdlMethodName);
				}

				outResult = (int[]) argList[9];
				outBegIdx = (MInteger) argList[7];

			} else {

				argType = new Class[NUM_PARAMS];
				argType[0] = int.class;
				argType[1] = int.class;
				argType[2] = double[].class;
				argType[3] = double[].class;
				argType[4] = double[].class;
				argType[5] = double[].class;
				argType[6] = MInteger.class;
				argType[7] = MInteger.class;
				argType[8] = int[].class;

				method = coreGeneric.getMethod(cdlMethodName, argType);

				Object[] argList = new Object[NUM_PARAMS];
				argList[0] = startIndex;
				argList[1] = endIndex;
				argList[2] = openPrices;
				argList[3] = highPrices;
				argList[4] = lowPrices;
				argList[5] = closePrices;
				argList[6] = outBegIdx;
				argList[7] = outNBElement;
				argList[8] = outResult;

				Object retObj = method.invoke(coreObj, argList);
				retCode = (RetCode) retObj;

				if (retCode != RetCode.Success) {
					System.err
							.println("Failed on candle stick pattern recognition "
									+ cdlMethodName);
				}

				outResult = (int[]) argList[8];
				outBegIdx = (MInteger) argList[6];
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outResult[size - outBegIdx.value - 1] / 100;
	}

	private boolean additionalParamsNeeded(CDLIndicators indicatorType) {

		return cdlIndicatorsCfg.cdlAdditionalParamsNeeded[indicatorType
				.getMask()];

	}

	private double getAdditionalParam(CDLIndicators indicatorType) {

		return cdlIndicatorsCfg.cdlAdditionalParamsValue[indicatorType
				.getMask()];

	}
}
