package com.dieselProjects.indicatorCluster;

import java.util.Arrays;

import com.dieselProjects.configurationCluster.DynMACDCfg;
import com.dieselProjects.supportCluster.Logger;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/*
 * MACD Buy Signal
 * A buy signal is generated when the MACD (blue line) crosses above the MACD Signal Line (red line).
 * MACD Sell Signal
 * Similarly, when the MACD crosses below the MACD Signal Line a sell signal is generated.
 */

public class MACDIndicator {

	DynMACDCfg dynMACDConfiguration;
	Logger logger = Logger.getInstance();

	RetCode macdRcore;
	Core macd;

	MInteger outBegIdx;
	MInteger outNBElement;
	double[] outMACD;
	double[] outMACDSignal;
	double[] outMACDHist;
	int result = 0;
	

	
	public MACDIndicator(DynMACDCfg dynMACDConfiguration){
		this.dynMACDConfiguration = dynMACDConfiguration;
	}
	
	
	
	
	public int indicate(String ticker, double[] prices, int startIndex,
			int endIndex, int size) {

		RetCode macdRcore;
		macd = new Core();

		outBegIdx = new MInteger();
		outNBElement = new MInteger();
		outMACD = new double[size];
		outMACDSignal = new double[size];
		outMACDHist = new double[size];
		result = 0;

		double avgValue = avgValue(prices);

		macdRcore = macd.macdExt(startIndex, endIndex, prices,
				dynMACDConfiguration.macdFastParam, dynMACDConfiguration.macdMATypeFastParam,
				dynMACDConfiguration.macdSlowParam, dynMACDConfiguration.macdMATypeSlowParam,
				dynMACDConfiguration.macdSignalParam, dynMACDConfiguration.macdMATypeSignalParam,
				outBegIdx, outNBElement, outMACD, outMACDSignal, outMACDHist);

		if (macdRcore != RetCode.Success)
			System.err.println("MACD indicator failed for " + ticker);

		String macdCalc = "prices = " + Arrays.toString(prices) + "\n"
				+ "outMACD = " + Arrays.toString(outMACD) + "\n" + "MACD["
				+ (size - outBegIdx.value - 1) + "] = "
				+ outMACD[size - outBegIdx.value - 1] + "\n" + "MACDSIGNAL = "
				+ Arrays.toString(outMACDSignal) + "\n" + "signal["
				+ (size - outBegIdx.value - 1) + "] = "
				+ outMACDSignal[size - outBegIdx.value - 1] + "\n"
				+ "MACDHist = " + Arrays.toString(outMACDHist) + "\n"
				+ "MACDHist[" + (size - outBegIdx.value - 1) + "] = "
				+ outMACDHist[size - outBegIdx.value - 1] + "\n"
				+ "outBegIdx   = " + outBegIdx.value + "\n" + "outNBElement = "
				+ outNBElement.value;

		//System.out.println(macdCalc);
		logger.addEvent(macdCalc);

		switch (dynMACDConfiguration.macdStrategyType) {
		case DEFAULT:
			result = macdDefaultStrategy(size);
			break;
		case CUSTOM1:
			result = macdCustomStrategy1(size);
			break;
		case CUSTOM2:
			result = macdCustomStrategy2(size, avgValue);
			break;
		}

		return result;

	}

	/*
	 * MACD default strategy:
	 */

	private int macdDefaultStrategy(int size) {
		int result = 0;

		if (outNBElement.value > 1) {
			// Buy indication:
			if ((outMACDHist[size - outBegIdx.value - 1] > 0)
					&& (outMACDHist[size - outBegIdx.value - 2] < 0))
				result = 1;
			// Sell indication:
			if ((outMACDHist[size - outBegIdx.value - 1] < 0)
					&& (outMACDHist[size - outBegIdx.value - 2] > 0))
				result = -1;
		}

		return result;
	}

	/*
	 * MACD custom strategy1: check past histogram. Buy: when histogram is
	 * Negative, if histogram[i:i=#n] is negative check if histogram[i] >
	 * histogram[i-1] (histogram changed direction) if so buy.
	 * 
	 * 
	 * || -----------------------||||(0) |||||||||||||||||||||||
	 * ||||||||||||||^(buy) ||||||^(buy) ||^(buy)
	 * 
	 * 
	 * Sell: when histogram is positive, if histogram[i:i=#n] is positive check
	 * if histogram[i] < histogram[i-1] (histogram changed direction) if so
	 * sell.
	 */

	private int macdCustomStrategy1(int size) {

		// Params:
		// N_ELEMENTS - number of elements to check
		// HIST_CHARGE_BUY - max hist value needed for starting the result
		// process
		// HIST_CHARGE_SELL - min hist value needed for starting the result
		// process
		//

		int N_ELEMENTS = 3;
		double HIST_CHARGE_BUY = -0.03;
		double HIST_CHARGE_SELL = 0.01;

		int index = size - outBegIdx.value - 1;

		int buyResult = 0;
		int sellResult = 0;
		boolean buyFail = false;
		boolean sellFail = false;
		boolean histCharge = false;

		if (outNBElement.value < N_ELEMENTS) {
			// Preliminary check:
			return 0;
		} else {

			// buy indication:
			for (int i = 0; i < N_ELEMENTS; i++) {

				if (outMACDHist[index - i] > 0) {
					// one or more elements are baggier than 0, unstable
					// histogram -> return 0
					buyFail = true;
				}
				if (outMACDHist[index - i] < HIST_CHARGE_BUY)
					histCharge = true;

			}

			if (histCharge == true && buyFail == false) {
				// check after peak:
				if (outMACDHist[index] > outMACDHist[index - 1]) {
					buyResult = 1;
				}
			}

			histCharge = false;

			// sell indication:
			for (int i = 0; i < N_ELEMENTS; i++) {

				if (outMACDHist[index - i] < 0) {
					// one or more elements are smaller than 0, unstable
					// histogram -> return 0
					sellFail = true;
				}
				if (outMACDHist[index - i] > HIST_CHARGE_SELL)
					histCharge = true;

			}

			if (histCharge == true && sellFail == false) {
				// check after peak:
				if (outMACDHist[index] < outMACDHist[index - 1]) {
					sellResult = -1;
				}
			}

		}
		return buyResult + sellResult;
	}

	/*
	 * MACD custom strategy2: check past histogram. Buy: when histogram is
	 * Negative, if histogram[i:i=#n] is negative check if histogram[i] >
	 * histogram[i-1] (histogram changed direction) if so buy.
	 * 
	 * 
	 * || -----------------------||||(0) ||||||||||||||||||||||| ||||||||||||||
	 * ||^(buy) |||||| ||^(buy)
	 * 
	 * 
	 * Sell: when histogram is positive, if histogram[i:i=#n] is positive check
	 * if histogram[i] < histogram[i-1] (histogram changed direction) if so
	 * sell.
	 */

	private int macdCustomStrategy2(int size, double avgValue) {

		// Params:
		// N_ELEMENTS - number of elements to check
		// HIST_CHARGE_BUY - max hist value needed for starting the result
		// process
		// HIST_CHARGE_SELL - min hist value needed for starting the result
		// process
		//

		int N_ELEMENTS = 3;
		// double HIST_CHARGE_BUY = -0.4; // (4e-6)*avgValue^2 +
		// 0.00275*avgValue + 0.08
		//double HIST_CHARGE_BUY = -((4e-6) * Math.pow(avgValue, 2) + 0.00275
		//		* avgValue + 0.08);
		double HIST_CHARGE_BUY = -((4e-6) * Math.pow(avgValue, 2) + 0.00275
				* avgValue + 0.08)/40;
		// double HIST_CHARGE_SELL = 0.3;
		//double HIST_CHARGE_SELL = ((4e-6) * Math.pow(avgValue, 2) + 0.00275
		//		* avgValue + 0.08) * 0.75;
		double HIST_CHARGE_SELL = (((4e-6) * Math.pow(avgValue, 2) + 0.00275
				* avgValue + 0.08) * 0.75)/40;

		int index = size - outBegIdx.value - 1;

		int buyResult = 0;
		int sellResult = 0;
		boolean buyFail = false;
		boolean sellFail = false;
		boolean histCharge = false;

		if (outNBElement.value < N_ELEMENTS) {
			// Preliminary check:
			return 0;
		} else {

			// buy indication:
			for (int i = 0; i < N_ELEMENTS; i++) {

				if (outMACDHist[index - i] > 0) {
					// one or more elements are baggier than 0, unstable
					// histogram -> return 0
					buyFail = true;
				}
				if (outMACDHist[index - i] < HIST_CHARGE_BUY)
					histCharge = true;

			}

			if (histCharge == true && buyFail == false) {

				// find last peak index:
				int lastPeak = index;
				for (int i = 0; i < ((N_ELEMENTS == outNBElement.value) ? N_ELEMENTS - 1
						: N_ELEMENTS); i++) {
					int peakIndex = i;
					if (outMACDHist[lastPeak - peakIndex] < outMACDHist[lastPeak
							- peakIndex - 1]) {
						lastPeak -= peakIndex;
						break;
					}
				}

				buyResult = ((index - lastPeak) == 1) ? 1 : 0;

			}

			histCharge = false;

			// sell indication:
			for (int i = 0; i < N_ELEMENTS; i++) {

				if (outMACDHist[index - i] < 0) {
					// one or more elements are smaller than 0, unstable
					// histogram -> return 0
					sellFail = true;
				}
				if (outMACDHist[index - i] > HIST_CHARGE_SELL)
					histCharge = true;

			}

			if (histCharge == true && sellFail == false) {
				// find last peak index:
				int lastPeak = index;
				for (int i = 0; i < ((N_ELEMENTS == outNBElement.value) ? N_ELEMENTS - 1
						: N_ELEMENTS); i++) {
					int peakIndex = i;
					if (outMACDHist[lastPeak - peakIndex] > outMACDHist[lastPeak
							- peakIndex - 1]) {
						lastPeak -= peakIndex;
						break;
					}
				}
				buyResult = ((index - lastPeak) == 1) ? -1 : 0;
			}

		}

		if (buyResult == 1 && sellResult == -1) {
			System.err
					.println("MACD indicator indicate both buy and sell indication");
			System.exit(1);
		}
		return buyResult + sellResult;
	}

	private double avgValue(double[] prices) {

		double sum = 0.0;

		for (int i = 0; i < prices.length; i++)
			sum = sum + prices[i];

		// calculate average value
		return sum / prices.length;

	}

}
