package com.dieselProjects.indicatorCluster;

import com.dieselProjects.configurationCluster.DynSRSICfg;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/*
 * SRSI definition
 *
 * Buy - when SRSI crosses above oversold line (30)
 * Sell - when SRSI crosses below overbought line (70)
 * 
 */

public class SRSIIndicator {

	DynSRSICfg dynSRSICfg;

	public SRSIIndicator(DynSRSICfg dynSRSICfg) {
		this.dynSRSICfg = dynSRSICfg;
	}

	public int indicate(String ticker, double[] prices, int startIndex,
			int endIndex, int size) {

		RetCode srsiCode;
		Core srsi = new Core();

		MInteger outBegIdx = new MInteger();
		MInteger outNBElement = new MInteger();
		double[] outSRSIK = new double[size];
		double[] outSRSID = new double[size];
		int result = 0;

		srsiCode = srsi.stochRsi(startIndex, endIndex, prices, (int) (-4e+37),
				dynSRSICfg.srsiFastKParam, dynSRSICfg.srsiFastDParam,
				dynSRSICfg.srsiMATypeDParam, outBegIdx, outNBElement, outSRSIK,
				outSRSID);

		if (srsiCode != RetCode.Success)
			System.err.println("SRSI indicator failed for " + ticker);

		// check data validity
		if (outNBElement.value > 0)
			result = (outSRSIK[size - outBegIdx.value - 1] < dynSRSICfg.srsiBuyParam) ? 1
					: (outSRSIK[size - outBegIdx.value - 1] > dynSRSICfg.srsiSellParam) ? -1
							: 0;
		return result;

	}

}
