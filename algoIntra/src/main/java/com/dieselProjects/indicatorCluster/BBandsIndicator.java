package com.dieselProjects.indicatorCluster;

import com.dieselProjects.configurationCluster.DynBBandsCfg;
import com.dieselProjects.supportCluster.Logger;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class BBandsIndicator {

	DynBBandsCfg dynBBandsConfiguration;
	Logger logger = Logger.getInstance();

	RetCode bbandsRCode;
	Core bbands = new Core();

	MInteger outBegIdx;
	MInteger outNBElement;
	double[] outUpper;
	double[] outMid;
	double[] outLower;
	int result = 0;

	public BBandsIndicator(DynBBandsCfg dynBBandsConfiguration){
		this.dynBBandsConfiguration = dynBBandsConfiguration;
	}
	
	
	public int indicate(String ticker, double[] prices, int startIndex,
			int endIndex, int size) {

		RetCode bbandsRCode;
		Core bbands = new Core();

		outBegIdx = new MInteger();
		outNBElement = new MInteger();
		outUpper = new double[size];
		outMid = new double[size];
		outLower = new double[size];

		bbandsRCode = bbands.bbands(startIndex, endIndex, prices,
				dynBBandsConfiguration.bbandsTimePeriod, dynBBandsConfiguration.bbandsPeriodUp,
				dynBBandsConfiguration.bbandsPeriodDown, dynBBandsConfiguration.bbandsMATypeParam,
				outBegIdx, outNBElement, outUpper, outMid, outLower);

		if (bbandsRCode != RetCode.Success)
			System.err.println("BBANDS failed on " + ticker);

		if (outNBElement.value > 0) {
			
			switch (dynBBandsConfiguration.bbandsStrategyType){
			case DEFAULT:
			result = bbandsDefaultStrategy(prices, size);
				break;
			case CUSTOM1:
			result = bbandsCustomStrategy1(prices, size);
				break;
			case CUSTOM2:
			result = bbandsCustomStrategy2(prices, size);
				break;
			}
			
			
		}

		return result;
	}

	private int bbandsDefaultStrategy(double[] prices, int size) {

		return (prices[size - 1] < outLower[size - outBegIdx.value - 1]) ? 1
				: (prices[size - 1] > outUpper[size - outBegIdx.value - 1]) ? -1
						: 0;

	}

	private int bbandsCustomStrategy1(double[] prices, int size) {

		int BBANDS_LOOK_BACK = dynBBandsConfiguration.bbandsLookBack;
		int result = 0;

		if (outNBElement.value > BBANDS_LOOK_BACK) {
			for (int i = 0; i < BBANDS_LOOK_BACK; i++) {
				if (prices[size - 1 - i] < outLower[size - outBegIdx.value - 1
						- i]) {
					result = 1;
				} else if (prices[size - 1 - i] > outUpper[size
						- outBegIdx.value - 1 - i]) {
					result = -1;
				}
			}
		}
		return result;
	}

	private int bbandsCustomStrategy2(double[] prices, int size) {

		int BBANDS_LOOK_BACK = dynBBandsConfiguration.bbandsLookBack;
		double MARGIN = dynBBandsConfiguration.bbandsMargin; // 5% margin
		int result = 0;
		int index = size - outBegIdx.value - 1;

		if (outNBElement.value > BBANDS_LOOK_BACK) {
			for (int i = 0; i < BBANDS_LOOK_BACK; i++) {

				if (prices[size - 1 - i] < (outLower[index - i] + (outUpper[index
						- i] - outLower[index - i])
						* MARGIN)) {

					result = 1;

				} else if (prices[size - 1 - i] > (outUpper[index - i] - (outUpper[index
						- i] - outLower[index - i])
						* MARGIN)) {

					result = -1;

				}

			}
		}
		return result;
	}
}
