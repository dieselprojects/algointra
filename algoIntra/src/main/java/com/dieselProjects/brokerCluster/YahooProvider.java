package com.dieselProjects.brokerCluster;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.OneStockDB;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Types.OPType;

public class YahooProvider implements OnlineProvider {
	private static final String TAG = "YahooProvider";

	Configuration configuration = Configuration.getInstance();
	FundsMan funds = FundsMan.getInstance();

	public String OPname = "Yahoo Provider";
	boolean useDB = false;

	public String getName() {
		return OPname;
	}

	public YahooProvider() {

	}

	public YahooProvider(boolean useDB) {
		this.useDB = useDB;
	}

	public ArrayList<Tick> requestPrices(String[] tickers) {

		// create a request for yahoo with all given ticker - up to 150 tickers
		// at a time
		// send request for yahoo for current prices or daily prices
		// Called should not send a request for more than 100 tickers
		// Added implementation for simulated ticks (dailyTicks only)

		ArrayList<Tick> returnedTickList = new ArrayList<Tick>();

		String requestOperands = "sl1ohgl1v"; // Ticker, price, open, high, low,
												// close, volume;

		String tickerString = "";
		for (String nextTicker : tickers) {
			tickerString += "+" + nextTicker;
		}

		// remove first '+'
		tickerString = tickerString.substring(1);

		try {

			// Generate yahoo price call:
			// --------------------------

			URL yahooPricesCall;
			yahooPricesCall = new URL(
					"http://finance.yahoo.com/d/quotes.csv?s=" + tickerString
							+ "&f=" + requestOperands + "&ignore=.csv");

			URLConnection yc = yahooPricesCall.openConnection();

			// Read Input Stream:
			BufferedReader in = new BufferedReader(new InputStreamReader(
					yc.getInputStream()));

			String pricesLine = "";
			while ((pricesLine = in.readLine()) != null) {
				String[] splitString = pricesLine.split(",");

				// Current request time
				Calendar currentTime = new GregorianCalendar();

				Tick nextPriceTick;

				nextPriceTick = new Tick(splitString[0], // ticker
						Double.valueOf(splitString[2]), // open
						Double.valueOf(splitString[3]), // high
						Double.valueOf(splitString[4]), // low
						Double.valueOf(splitString[5]), // close
						Integer.valueOf(splitString[6]), // volume
						currentTime); // current time

				// Add to returned List
				returnedTickList.add(nextPriceTick);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

		return returnedTickList;
	}

	public Bid placeBid(Bid bid) {

		switch (bid.opType) {
		case BUY:
			if (funds.updateFunds(bid)) {

				// updating internal stock for buy and updating generalChart
				bid.stock.stockBuy(bid);

			} else {
				System.err.println("Incorrent funds to buy this bid");
				System.err.println(bid.print());
				System.exit(1);
			}

			break;
		case SELL:
			funds.updateFunds(bid);
			bid.stock.stockSell(bid);

			break;
		default:
			System.err.println(TAG + " Invalid opType for placeBid");
			System.exit(1);
			break;

		}

		return bid;
	}
	
	public List<Bid> placeBids(List<Bid> bids){
		LinkedList<Bid> placedBids = new LinkedList<Bid>();
		
		Iterator<Bid> bidsItr = bids.iterator();
		while(bidsItr.hasNext()){
			placedBids.add(placeBid(bidsItr.next()));
		}
				
		return placedBids;
	}
	
	

	public LinkedList<Tick> getPastTicks(String ticker, String startDate,
			String endDate, String sample) {

		LinkedList<Tick> ticks = new LinkedList<Tick>();

		if (useDB == false) {
			try {
				ticks = getOnlineTicks(ticker, startDate, endDate, sample);
			} catch (IOException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			ticks = getDBTicks(ticker, startDate, endDate, sample);
		}

		return ticks;

	}

	public LinkedList<Tick> getOnlineTicks(String ticker, String startDate,
			String endDate, String sample) throws IOException, ParseException {

		// setting Date for Yahoo call:
		String[] sSD = startDate.split("\\.");
		String[] sED = endDate.split("\\.");

		// Instantiate prices List:
		LinkedList<Tick> tickList = new LinkedList<Tick>();

		// Generating URL req for Yahoo:
		String reqURL = new String("http://ichart.yahoo.com/table.csv?s="
				+ ticker + "&a=" + String.valueOf(Integer.valueOf(sSD[1]) - 1) + // Start
																					// Month
																					// -
																					// 1
				"&b=" + sSD[0] + // Start Day
				"&c=" + sSD[2] + // Start Year
				"&d=" + String.valueOf(Integer.valueOf(sED[1]) - 1) + // End
																		// Month
																		// - 1
				"&e=" + sED[0] + // End Day
				"&f=" + sED[2] + // End Year
				"&g=" + sample + "&ignore=.csv");
		System.out.println(reqURL);

		URL yahooPricescall = new URL(reqURL); // Sample in days

		// Open URL connection:
		URLConnection yc = yahooPricescall.openConnection();

		// Read Input Stream:
		BufferedReader in = new BufferedReader(new InputStreamReader(
				yc.getInputStream()));

		// first line isn't prices - getting the next line
		String pricesLine = in.readLine();

		while ((pricesLine = in.readLine()) != null) {
			String[] splitString = pricesLine.split(",");

			// Accumulate all prices in the test, later this prices will be send
			// one by one (like in the real world)
			// -----------------------------------------------------------------------------------------------------
			// Date:
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Calendar date = Calendar.getInstance();
			date.setTime(format.parse(splitString[0]));
			Tick newTick = new Tick(ticker, Double.valueOf(splitString[1]),
					Double.valueOf(splitString[2]),
					Double.valueOf(splitString[3]),
					Double.valueOf(splitString[4]),
					Integer.valueOf(splitString[5]), date);
			tickList.add(0, newTick);

		}
		return tickList;
	}

	public LinkedList<Tick> getDBTicks(String ticker, String startDate,
			String endDate, String sample) {
		LinkedList<Tick> ticks = new LinkedList<Tick>();

		// check if data base exists:
		File dbFile = new File("/disk/workarea/algoTrader/" + ticker + "/"
				+ sample + "/" + startDate + ".db");

		if (!dbFile.exists()) {
			System.err.println("Data base does not exists for " + ticker
					+ " on " + startDate);
			System.exit(2);
		}

		// if exists load data base:
		OneStockDB stockDB = null;
		try {

			InputStream is = new FileInputStream(dbFile);
			InputStream bf = new BufferedInputStream(is);
			ObjectInput oInput = new ObjectInputStream(bf);
			stockDB = (OneStockDB) oInput.readObject();
			oInput.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ticks = stockDB.ticks;
		return ticks;
	}

}
