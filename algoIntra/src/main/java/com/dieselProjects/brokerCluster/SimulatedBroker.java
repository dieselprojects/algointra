package com.dieselProjects.brokerCluster;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.PositionBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.beans.TransactionBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.SimulateTimeMan;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.StockStateType;

// Simulated Broker:
// -----------------

// General:
// --------

// Handle simulated tick and prices 
// holds 

// Singleton implementation:	

public class SimulatedBroker implements BrokerIF {

	// ---------------------------- //
	// - Singleton implementation - //
	// ---------------------------- //

	private static final SimulatedBroker broker = new SimulatedBroker();

	public static SimulatedBroker getInstance() {
		return broker;
	}

	private OnlineProvider onlineProvider;
	Configuration configuration;
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	int EndOfTest;
	BufferedReader FilteredReader;
	// public ArrayList<algoTesterBean> testerArray;
	FundsMan funds;
	private Logger logger = Logger.getInstance();

	private int ID = 0;

	// --------------- //
	// - Constructor - //
	// --------------- //

	private SimulatedBroker() {

		System.out
				.println("__Constructor__ Simulated Broker, Creating Simulated Data");

		// ---------------------------- //
		// - Creating Stimulated Data - //
		// ---------------------------- //

		configuration = Configuration.getInstance();
		switch (configuration.providerType) {
		case YAHOO_PROVIDER:
			System.out
					.println("Creating onlineProvider, providerType - Yahoo provider");
			onlineProvider = new YahooProvider();
			break;
		case DB_PROVIDER:
			System.out
					.println("Creating offlineProvider, providerType - DB provider");
			onlineProvider = new YahooProvider(true);
			break;
		default:
			System.err.println("Invalid provider type selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}

		// Date parameters:
		String startDate = configuration.startDate;
		String endDate = configuration.endDate;
		String sample = configuration.sample;

		// Array list of Stock Prices overTime:
		// ------------------------------------
		// testerArray = new ArrayList<algoTesterBean>();

		// Generating d:
		// -------------------------------------
		// Each stock will create a new LinkedList and the Linked list will be
		// added to the ArrayList in a new place

		try {
			File filterStocks = new File(this.getClass().getResource("/filteredStocks.csv").toURI());
			FilteredReader = new BufferedReader(new FileReader(filterStocks));
			String ticker = "";

			while ((ticker = FilteredReader.readLine()) != null) {

				ticker = ticker.replaceAll("\"", "");

				// Setting simulated ticks:
				configuration.simulateCfg.testerArray.put(ticker,
						onlineProvider.getPastTicks(ticker, startDate, endDate,
								sample));

				Iterator<Tick> tickIterator = configuration.simulateCfg.testerArray
						.get(ticker).iterator();
				while (tickIterator.hasNext()) {
					simTimeMan.simulatedDates.addLast(tickIterator.next().date);
				}
				// Currently in algotester there should be only one stock
				// if configuration states, store prices to file, need to add
				// volume
				if (configuration.enableStoreToFile == true) {

					try {
						PrintWriter writer = new PrintWriter(
								configuration.storeDBFile, "UTF-8");
						// writer.print(configuration.simulateCfg.testerArray.get(
						// 0).printAll());
						writer.close();
					} catch (FileNotFoundException
							| UnsupportedEncodingException e) {
						System.err
								.println("Failed to store Stock in algoTester");
						e.printStackTrace();
						System.exit(2);
					}

				}

				// 100 ms wait until next request due to overload constraint on
				// Yahoo CSV API
				Thread.sleep(100);
			}
			FilteredReader.close();

		} catch (Exception e) {

			System.err.println("Simulate Broker, Constructor Read File Error");
			System.out.println(e.getMessage());
			System.exit(1);

		}

		// Setting class Variables:
		// ------------------------

		simTimeMan.iteration = 0;

		EndOfTest = configuration.simulateCfg.getEndOfTest();

		// Getting funds singleton:
		// ------------------------
		funds = FundsMan.getInstance();

		System.out
				.println("Simulated Broker Constructor, Done Creating Simulated Data, Total of planed simTimeMan.iteration until EOT: "
						+ EndOfTest);
	}

	// ------------- //
	// - Get Tick - //
	// ------------- //
	public Tick getTick(String ticker) {

		Tick tick = null;

		// find ticker in configuration.simulateCfg.testerArray:
		// ---------------------------
		tick = configuration.simulateCfg.testerArray.get(ticker).get(
				simTimeMan.iteration);

		if (tick != null) {
			return tick;
		} else {
			System.err.println("Ticker not Found in Tester Array: " + ticker);
			System.exit(1);
		}

		return null;

	}

	public List<Tick> getTicks(List<String> tickerList) {

		List<Tick> returnedArray = new LinkedList<Tick>();

		for (String nextTicker : tickerList) {
			returnedArray.add(getTick(nextTicker));
		}

		if (configuration.simulatedDelay) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.err
						.println("__ERR__ Simulated Delay failed, Broker.getTick");
				System.exit(1);
			}
		}

		return returnedArray;

	}

	// -------------- //
	// - Place Bids - //
	// -------------- //

	// Sale useCase:
	// -------------
	// d. funds get the DoneBids and removeFunds according to the doneBids

	public List<Bid> placeBids(List<Bid> bids) {

		return onlineProvider.placeBids(bids);

	}

	// ----------------- //
	// - getAllTickers - //
	// ----------------- //

	public List<String> getAllTickers(Boolean getTickersFromFile) {
		List<String> tickerArray = new ArrayList<String>();

		for (String ticker : configuration.simulateCfg.testerArray.keySet()) {
			tickerArray.add(ticker);
		}

		return tickerArray;
	}

	// Advance iteration number for simulated broker
	public Boolean advIteration() {
		simTimeMan.iteration++;
		return (simTimeMan.iteration != EndOfTest);

	}

	// TODO: future implementation can generate a simulated date and not
	// iteration
	// this will be before the uploading
	public int getIteration() {
		return simTimeMan.iteration;
	}
	
	public boolean getEOT(){
		return (simTimeMan.iteration ==  (EndOfTest-1));
	}

}
