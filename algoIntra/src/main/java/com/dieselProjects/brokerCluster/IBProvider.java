package com.dieselProjects.brokerCluster;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.FundsMan;
import com.ib.contracts.StkContract;
import com.ib.controller.ApiConnection.ILogger;
import com.ib.controller.ApiController;
import com.ib.controller.ApiController.IConnectionHandler;
import com.ib.controller.ApiController.IRealTimeBarHandler;
import com.ib.controller.Bar;
import com.ib.controller.NewContract;
import com.ib.controller.Types.WhatToShow;

public class IBProvider implements OnlineProvider, IConnectionHandler {

	Configuration configuration = Configuration.getInstance();
	ApiController twsController;
	FundsMan funds = FundsMan.getInstance();
	SyncTicksCollector ticksCollector;

	private class twsLogger implements ILogger {
		@Override
		public void log(String valueOf) {
		}
	}

	twsLogger inLogger = new twsLogger();
	twsLogger outLogger = new twsLogger();

	public IBProvider(SyncTicksCollector ticksCollector) {
		twsController = new ApiController(this, inLogger, outLogger);
		twsController.connect(configuration.twsHost, configuration.twsPort,
				configuration.twsID);
		this.ticksCollector = ticksCollector;
	}
	
	
	public void registerRealTimeBars(String ticker){
	
		
		StkContract contract = new StkContract(ticker);
		contract.m_exchange = "SMART";
		contract.m_secType = "STK";
		contract.m_primaryExch = "NASDAQ";
		contract.m_currency = "USD";
		RealTimeBarsHandler realTimeBarsHandler = new RealTimeBarsHandler();
		RealTimeBarsCollector barsCollector = new RealTimeBarsCollector(ticker,ticksCollector);
		
		realTimeBarsHandler.barsCollector = barsCollector;
				
		NewContract newContract = new NewContract(contract);
		twsController.reqRealTimeBars(newContract, WhatToShow.TRADES, true,
				realTimeBarsHandler);
		
		System.out.println("Register to " + ticker);
	}
	
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Tick> requestPrices(String[] tickers) {

		return null;

	}

	@Override
	public List<Bid> placeBids(List<Bid> bids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkedList<Tick> getPastTicks(String ticker, String startDate,
			String endDate, String sample) throws IOException, ParseException {
		return null;
	}

	@Override
	public void connected() {
		System.out.println("connected");
	}

	@Override
	public void disconnected() {
		System.out.println("Not connected");
	}

	@Override
	public void accountList(ArrayList<String> list) {
		// TODO Auto-generated method stub

	}

	@Override
	public void error(Exception e) {
		// TODO Auto-generated method stub
		System.out.println(e.getMessage());
	}

	@Override
	public void message(int id, int errorCode, String errorMsg) {
		System.err.println("Recieves error message ID " + errorCode + " MSG " + errorMsg);
		if (errorCode == 504){
			System.err.println("Connect to TWS server");
			System.exit(1);
		}
	}

	@Override
	public void show(String string) {
		// TODO Auto-generated method stub

	}

	private class RealTimeBarsHandler implements IRealTimeBarHandler{

		public RealTimeBarsCollector barsCollector;
		
		@Override
		public void realtimeBar(Bar bar) {
			System.out.println("Recieved bar");
			barsCollector.add(bar);
		}

	}

}
