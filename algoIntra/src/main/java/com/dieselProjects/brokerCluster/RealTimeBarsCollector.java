package com.dieselProjects.brokerCluster;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.dieselProjects.beans.Tick;
import com.ib.controller.Bar;

public class RealTimeBarsCollector {

	int BARS_TO_TICKS = 1;
	
	String ticker = "";
	Tick resultTick = null;
	int counter = 0;
	SyncTicksCollector ticksCollector;

	public RealTimeBarsCollector(String ticker, SyncTicksCollector ticksCollector){
		this.ticker = ticker;
		this.ticksCollector = ticksCollector;
	}
	
	
	public void add(Bar bar) {
		
		System.out.println("Adding new 5 second bar for " + ticker);
		// update 1M tick here:
		resultTick = updateTick(bar);
		counter++;

	}

	private Tick updateTick(Bar bar) {
		Tick processedTick = resultTick;
		if (counter == 0) {

			Calendar time = GregorianCalendar.getInstance();
			time.setTimeInMillis(bar.time() * 1000);
			Long volume = new Long(bar.volume());
			processedTick = new Tick(ticker, bar.open(), bar.high(), bar.low(),
					bar.close(), volume.intValue(), time);
		} else {
			if (counter == BARS_TO_TICKS) {
				counter = 0;
				processedTick.setClose(bar.close());
				
				// push tick to some syncronized array
				ticksCollector.addTick(processedTick);
			}

			if (processedTick.low() > bar.low()) {
				processedTick.setLow(bar.low());
			}

			if (processedTick.high() > bar.high()) {
				processedTick.setHigh(bar.high());
			}
			Long volume = new Long(bar.volume());
			processedTick.volume += volume.intValue();

		}
		
		return processedTick;
	}
}
