package com.dieselProjects.brokerCluster;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.PositionBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.beans.TransactionBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.controllerCluster.TWSController;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.SimulateTimeMan;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.StockStateType;

public class TWSBroker implements BrokerIF {

	private static final TWSBroker broker = new TWSBroker();

	public static TWSBroker getInstance() {
		return broker;
	}

	private IBProvider provider;
	public SyncTicksCollector ticksCollector;

	private Configuration configuration = Configuration.getInstance();
	private Logger logger = Logger.getInstance();
	public TWSController twsController;
	
	
	// for simulated bids:
	
	private int ID = 0;
	FundsMan funds = FundsMan.getInstance(); ;
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	// --------------- //
	// - Constructor - //
	// --------------- //

	private TWSBroker() {

		System.out.println("__Constructor__ TWS Broker");
		ticksCollector = new SyncTicksCollector();
		provider = new IBProvider(ticksCollector);

	}

	public Tick getTick(String ticker) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Tick> getTicks(List<String> tickerArray) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Bid> placeBids(List<Bid> bids) {

		return provider.placeBids(bids);

	}

	public List<String> getAllTickers(Boolean getTickersFromFile) {

		List<String> tickerArray = new ArrayList<String>();

		if (getTickersFromFile == true) {

			// Loading Filtered stock list and storing in tickerArray
			try {

				BufferedReader FilteredReader = new BufferedReader(
						new FileReader(configuration.filteredStockFile));
				String tickerString = "";

				while ((tickerString = FilteredReader.readLine()) != null) {

					tickerString = tickerString.replaceAll("\"", "");
					tickerArray.add(tickerString);

				}
				FilteredReader.close();

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}

		} else {
			System.err.println("Should not get tickers from broker! ");
			System.exit(1);
		}

		return tickerArray;
	}

	public void registerRealTimeBars(List<String> tickers) {

		// create a new thread for each stock
		Iterator<String> stockItr = tickers.iterator();
		while (stockItr.hasNext()) {
			String ticker = stockItr.next();
			provider.registerRealTimeBars(ticker);

		}

	}

}
