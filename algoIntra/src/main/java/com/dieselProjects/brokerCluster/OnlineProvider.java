package com.dieselProjects.brokerCluster;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.Tick;

public interface OnlineProvider {
	

	public String OPname = "";
	
	public String getName();
	
	// ------------------ //
	// - Request Prices - //
	// ------------------ //
	
	public ArrayList<Tick> requestPrices(String[] tickers);
	
	

	public List<Bid> placeBids(List<Bid> bids);
	
	
	// ---------------- //
	// - getPastTicks - //
	// ---------------- //
	
	public LinkedList<Tick> getPastTicks(String ticker, String startDate, String endDate, String sample) throws IOException, ParseException;

}
