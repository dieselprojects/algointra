package com.dieselProjects.brokerCluster;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Tick;
import com.dieselProjects.controllerCluster.TWSController;

public class SyncTicksCollector {

	List<Tick> allTicks;
	public TWSController twsController;

	public SyncTicksCollector() {
		allTicks = new LinkedList<Tick>();
	}

	public void addTick(Tick tick) {
		synchronized (this) {
			tick.print();
			allTicks.add(tick);

			if (allTicks.size() == 4) {
				LinkedList<Tick> sentTickArray = new LinkedList<Tick>();
				for (Tick nextTick : allTicks) {
					sentTickArray.add(new Tick(nextTick.ticker,
							nextTick.open(), nextTick.high(),
							nextTick.low(), nextTick.close(),
							nextTick.volume(), nextTick.date()));
				}
				System.out
						.println("Preparing to start iteration from SyncTickCollector");
				try{
					twsController.startIteration(sentTickArray);
				} catch (Exception e){
					e.printStackTrace();
				}
				allTicks = new ArrayList<Tick>();

			}
		}
	}
}
