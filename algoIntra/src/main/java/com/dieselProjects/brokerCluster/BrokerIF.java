package com.dieselProjects.brokerCluster;

import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.Tick;


public interface BrokerIF {
	
	
	// ---------------------------- //
	// - Get Tick (single ticker) - //
	// ---------------------------- //
	
	public Tick getTick(String ticker);
	
	
	// ------------------------------- //
	// - Get Tick (By ticker vector) - //
	// ------------------------------- //
	
	// Call Broker and Get Tick information: PriceTick - Current price tick Information, 
	// Daily Tick Last Trade Day prices Information
	// Input: ArrayList<String> tickers
	// Output: ArrayList<DailyTick>
	
	public List<Tick> getTicks(List<String> tickerList);
	
	
	// -------------- //
	// - Place Bids - //
	// -------------- //
	
	// Place arrayList of bids
	public List<Bid> placeBids(List<Bid> bids);
	
	
	// --------------------- //
	// - Get StocksTickers - //
	// --------------------- //
	
	public List<String> getAllTickers(Boolean getTickersFromFile);
	
	
	
}
