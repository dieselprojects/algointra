package com.dieselProjects.confirmation;

import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;

public interface HumanIF {

	public Bid getConfirmation(Bid requestBid);
	
	public List<Bid> getConfirmation(List<Bid> bidsList);
}
