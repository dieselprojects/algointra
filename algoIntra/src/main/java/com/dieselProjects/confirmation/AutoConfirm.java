package com.dieselProjects.confirmation;

import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;

public class AutoConfirm implements HumanIF {

	@Override
	public Bid getConfirmation(Bid requestBid) {
		requestBid.approvedHuman = true;
		return requestBid;
	}

	@Override
	public List<Bid> getConfirmation(List<Bid> bidsList) {
		List<Bid> approvedBids = new LinkedList<Bid>();
				
		for(Bid nextBid : bidsList){
			Bid returnedBid = getConfirmation(nextBid);
		
			if (returnedBid.approvedHuman == true){
				approvedBids.add(returnedBid);
			}
		}
	
		return approvedBids;		
		
	}

}
