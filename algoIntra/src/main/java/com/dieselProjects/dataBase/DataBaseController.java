package com.dieselProjects.dataBase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.supportCluster.Logger;
import com.dieselProjects.supportCluster.Types.BrokerTickType;

public class DataBaseController {

	// ------------------- //
	// - Class Variables - //
	// ------------------- //

	StockDB dataBase;
	Logger logger = Logger.getInstance();

	// --------------- //
	// - Constructor - //
	// --------------- //

	public DataBaseController(String name) {

		dataBase = new StockDB(name + "_InternalDB");
	}

	// ----------------- //
	// - Class methods - //
	// ----------------- //

	// ----------------------------------- //
	// - Get StockBean & StockBean Array - //
	// ----------------------------------- //

	public StockBean getStockBean(String ticker) {
		return dataBase.getBean(ticker);
	}

	public List<StockBean> getStockBean(
			List<String> stockBeanTickerArr) {

		List<StockBean> stockBeanArray = new ArrayList<StockBean>(
				stockBeanTickerArr.size());

		for (String nextTicker : stockBeanTickerArr) {
			if (dataBase.isExist(nextTicker)) {
				stockBeanArray.add(dataBase.getBean(nextTicker));
			} else {
				System.err.println("Ticker ERROR in DataBase: "
						+ dataBase.dataBaseName + "- no ticker Value for "
						+ nextTicker);
			}
		}

		return stockBeanArray;

	}

	// Get all Stocks by Bean:
	// -----------------------

	public List<StockBean> getAllStockBean() {

		List<StockBean> stockList = dataBase.getAllBeans();
		List<StockBean> stockArray = new ArrayList<StockBean>(
				stockList.size());

		for (StockBean nextStock : stockList) {
			stockArray.add(nextStock);
		}

		return stockArray;

	}

	// Get all Stocks by string:
	// -------------------------

	public List<String> getAllStocksBean() {

		List<StockBean> stockArray = getAllStockBean();
		List<String> stockStringList = new ArrayList<String>(
				stockArray.size());

		for (StockBean nextStock : stockArray) {
			stockStringList.add(nextStock.ticker);
		}

		return stockStringList;

	}

	// ----------------------------------- //
	// - Add StockBean & StockBean Array - //
	// ----------------------------------- //

	public void addStockBean(StockBean bean) {
		dataBase.addBean(bean);
	}

	public void addStockBeans(List<StockBean> stockBeanArr) {

		for (StockBean nextBean : stockBeanArr) {
			addStockBean(nextBean);
		}
	}

	// -------------------------------------------- //
	// - add Stockbean & StockBean Array by Ticks - //
	// -------------------------------------------- //

	public void addStockBean(Tick tick, BrokerTickType brokerTickType) {

		StockBean bean;
		bean = new StockBean(tick);

		dataBase.addBean(bean);
	}

	public void addStockBean(ArrayList<Tick> ticksArr,
			BrokerTickType brokerTickType) {

		for (Tick nextTick : ticksArr) {
			addStockBean(nextTick, brokerTickType);
		}
	}

	// ------------------------------------ //
	// - updateBeanPrices - //
	// - Update Data base with new Prices - //
	// ------------------------------------ //

	public Boolean updateBeanPrices(Tick tick) {

		// Get tick.ticker StockBean,
		// Update Prices in regard to tickType
		// write back.
		// if ticker doesn't exist, return false, and ERR

		if (dataBase.isExist(tick.ticker) == false) {
			System.err.println("__ERR__, updateBeanPrices for: "
					+ dataBase.dataBaseName + ", Failed, ticker: "
					+ tick.ticker + ", not exists");
			return false;
		}

		// Get bean
		StockBean bean = getStockBean(tick.ticker);

		if (tick.high() == 0 && tick.low() == 0) {
			System.err
					.println("__ERR__, casting to daily Tick from tick failed");
			System.exit(1);
		}

		// Update Prices
		bean.addTick(tick);

		// WriteBack:
		dataBase.addBean(bean);

		return true;

	}

	// ------------------------------------------ //
	// - updateBeansPrice - //
	// - Update DataBase with new Prices Vector - //
	// ------------------------------------------ //

	public List<Boolean> updateBeanPrices(List<Tick> nextTickArr) {

		List<Boolean> returnList = new ArrayList<Boolean>(
				nextTickArr.size());

		for (Tick nextTick : nextTickArr) {
			returnList.add(updateBeanPrices(nextTick));
		}

		return returnList;

	}

	// -------------------------------------- //
	// - Remove StockBean & Stockbean Array - //
	// -------------------------------------- //

	public StockBean removeStockBean(StockBean bean) {
		return dataBase.removeBean(bean);
	}

	public List<StockBean> removeStockBean(
			List<StockBean> stockBeanArr) {

		List<StockBean> stockBeanReturned = new ArrayList<StockBean>(
				stockBeanArr.size());

		for (StockBean nextBean : stockBeanArr) {
			if (dataBase.isExist(nextBean)) {
				stockBeanReturned.add(removeStockBean(nextBean));
			} else {
				System.err.println("Ticker Removal ERROR in DataBase: "
						+ dataBase.dataBaseName + "- no ticker Value for "
						+ nextBean.ticker);
			}
		}

		return stockBeanReturned;

	}

	public Integer dataBaseSize() {
		return dataBase.size;
	}



}
