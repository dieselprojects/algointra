package com.dieselProjects.dataBase;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.dieselProjects.beans.StockBean;

public class StockDB {

	// ------------------- //
	// - Class Variables - //
	// ------------------- //

	// Hash DataBase, Stores Beans:
	private HashMap<String, StockBean> dataBase;

	// DataBase Size:
	protected int size;

	// DataBase Name (For Printing & Debug Purposes):
	protected String dataBaseName;

	// Amount of Samples taken on Stocks
	protected Integer stockSamples;

	// --------------- //
	// - Constructor - //
	// --------------- //

	public StockDB(String name) {
		this.dataBase = new HashMap<String, StockBean>();
		this.size = 0;
		this.dataBaseName = name;
		this.stockSamples = 0;
	}

	// Constructor for online trader
	public StockDB(String name, Integer stockSamples, Integer size) {
		this.dataBase = new HashMap<String, StockBean>();
		this.size = size;
		this.dataBaseName = name;
		this.stockSamples = stockSamples;
	}

	// ------------------------------ //
	// - stock exist (by StockBean) - //
	// ------------------------------ //

	public boolean isExist(StockBean bean) {
		return dataBase.containsKey(bean.ticker);
	}

	// --------------------------- //
	// - stock exist (by ticker) - //
	// --------------------------- //

	public boolean isExist(String ticker) {
		return dataBase.containsKey(ticker);
	}

	// ------------------ //
	// - Get StockBean - //
	// ------------------ //

	public StockBean getBean(String ticker) {
		return dataBase.get(ticker);
	}

	// -------------------- //
	// - Remove StockBean - //
	// -------------------- //

	public StockBean removeBean(StockBean bean) {
		return dataBase.remove(bean.ticker);
	}

	// -------------------------- //
	// - ADD/Replace Stock BEAN - //
	// -------------------------- //

	public void addBean(StockBean bean) {

		if (isExist(bean))
			// replace stock
			dataBase.remove(bean.ticker);

		dataBase.put(bean.ticker, bean);
		this.size = dataBase.size();

	}

	// ---------------------- //
	// - ADD Prices to bean - //
	// ---------------------- //

	// ----------------------------------- //
	// - Get linkedList of all YAPIBeans - //
	// ----------------------------------- //

	public LinkedList<StockBean> getAllBeans() {

		LinkedList<StockBean> stockList = new LinkedList<StockBean>();

		Iterator<Entry<String, StockBean>> it = dataBase.entrySet().iterator();
		while (it.hasNext()) {

			String stockTicker = it.next().getKey();
			stockList.add((StockBean) dataBase.get(stockTicker));

		}
		return stockList;
	}

	// ------------------ //
	// - Print DataBase - //
	// ------------------ //

	public String printDataBase() {

		String dataBaseString = "";
		LinkedList<StockBean> dataBaseList = getAllBeans();

		dataBaseString += "DataBase: " + dataBaseName + ", Size: " + size
				+ "\n";
		dataBaseString += "-----------------------------------------------------\n";

		for (StockBean nextBean : dataBaseList) {
			dataBaseString += nextBean.printHuman() + "\n";
		}
		return dataBaseString;
	}


	public String storeDataBase() {

		return "";
	}

}
