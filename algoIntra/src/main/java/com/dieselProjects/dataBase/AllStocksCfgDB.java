package com.dieselProjects.dataBase;

import java.io.Serializable;
import java.util.HashMap;

public class AllStocksCfgDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * This class should hold all stocks parameters lists (which are the dynamic
	 * stock configuration for buy and sell)
	 */

	public HashMap<String, SingleStockCfgDB> stocksCfgList;
}
