package com.dieselProjects.dataBase;

import java.io.Serializable;

import com.dieselProjects.configurationCluster.DynStockCfg;

public class SingleStockCfgDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DynStockCfg dynStockCfgBuy;
	public DynStockCfg dynStockCfgSell;

}
