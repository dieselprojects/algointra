package com.dieselProjects.main;


import java.util.Iterator;

import org.neuroph.core.NeuralNetwork;

import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.controllerCluster.OnlineAlgoTrade;
import com.dieselProjects.controllerCluster.SimulatedAlgoTrade;
import com.dieselProjects.controllerCluster.TWSAlgoTrade;
import com.dieselProjects.supportCluster.Screener;

public class algoTesterMain {

	public static void main(String[] args) {
	
		NeuralNetwork nn;
		
	    Configuration configuration;
		configuration = Configuration.getInstance();
		Screener screener = new Screener();
		
		System.out.println("----------------------");
		System.out.println("-- Start AlgoTrader --");
		System.out.println("----------------------\n");
        configuration.print();
        
        if (configuration.screenerCfg.useFilterAtStart == true){
            screener.screenStocks();
        }
	
        switch (configuration.tradeType){
        case OFFLINE_SIMULATED:
			System.out.println("Creating simutaltedAlgoTrade");
			SimulatedAlgoTrade simulatedTest = new SimulatedAlgoTrade();
			System.out.println("Calling simulatedTest.SimulationStarter()");
			simulatedTest.SimulationStarter();
			
			// show chart:
			Iterator<StockBean> stockItr = simulatedTest.dataBaseController.getAllStockBean().iterator();
			while(stockItr.hasNext())
				stockItr.next().generalChart.showPriceVolumeChart();
			
			break;
        case ONLINE_SIMULATED:
			System.out.println("Creating OnlineAlgoTrade");
			OnlineAlgoTrade onlineTest = new OnlineAlgoTrade();
			
			System.out.println("Calling onlineTest.SimulationStarter()");
			onlineTest.AlgoStarter();
        	break;
        case ONLINE:
			System.out.println("Creating TWSAlgoTrade");
			TWSAlgoTrade onlineTrade = new TWSAlgoTrade();
			
			System.out.println("Calling onlineTest.SimulationStarter()");
			onlineTrade.Start();
        	
        	
        	break;
        default:
        	System.err.println("Not valid tradeType selection");
        	System.err.println("Exiting...");
        	System.exit(1);
        	break;
        }
                
		System.out.println("\n----------------------");
		System.out.println("-- Ended AlgoTrader --");
		System.out.println("----------------------\n\n");
		
		//while(true);
	}

}
