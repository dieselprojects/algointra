package com.dieselProjects.algoCluster;

import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.StockStateType;

public class CreateBidsSellBasic implements CreateBidsIF {

	private static final String TAG = "Create Sell Bids Basic ";
	
	
	@Override
	public List<Bid> createBids(List<StockBean> stocks, Calendar date) {
		List<Bid> bidList = new LinkedList<Bid>();
		Iterator<StockBean> itr = stocks.iterator();
		
		while(itr.hasNext()){
			StockBean nextStock = itr.next();
			if (nextStock.currentGrade == -1.0)
				bidList.add(createBid(nextStock,date));
		}
		return bidList;
	}

	// Basic implementation sell all positions

	// TODO: need to think maybe sell algorithm will be based on
	// PositionBean and not StockBean
	private Bid createBid(StockBean stock, Calendar date) {

		// because position is in holding state
		if (stock.state != StockStateType.HOLDING) {
			System.err.println(TAG + " Error selling stock, state isn't legit "
					+ stock.state);
			System.exit(1);
		}

		if (stock.getAmountForSale() == 0) {
			System.err.println(TAG + "Error selling stock, no position to sell "
					+ stock.ticker);
			System.exit(1);
		}

		stock.state = StockStateType.PENDING_SELL;
		return new Bid(stock.ticker, stock.getPrice(),
				stock.getAmountForSale(), OPType.SELL, date,
				stock.taIndicators, stock.cdlIndicators, stock);

	}

}
