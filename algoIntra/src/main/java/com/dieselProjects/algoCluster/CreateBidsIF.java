package com.dieselProjects.algoCluster;

import java.util.Calendar;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;

public interface CreateBidsIF {

	List<Bid> createBids(List<StockBean> stocks, Calendar date);
}
