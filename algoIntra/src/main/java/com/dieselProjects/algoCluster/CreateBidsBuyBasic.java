package com.dieselProjects.algoCluster;

import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.FundsMan;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.StockStateType;

/*
 * Description:
 * Receive list with stocks to buy, return bid list
 * 
 */

public class CreateBidsBuyBasic implements CreateBidsIF {

	Configuration configuration = Configuration.getInstance();
	FundsMan funds = FundsMan.getInstance();

	// basic algorithm, each stock receives a bid up to #PARAM(4) stocks
	public List<Bid> createBids(List<StockBean> stocks, Calendar date) {
		List<Bid> bids = new LinkedList<Bid>();

		Iterator<StockBean> itr = stocks.iterator();
		while (itr.hasNext()) {
			
			StockBean nextStock = itr.next();
			
			if (nextStock.currentGrade == 1.0){
				Bid nextBid = createBid(nextStock, date);
				if (nextBid != null) {
					bids.add(nextBid);
				}
			}
			
		}

		return bids;
	}

	private Bid createBid(StockBean stock, Calendar date) {

		Bid newBid = null;
		// need to create a significant bid
		// rules for significant bid:
		// 1. amount of money invested, no less than %total funds
		// 2. leave room for other bids unless this is the last bid
		// 3. do not buy same stocks, unless stock some other rules
		// 4. set maximum open positions, currently 4

		double totalFunds = funds.getTotalFunds();
		double currentAvaliableFunds = funds.getCurrentFunds();

		// check if no new bids allowed
		if (funds.noNewBidsAllowed == true) {
			return null;
		}
	
		//if (stock.price > currentAvaliableFunds){
		//	funds.noNewBidsAllowed = false;
		//	return null;
		//}

		// if stock already bought
		if (stock.state != StockStateType.ACTIVE) {
			System.err
					.println("Trying to buy stock that already been bought in the past and it's state isn't active");
			System.exit(1);
		}

		// Bid will be generated for the stock, changing state to pending buy
		stock.state = StockStateType.PENDING_BUY;
		
		// check if this is the last bid:
		if ((currentAvaliableFunds / totalFunds) < 0.1) {
			// this is last bid, use all available funds
			newBid = new Bid(stock, funds.getAmount(stock.getPrice(), 100),
					date, OPType.BUY);
			return newBid;
		}

		// general bid
		newBid = new Bid(stock, funds.getAmount(stock.getPrice(),
				configuration.maxPerToBuy), date, OPType.BUY);
		return newBid;
	}

}
