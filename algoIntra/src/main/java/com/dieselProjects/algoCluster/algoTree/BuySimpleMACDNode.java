package com.dieselProjects.algoCluster.algoTree;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynMACDCfg;
import com.dieselProjects.indicatorCluster.MACDIndicator;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;
import com.dieselProjects.supportCluster.Types.IndicatorsType;
import com.dieselProjects.supportCluster.Types.TickValueType;

public class BuySimpleMACDNode implements AlgoNodeIF {

	private static final String TAG = "Simple MACD Node - BUY - ";

	DynMACDCfg dynMACDCfg;

	private String ticker;
	private StockBean stock;
	private Calendar date;

	public boolean isSimulated = false;

	private int size;
	private int beginIndex;
	private int endIndex;
	private int result;
	MACDIndicator macdIndicator;

	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");

	public BuySimpleMACDNode(String ticker, StockBean stock,
			DynMACDCfg dynMACDCfg, Calendar date) {
		this(ticker, stock, dynMACDCfg, date, false);
	}

	public BuySimpleMACDNode(String ticker, StockBean stock,
			DynMACDCfg dynMACDCfg, Calendar date, boolean isSimulated) {
		this.stock = stock;
		this.dynMACDCfg = dynMACDCfg;
		this.ticker = ticker;
		this.date = date;
		this.isSimulated = isSimulated;

		this.macdIndicator = new MACDIndicator(dynMACDCfg);
		this.size = stock.numOfSamples;
		this.beginIndex = 0;
		this.endIndex = size - 1;

	}

	@Override
	public NodeResultBean call() throws Exception {

		System.out.println(TAG + " for " + ticker + " started on "
				+ sdf.format(date.getTime()));

		NodeResultBean resultBean = new NodeResultBean(false, 0);
		stock.lastIterationIndication[DynBuyMainNodeType.SIMPLE_MACD_NODE
				.getMask()] = false;
		double[] prices = stock.getListAsArray(TickValueType.CLOSE);

		this.size = prices.length;
		this.beginIndex = 0;
		this.endIndex = size - 1;
		result = macdIndicator.indicate(ticker, prices, beginIndex, endIndex,
				size);

		if (result == 1) {
			stock.generalChart.addIndicator(date, IndicatorsType.BUY,
					"SIMPLE_MACD", isSimulated);
			if (isSimulated == false)
				stock.lastIterationIndication[DynBuyMainNodeType.SIMPLE_MACD_NODE
						.getMask()] = true;

			resultBean = new NodeResultBean(true, 100);
		}
		System.out.println(TAG + " for " + ticker + " done, result "
				+ resultBean.print());
		return resultBean;
	}
}
