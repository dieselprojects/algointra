package com.dieselProjects.algoCluster.algoTree;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.configurationCluster.DynMACDCfg;
import com.dieselProjects.indicatorCluster.MACDIndicator;
import com.dieselProjects.supportCluster.Types.IndicatorsType;
import com.dieselProjects.supportCluster.Types.TickValueType;

public class SellSimpleMACDNode implements AlgoNodeIF {

	private static final String TAG = "Simple MACD Node - BUY - ";

	DynMACDCfg dynMACDCfg;

	private String ticker;
	StockBean stock;
	Calendar date;

	public boolean isSimulated = false;

	private int size;
	private int beginIndex;
	private int endIndex;
	private int result;
	MACDIndicator macdIndicator;

	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");

	public SellSimpleMACDNode(String ticker, StockBean stock,
			DynMACDCfg dynMACDCfg, Calendar date) {

		this(ticker, stock, dynMACDCfg, date, false);
	}

	public SellSimpleMACDNode(String ticker, StockBean stock,
			DynMACDCfg dynMACDCfg, Calendar date, boolean isSimulated) {

		this.dynMACDCfg = dynMACDCfg;

		this.ticker = ticker;
		this.stock = stock;
		this.date = date;
		this.isSimulated = isSimulated;

		this.macdIndicator = new MACDIndicator(dynMACDCfg);
		this.size = stock.numOfSamples;
		this.beginIndex = 0;
		this.endIndex = size - 1;

	}

	@Override
	public NodeResultBean call() throws Exception {

		System.out.println(TAG + " for " + ticker + " started on "
				+ sdf.format(date.getTime()));

		NodeResultBean resultBean = new NodeResultBean(false, 0);

		double[] prices = stock.getListAsArray(TickValueType.CLOSE);

		result = macdIndicator.indicate(ticker, prices, beginIndex, endIndex,
				size);

		if (result == -1) {
			// sell:
			// set all positions for sell
			stock.generalChart.addIndicator(date, IndicatorsType.SELL, "SIMPLE_MACD",
					isSimulated);
			stock.setAllPositionForSale();
			resultBean = new NodeResultBean(true, 100);
		}

		return resultBean;

	}
}
