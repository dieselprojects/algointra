package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.dieselProjects.beans.PositionBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.configurationCluster.DynStopLostCfg;
import com.dieselProjects.supportCluster.Types.IndicatorsType;

public class StopLostNode implements AlgoNodeIF{
	
	StockBean stock;
	Calendar date;
	double currentPrice;
	List<PositionBean> positions;
	DynStopLostCfg dynStopLostCfg;

	public StopLostNode(StockBean stock, DynStopLostCfg dynStockLostCfg, Calendar date) {

		this.stock = stock;
		this.date = date;
		this.currentPrice = stock.getPrice();
		this.positions = stock.positions;
		this.dynStopLostCfg = dynStockLostCfg;
	
	}

	@Override
	public NodeResultBean call() throws Exception {
	
		// algorithm searches for position which is loosing and sell it:
		NodeResultBean resultBean = new NodeResultBean(false, 0.0);
		
		Iterator<PositionBean> posItr = positions.iterator();
		while (posItr.hasNext()){
			PositionBean position = posItr.next();
			
			if ( (currentPrice / position.price) < dynStopLostCfg.stopLostPer ){
				position.setPositionForSell();
				position.setPositionGrade(-100);
				resultBean = new NodeResultBean(true,-100);
			
				// update chart:
				stock.generalChart.addIndicator(date, IndicatorsType.STOPLOST, "SimpleStopLost", false);
			
			}
			
		}
		return resultBean;
	}
}
