package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.Types.OPType;

public class AlgorithmTree implements Callable<StockBean> {

	private static final String TAG = "AlgorithmTree";

	// main concept, this class should be able, according to some parameters to
	// implement buy or sell algorithm
	// Ideas, create a handle for some basic feature for example:
	// Phases:
	// phase one: check algorithm for trend, check if stock had failed
	// this day, check if there was a "virtual trade" (trade with no funds
	// available and if it was successful
	// phase two: list of main core algorithms according to indicators
	// phase three: last algorithm for example Hold sell or stop lost

	// each algorithm should be operate in a similar manner: start function can
	// return true or false
	// each algorithm should have a list of parameters and stock can give it to
	// them (when passing stock)
	// each stock can have it's own list of algorithms or a general list of
	// algorithms (for now, general list for all stocks)
	// each instance can only buy or sell not both

	Configuration configuration = Configuration.getInstance();

	public StockBean stock;
	OPType opType;
	Calendar date;

	public AlgorithmTree(StockBean stock, OPType opType, Calendar date) {

		this.stock = stock;
		this.opType = opType;
		this.date = date;

		switch (opType) {
		case BUY:
			stock.algoTreeBuy = new AlgorithmTreeBuy(stock.ticker, stock,
					stock.dynBuyStockCfg, date);
			break;
		case SELL:
			stock.algoTreeSell = new AlgorithmTreeSell(stock.ticker, stock,
					stock.dynSellStockCfg, date);
			break;
		default:
			System.err.println(TAG + " invalid opType for " + stock.ticker);
			System.exit(1);
			break;
		}

	}

	public AlgorithmTree(StockBean stock, OPType opType, Calendar date, boolean isSimulated) {

		this.stock = stock;
		this.opType = opType;
		this.date = date;

		switch (opType) {
		case BUY:
			stock.algoTreeBuy = new AlgorithmTreeBuy(stock.ticker, stock,
					stock.dynBuyStockCfg, date, isSimulated);
			break;
		case SELL:
			stock.algoTreeSell = new AlgorithmTreeSell(stock.ticker, stock,
					stock.dynSellStockCfg, date, isSimulated);
			break;
		default:
			System.err.println(TAG + " invalid opType for " + stock.ticker);
			System.exit(1);
			break;
		}

	}
	@Override
	public StockBean call() throws Exception {

		// call buyGenericAlgorithm from stock check if result is true, if so
		// set grade to 1
		ExecutorService exec = Executors.newCachedThreadPool();
		Future<NodeResultBean> futureResult = null;
		futureResult = exec.submit((opType == OPType.BUY) ? stock.algoTreeBuy
				: stock.algoTreeSell);
		NodeResultBean resultBean = futureResult.get();

		switch (opType) {
		case BUY:

			if (resultBean.result == true) {
				// For buy, update internal stock grade:
				//stock.currentGrade = resultBean.certainty;
				stock.currentGrade = 1.0;
			}
			break;
		case SELL:
			if (resultBean.result == true) {
				// result is true, selling all stocks
				stock.currentGrade = -1.0;
				// Currently selling all positions, however resultBean returns
				// ID list with all Position Id's for sell and position
				// certainty
				stock.updatePositionsGrades(resultBean.positionResults);
			}
			break;
		default:
			System.err.println(TAG + " opType " + opType.toString()
					+ " not valid for stock " + stock.ticker);
			System.exit(1);
			break;
		}

		return stock;
	}

}
