package com.dieselProjects.algoCluster.algoTree;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynBuyCdlPatternCfg;
import com.dieselProjects.indicatorCluster.CdlIndicators;
import com.dieselProjects.supportCluster.Types.CDLIndicators;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;
import com.dieselProjects.supportCluster.Types.IndicatorsType;

public class BuyCdlPatternNode implements AlgoNodeIF{
	
	private static final String TAG = "BuyCdlPatternNode ";
	
	DynBuyCdlPatternCfg dynCdlBuyPatternCfg;

	private String ticker;
	private StockBean stock;
	Calendar date;
	
	public boolean isSimulated = false;

	CdlIndicators cdlIndicators;
	
	
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");

	public BuyCdlPatternNode(String ticker, StockBean stock,
			DynBuyCdlPatternCfg dynCdlBuyPatternCfg, Calendar date, boolean isSimulated) {
	
		this.dynCdlBuyPatternCfg = dynCdlBuyPatternCfg;
		this.isSimulated = isSimulated;
		
		this.ticker = ticker;
		this.stock = stock;
		this.date = date;
		
		cdlIndicators = new CdlIndicators(stock, dynCdlBuyPatternCfg, date);
	}
	
	@Override
	public NodeResultBean call() throws Exception {
		System.out.println(TAG + " for " + ticker + " started on "
				+ sdf.format(date.getTime()));
		//NodeResultBean resultBean = new NodeResultBean(false, 0);
		NodeResultBean resultBean;

		// calculate array of pattern indication, some of them will be false because they will not be calculated
		int[] results = cdlIndicators.start();
		
		if (dynCdlBuyPatternCfg.requireAllPatterns == true){
			
			resultBean = new NodeResultBean(true, 100);
			
			for (int i = 0; i < CDLIndicators.getSize(); i++){
				// check if both requested patterns is true and result is 100 (buy indication)
				if (dynCdlBuyPatternCfg.patterns[i] == true){
					if (results[i] != 1){
						resultBean = new NodeResultBean(false,0);
					}
				}
			}
			
		} else {
			
			resultBean = new NodeResultBean(false, 0);
			
			for (int i = 0; i < CDLIndicators.getSize(); i++){
				// check if both requested patterns is true and result is 100 (buy indication)
				if (dynCdlBuyPatternCfg.patterns[i] == true){
					if (results[i] == 1){
						resultBean = new NodeResultBean(true,100);
					}
				}
			}
		}
		
		if (resultBean.result == true){
			if (isSimulated == false){
				stock.lastIterationIndication[DynBuyMainNodeType.CDL_PATTERN_NODE.getMask()] = true;
				stock.generalChart.addIndicator(date, IndicatorsType.BUY,"BuyPatterns",isSimulated);
			}
		}
		
		
		System.out.println(TAG + " for " + ticker + " done, result "
				+ resultBean.print());
		return resultBean;

	}
	
 
}
