package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;
import java.util.List;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynSellStockCfg;
import com.dieselProjects.configurationCluster.DynStopLostCfg;
import com.dieselProjects.configurationCluster.*;

import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AlgorithmTreeSell implements AlgoNodeIF {

	private static final String TAG = "AlgorithmTreeSell";

	// main sell nodes receives ticks and generate sell signal (phase 1)
	List<AlgoNodeIF> mainSellNodes;

	// Stop lost nodes, receives position beans and sell for stop lost no matter
	// what
	List<AlgoNodeIF> stopLostNodes;

	// support, after main generated sell signals support checks if different
	// approach is needed (basically remove sell signal) receive position
	List<AlgoNodeIF> supportSellNodes;

	public boolean isSimulated;

	public AlgorithmTreeSell(String ticker, StockBean stock,
			DynSellStockCfg dynSellStockCfg, Calendar date) {

		this(ticker, stock, dynSellStockCfg, date, false);

	}

	public AlgorithmTreeSell(String ticker, StockBean stock,
			DynSellStockCfg dynSellStockCfg, Calendar date, boolean isSimulated) {

		stopLostNodes = new LinkedList<AlgoNodeIF>();
		mainSellNodes = new LinkedList<AlgoNodeIF>();
		supportSellNodes = new LinkedList<AlgoNodeIF>();

		// stop lost algorithms:
		// ---------------------
		for (int i = 0; i < dynSellStockCfg.stopLostNodeTypes.size(); i++) {
			switch (dynSellStockCfg.stopLostNodeTypes.get(i)) {
			case STOP_LOST_NODE:

				stopLostNodes.add(new StopLostNode(stock,
						(DynStopLostCfg) dynSellStockCfg.stopLostCfgNodes
								.get(i),date));
				break;
			case STOP_LOST_ONGOING_NODE:
				
				stopLostNodes.add(new StopLostOngoingNode(stock,
						(DynStopLostOngoingCfg) dynSellStockCfg.stopLostCfgNodes
								.get(i),date));
				break;
			default:
				System.err.println(TAG
						+ " not a valid stop lost algorithm node");
				System.exit(1);
				break;
			}
		}

		// main sell nodes:
		// ----------------
		for (int i = 0; i < dynSellStockCfg.mainSellNodeTypes.size(); i++) {
			switch (dynSellStockCfg.mainSellNodeTypes.get(i)) {
			case SIMPLE_MACD_NODE:

				mainSellNodes.add(new SellSimpleMACDNode(ticker, stock,
						(DynMACDCfg) dynSellStockCfg.mainSellCfgNodes.get(i),
						date, isSimulated));
				break;
			case SIMPLE_BBANDS_NODE:
				
				mainSellNodes.add(new SellSimpleBBandsNode(ticker, stock,
						(DynBBandsCfg) dynSellStockCfg.mainSellCfgNodes.get(i),
						date, isSimulated));
				break;
			case STOPLOST_ONGOING_NODE:
				
				mainSellNodes.add(new StopLostOngoingNode(stock,
						(DynStopLostOngoingCfg) dynSellStockCfg.mainSellCfgNodes
								.get(i),date));
				break;
			case CDL_PATTERN_NODE:
				
				mainSellNodes.add(new SellCdlPatternNode(ticker,
						stock,
						(DynSellCdlPatternCfg) dynSellStockCfg.mainSellCfgNodes.get(i),
						date, isSimulated));
				break;
			default:

				System.err.println(TAG + " invalid sell main node "
						+ dynSellStockCfg.mainSellNodeTypes.get(i));
				System.exit(1);
				break;

			}
		}

		// main support nodes:
		// ----------------
		for (int i = 0; i < dynSellStockCfg.supportSellNodeTypes.size(); i++) {
			switch (dynSellStockCfg.supportSellNodeTypes.get(i)) {
			case HOLD_SELL_NODE:

				supportSellNodes.add(new HoldSellNode(ticker, stock,
						(DynHoldSellCfg) dynSellStockCfg.supportSellCfgNodes
								.get(i), date));
				break;
			default:
				System.err.println(TAG + " invalid sell support node "
						+ dynSellStockCfg.supportSellNodeTypes.get(i));
				System.exit(1);
				break;
			}
		}
	}

	@Override
	public NodeResultBean call() throws Exception {

		// Sell:
		// StopLostSubAlgo1 || StopLostSubAlgo2 || (mainSellSubAlgo1 ||
		// mainSellSubAlgo2) && supportSellSubAlgo1 && supportSellSubAlgo2
		// example:
		// StopLostLoss || ( MACDAlgo || BBandsAlgo || Pattern1 ) && holdSell

		List<NodeResultBean> stopLostResults = fork(stopLostNodes);
		List<NodeResultBean> mainSellResults = fork(mainSellNodes);
		List<NodeResultBean> supportSellResults = fork(supportSellNodes);

		NodeResultBean orOnMain = calcOr(mainSellResults);

		supportSellResults.add(orOnMain);
		NodeResultBean andOnSupport = calcAnd(supportSellResults);

		stopLostResults.add(andOnSupport);

		return calcOr(stopLostResults);

	}

	private List<NodeResultBean> fork(List<AlgoNodeIF> subAlgoList) {

		ExecutorService exec = Executors.newFixedThreadPool(subAlgoList.size());
		List<Future<NodeResultBean>> resultList = null;

		try {
			resultList = exec.invokeAll(subAlgoList);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		List<NodeResultBean> results = new LinkedList<NodeResultBean>();
		for (int i = 0; i < resultList.size(); i++) {
			Future<NodeResultBean> nextFuture = resultList.get(i);
			try {
				NodeResultBean result = nextFuture.get();
				results.add(result);
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getLocalizedMessage());
				e.printStackTrace();
				System.exit(1);
			}

		}
		exec.shutdown();

		return results;
	}

	private NodeResultBean calcOr(List<NodeResultBean> operands) {

		NodeResultBean calcResult = new NodeResultBean(false, 0);

		for (int i = 0; i < operands.size(); i++) {
			if (operands.get(i).result == true) {
				calcResult.result = true;
				calcResult.certainty = 100;
			}
		}
		return calcResult;
	}

	private NodeResultBean calcAnd(List<NodeResultBean> operands) {

		NodeResultBean calcResult = new NodeResultBean(true, 100);

		for (int i = 0; i < operands.size(); i++) {
			if (operands.get(i).result == false) {
				calcResult.result = false;
				calcResult.certainty = 0;
			}
		}
		return calcResult;
	}
}
