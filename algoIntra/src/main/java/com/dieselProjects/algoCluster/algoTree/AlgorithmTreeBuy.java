package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynBuyStockCfg;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;
import com.dieselProjects.supportCluster.Types.DynBuySupportNodeType;
import com.dieselProjects.configurationCluster.DynMACDCfg;
import com.dieselProjects.configurationCluster.DynBBandsCfg;
import com.dieselProjects.configurationCluster.*;

public class AlgorithmTreeBuy implements AlgoNodeIF {

	private static final String TAG = "AlgorithmTreeBuy";

	// Buy
	// sub algo lists:
	List<AlgoNodeIF> mainBuyNodes;
	List<AlgoNodeIF> supportBuyNodes;

	public boolean isSimulated = false;

	public AlgorithmTreeBuy(String ticker, StockBean stock,
			DynBuyStockCfg dynBuyStockCfg, Calendar date) {

		this(ticker, stock, dynBuyStockCfg, date, false);

	}

	public AlgorithmTreeBuy(String ticker, StockBean stock,
			DynBuyStockCfg dynBuyStockCfg, Calendar date, boolean isSimulated) {

		// according to configuration create both main and support algo list
		mainBuyNodes = new LinkedList<AlgoNodeIF>();
		supportBuyNodes = new LinkedList<AlgoNodeIF>();
		this.isSimulated = true;

		for (int i = 0; i < dynBuyStockCfg.mainBuyNodeTypes.size(); i++) {

			DynBuyMainNodeType nextType = dynBuyStockCfg.mainBuyNodeTypes
					.get(i);

			switch (nextType) {
			case SIMPLE_MACD_NODE:

				BuySimpleMACDNode macdNode = new BuySimpleMACDNode(ticker,
						stock, (DynMACDCfg) dynBuyStockCfg.mainCfgNodes.get(i),
						date, isSimulated);
				mainBuyNodes.add(macdNode);

				break;
			case SIMPLE_BBANDS_NODE:

				BuySimpleBBandsNode bbandsNode = new BuySimpleBBandsNode(
						ticker, stock,
						(DynBBandsCfg) dynBuyStockCfg.mainCfgNodes.get(i),
						date, isSimulated);
				mainBuyNodes.add(bbandsNode);

				break;
			case CDL_PATTERN_NODE:
				BuyCdlPatternNode cdlPatternNode = new BuyCdlPatternNode(
						ticker, stock,
						(DynBuyCdlPatternCfg) dynBuyStockCfg.mainCfgNodes
								.get(i), date, isSimulated);
				mainBuyNodes.add(cdlPatternNode);
				break;
			default:
				System.err.println(TAG + " invalid buy algorithm type for "
						+ ticker);
				System.exit(1);
				break;
			}
		}

		for (int i = 0; i < dynBuyStockCfg.supportBuyNodeTypes.size(); i++) {
			DynBuySupportNodeType nextType = dynBuyStockCfg.supportBuyNodeTypes
					.get(i);
			switch (nextType) {
			case HOLD_BUY_NODE:
				HoldBuyNode holdBuyNode = new HoldBuyNode(ticker, stock,
						(DynHoldBuyCfg) dynBuyStockCfg.supportCfgNodes.get(i),
						date, isSimulated);
				supportBuyNodes.add(holdBuyNode);
				break;
			default:
				System.err.println(TAG + "support type " + nextType
						+ " is not valid for " + ticker);
				System.exit(1);
				break;
			}

		}

	}

	@Override
	public NodeResultBean call() throws Exception {

		List<NodeResultBean> mainBuyResults = fork(mainBuyNodes);

		List<NodeResultBean> supportBuyResults = new LinkedList<NodeResultBean>();
		if (supportBuyNodes.size() > 0) {
			supportBuyResults = fork(supportBuyNodes);
		}
		// Buy:
		// (mainBuySubAlgorithm1 || mainBuySubAlgorithm2) && supportBuySubAlgo1
		// && supportBuySubAlgo2
		// example:
		// (MACDAlgo || BBandsAlgo || macdBBandsAlgo) && NoBuyAtEndsOfTradeDay

		// or for mainBuy Results:
		NodeResultBean orOnMain = calcOr(mainBuyResults);

		// and for support and result from or:
		supportBuyResults.add(orOnMain);

		return calcAnd(supportBuyResults);

	}

	private List<NodeResultBean> fork(List<AlgoNodeIF> subAlgoList) {

		ExecutorService exec = Executors.newFixedThreadPool(subAlgoList.size());
		List<Future<NodeResultBean>> resultList = null;

		try {
			resultList = exec.invokeAll(subAlgoList);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		List<NodeResultBean> results = new LinkedList<NodeResultBean>();
		for (int i = 0; i < resultList.size(); i++) {
			Future<NodeResultBean> nextFuture = resultList.get(i);
			try {
				NodeResultBean result = nextFuture.get();
				results.add(result);
			} catch (InterruptedException | ExecutionException e) {
				System.out.println(e.getLocalizedMessage());
				e.printStackTrace();
				System.exit(1);
			}

		}
		exec.shutdown();

		return results;
	}

	private NodeResultBean calcOr(List<NodeResultBean> operands) {

		NodeResultBean calcResult = new NodeResultBean(false, 0);

		for (int i = 0; i < operands.size(); i++) {
			if (operands.get(i).result == true) {
				calcResult.result = true;
				calcResult.certainty = 100;
			}
		}
		return calcResult;
	}

	private NodeResultBean calcAnd(List<NodeResultBean> operands) {

		NodeResultBean calcResult = new NodeResultBean(true, 100);

		for (int i = 0; i < operands.size(); i++) {
			if (operands.get(i).result == false) {
				calcResult.result = false;
				calcResult.certainty = 0;
			}
		}
		return calcResult;
	}

}
