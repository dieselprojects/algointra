package com.dieselProjects.algoCluster.algoTree;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.configurationCluster.DynBBandsCfg;
import com.dieselProjects.indicatorCluster.BBandsIndicator;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;
import com.dieselProjects.supportCluster.Types.IndicatorsType;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.TickValueType;

public class BuySimpleBBandsNode implements AlgoNodeIF {

	private static final String TAG = "Simple BBands Node - BUY - ";

	DynBBandsCfg dynBBandsCfg;

	private String ticker;
	private StockBean stock;
	Calendar date;

	public boolean isSimulated = false;

	private int size;
	private int beginIndex;
	private int endIndex;
	private int result;
	BBandsIndicator bbandsIndicator;

	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");

	public BuySimpleBBandsNode(String ticker, StockBean stock,
			DynBBandsCfg dynBBandsCfg, Calendar date) {

		this(ticker, stock, dynBBandsCfg, date, false);
	}

	public BuySimpleBBandsNode(String ticker, StockBean stock,
			DynBBandsCfg dynBBandsCfg, Calendar date, boolean isSimulated) {

		this.dynBBandsCfg = dynBBandsCfg;
		this.isSimulated = isSimulated;

		this.ticker = ticker;
		this.stock = stock;
		this.date = date;

		this.bbandsIndicator = new BBandsIndicator(dynBBandsCfg);
		this.size = stock.numOfSamples;
		this.beginIndex = 0;
		this.endIndex = size - 1;
	}

	@Override
	public NodeResultBean call() throws Exception {
		System.out.println(TAG + " for " + ticker + " started on "
				+ sdf.format(date.getTime()));

		NodeResultBean resultBean = new NodeResultBean(false, 0);
		stock.lastIterationIndication[DynBuyMainNodeType.SIMPLE_BBANDS_NODE
				.getMask()] = false;

		double[] prices = stock.getListAsArray(TickValueType.CLOSE);

		result = bbandsIndicator.indicate(ticker, prices, beginIndex, endIndex,
				size);

		if (result == 1) {

			stock.generalChart.addIndicator(date, IndicatorsType.BUY,
					"SIMPLE_BBANDS", isSimulated);
			if (isSimulated == false)
				stock.lastIterationIndication[DynBuyMainNodeType.SIMPLE_BBANDS_NODE
						.getMask()] = true;
			resultBean = new NodeResultBean(true, 100);
		}
		System.out.println(TAG + " for " + ticker + " done, result "
				+ resultBean.print());
		return resultBean;
	}
}
