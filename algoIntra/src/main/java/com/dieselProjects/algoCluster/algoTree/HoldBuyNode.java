package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynHoldBuyCfg;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;

/*
 * Support node for Buy tree algorithm
 */
public class HoldBuyNode implements AlgoNodeIF {

	private static final String TAG = "Hold Buy Node ";

	DynHoldBuyCfg dynHoldBuyCfg;

	private String ticker;
	private StockBean stock;
	Calendar date;
	boolean isSimulated;

	public HoldBuyNode(String ticker, StockBean stock,
			DynHoldBuyCfg dynHoldBuyCfg, Calendar date, boolean isSimulated) {
		this.dynHoldBuyCfg = dynHoldBuyCfg;

		this.ticker = ticker;
		this.stock = stock;
		this.date = date;
		this.isSimulated = isSimulated;

	}

	@Override
	public NodeResultBean call() throws Exception {

		// counts number of back to back buy indication before buying
		// stock bean will receive a new data, counting each main indication
		// with name of indication
		// hold buy should check if number of back to back buy indication exceed
		// cfg and id so, allow buy

		// check if stock has some true inside it's lastIterationIndication
		NodeResultBean resultBean = new NodeResultBean(false, 0);

		if (isSimulated == false) {
			boolean foundIndication = false;
			for (int i = 0; i < DynBuyMainNodeType.getSize(); i++) {
				if (stock.lastIterationIndication[i]) {
					foundIndication = true;
				}
			}

			if (foundIndication)
				stock.back2backBuyIndication++;
			else
				stock.back2backBuyIndication = 0;

			if (stock.back2backBuyIndication > dynHoldBuyCfg.minHoldBuy) {

				// simple configuration, only min affect the result:
				resultBean = new NodeResultBean(true, 100);
			}
		}

		return resultBean;
	}

}
