package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.PositionBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynStopLostOngoingCfg;
import com.dieselProjects.supportCluster.Types.IndicatorsType;

public class StopLostOngoingNode implements AlgoNodeIF {

	/*
	 * follows stock price, in relative to max price since buy, if stock
	 * decrease more than #% sell
	 */

	StockBean stock;
	Calendar date;
	DynStopLostOngoingCfg dynStopLostOngoingCfg;
	List<PositionBean> positions;

	public StopLostOngoingNode(StockBean stock,
			DynStopLostOngoingCfg dynStopLostOngoingCfg, Calendar date) {
		this.stock = stock;
		this.positions = stock.positions;
		this.dynStopLostOngoingCfg = dynStopLostOngoingCfg;
		this.date = date;
	}

	@Override
	public NodeResultBean call() throws Exception {

		//
		NodeResultBean resultBean = new NodeResultBean(false, 0.0);
		double currentPrice = stock.getPrice();

		Iterator<PositionBean> posItr = positions.iterator();
		while (posItr.hasNext()) {
			PositionBean position = posItr.next();

			if (position.stopLostOngoingBuyPrice < currentPrice) {
				position.stopLostOngoingBuyPrice = currentPrice;
			}

			// stop lost ongoing does not sell in loss, regular stop lost can
			// handle this

			if (currentPrice > position.price) {
				if ((currentPrice / position.stopLostOngoingBuyPrice) < dynStopLostOngoingCfg.stopLostFactor) {
					position.setPositionForSell();
					position.setPositionGrade(-100);
					resultBean = new NodeResultBean(true, -100);
					stock.generalChart.addIndicator(date,
							IndicatorsType.STOPLOSTONGOING, "StopLostOngoing", false);
				}
			}
		}

		return resultBean;
	}

}
