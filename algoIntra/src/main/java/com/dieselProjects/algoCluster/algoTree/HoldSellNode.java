package com.dieselProjects.algoCluster.algoTree;

import java.util.Calendar;
import java.util.Iterator;

import com.dieselProjects.beans.NodeResultBean;
import com.dieselProjects.beans.PositionBean;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.configurationCluster.DynHoldSellCfg;

public class HoldSellNode implements AlgoNodeIF {

	DynHoldSellCfg dynHoldSellCfg;

	String ticker;
	StockBean stock;
	Calendar date;

	/*
	 * Description, check for sell indication when price is lower than buy price
	 * and hold sell
	 */
	Double grade;

	public HoldSellNode(String ticker, StockBean stock,
			DynHoldSellCfg dynHoldSellCfg, Calendar date) {

		this.ticker = ticker;
		this.stock = stock;
		this.dynHoldSellCfg = dynHoldSellCfg;
		this.date = date;
	}

	@Override
	public NodeResultBean call() throws Exception {

		/*
		 * check all positions that are ready to be sold
		 */

		NodeResultBean resultBean = null;
		Iterator<PositionBean> posItr = stock.positions.iterator();

		while (posItr.hasNext()) {
			PositionBean position = posItr.next();
			if (position.isPositionForSell() == true) {
				if ((stock.getPrice() / position.price) < dynHoldSellCfg.holdSellUntil) {
					position.setPositionForHold();
				}
			}
		}

		// if some positions are still for sale return true 100, otherwise
		// return false 0
		if (stock.isPositionsForSale() == true) {
			resultBean = new NodeResultBean(true, -100);
		} else {
			resultBean = new NodeResultBean(false, 0);
		}

		return resultBean;
	}
}
