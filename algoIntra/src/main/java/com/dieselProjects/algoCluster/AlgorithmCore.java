package com.dieselProjects.algoCluster;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.dieselProjects.algoCluster.algoTree.AlgorithmTree;
import com.dieselProjects.beans.Bid;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.supportCluster.Types.OPType;
import com.dieselProjects.supportCluster.Types.StockStateType;

public class AlgorithmCore {

	private static final String TAG = "AlgorithmCore";
	
	// implements buy and sell and receives CreateBuyBids and CreateSellBids sub
	// algorithms

	List<Bid> bidList;

	List<StockBean> buyList;
	List<StockBean> sellList;

	CreateBidsIF createBidsBuy;
	CreateBidsIF createBidsSell;

	public AlgorithmCore(List<StockBean> stocks, CreateBidsIF createBidsBuy,
			CreateBidsIF createBidsSell) {

		this.buyList = new LinkedList<StockBean>();
		this.sellList = new LinkedList<StockBean>();
		this.bidList = new LinkedList<Bid>();
		
		this.createBidsBuy = createBidsBuy; 
		this.createBidsSell = createBidsSell; 
		

		// split stocks into consider buy stocks and consider sell stocks:
		for (StockBean stock : stocks) {
			switch (stock.state) {
			case ACTIVE:
				buyList.add(stock);
				break;
			case HOLDING:
				sellList.add(stock);
				break;
			default:
				break;
			}
		}
	}

	public List<Bid> start(Calendar date) {

		if (buyList.size() > 0)
			bidList.addAll(startOP(buyList, date, OPType.BUY));
		if (sellList.size() > 0)
			bidList.addAll(startOP(sellList, date, OPType.SELL));
		return bidList;

	}

	public List<Bid> startOP(List<StockBean> stocks, Calendar date,
			OPType opType) {

		// each stock starts it's own GenericAlgorithm
		List<AlgorithmTree> algoList = new LinkedList<AlgorithmTree>();
		for (StockBean stock : stocks) {
			algoList.add(new AlgorithmTree(stock, opType, date));
		}

		ExecutorService exec = Executors.newFixedThreadPool(stocks.size());
		List<Future<StockBean>> futureResultList = null;
		try {
			futureResultList = exec.invokeAll(algoList);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(1);
		}

		// Receive all results:
		List<StockBean> resultStocks = new LinkedList<StockBean>();
		for (Future<StockBean> nextFuture : futureResultList) {
			try {
				resultStocks.add(nextFuture.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				System.err.println(TAG + " error recieveing algorithm result");
				System.exit(1);
			}
		}
		exec.shutdown();

		// resultStocks holds all stocks with updated grade, creating bids using
		// CreateBidBuy or Create bid sell

		if (opType == OPType.BUY) {
			return createBidsBuy.createBids(resultStocks,date);
		}

		// sell:
		return createBidsSell.createBids(resultStocks,date);

	}
	
	public List<Bid> sellAll(List<StockBean> stocks,Calendar date){
		List<Bid> sellBids = new LinkedList<Bid>();
		
		for (StockBean stock : stocks){
			if (stock.state == StockStateType.HOLDING){
				stock.setAllPositionForSale();
				stock.state = StockStateType.PENDING_SELL;
				sellBids.add(new Bid(stock.ticker,
						stock.getPrice(),
						stock.getAmountForSale(), OPType.SELL,
						date, stock.taIndicators, stock.cdlIndicators,
						stock));
			}
		}
		return sellBids;
	}

}
