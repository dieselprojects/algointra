package com.dieselProjects.algoCluster;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;

import com.dieselProjects.algoCluster.algoTree.AlgorithmTree;
import com.dieselProjects.beans.StockBean;
import com.dieselProjects.supportCluster.Types.OPType;

public class AlgorithmCoreSim {

	private static final String TAG = "AlgorithmCoreSim";
	
	/*
	 * indicate all test indication on graph
	 */

	List<StockBean> stocks;

	public AlgorithmCoreSim(List<StockBean> stocks) {

		this.stocks = stocks;
	}

	public void start(Calendar date) {

		startOP(stocks, date, OPType.BUY);
		startOP(stocks, date, OPType.SELL);

	}

	public void startOP(List<StockBean> stocks, Calendar date,
			OPType opType) {

		// each stock starts it's own GenericAlgorithm
		List<AlgorithmTree> algoList = new LinkedList<AlgorithmTree>();
		for (StockBean stock : stocks) {
			AlgorithmTree at = new AlgorithmTree(stock, opType, date, true);
			algoList.add(at);
		}

		ExecutorService exec = Executors.newFixedThreadPool(stocks.size());
		List<Future<StockBean>> futureResultList = null;
		try {
			futureResultList = exec.invokeAll(algoList);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(1);
		}

		// Receive all results:
		List<StockBean> resultStocks = new LinkedList<StockBean>();
		for (Future<StockBean> nextFuture : futureResultList) {
			try {
				resultStocks.add(nextFuture.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				System.err.println(TAG + " error recieveing algorithm result");
				System.exit(1);
			}
		}
		exec.shutdown();
	}
}
