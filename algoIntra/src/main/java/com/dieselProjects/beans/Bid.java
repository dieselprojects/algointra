package com.dieselProjects.beans;

import java.util.Calendar;
import java.util.List;

import com.dieselProjects.supportCluster.Types.OPType;

public class Bid implements Cloneable {

	// StateMachine approves:
	// ----------------------
	public Boolean approvedHuman;
	public Boolean approvedBroker;

	public String ticker;
	public Double price;
	public int amount;
	public OPType opType;
	public Calendar date;
	public StockBean stock;

	public boolean[] taIndicators;
	public boolean[] cdlIndicators;

	// for sell:
	public List<Integer> positionIdList;
	public List<PositionBean> positionsForSell;

	public double margin = 0.01; // 1% margin on stock price

	public Bid(StockBean stock, int amount, Calendar date, OPType opType) {
		this.ticker = stock.ticker;
		this.price = stock.getPrice();
		this.amount = amount;
		this.date = date;
		this.opType = opType;
		this.stock = stock;

		this.taIndicators = stock.taIndicators;
		this.cdlIndicators = stock.cdlIndicators;

		if (opType == OPType.SELL)
			this.positionIdList = stock.getPositionID();

		this.approvedHuman = false;
		this.approvedBroker = false;
	}

	public Bid(String ticker, Double price, int amount, OPType opType,
			Calendar date, boolean[] taIndicators, boolean[] cdlIndicators,
			StockBean stock) {
		this.ticker = ticker;
		this.price = price;
		this.amount = amount;
		this.opType = opType;
		this.date = date;
		this.taIndicators = taIndicators;
		this.cdlIndicators = cdlIndicators;

		this.stock = stock;

		if (opType == OPType.SELL)
			this.positionIdList = stock.getPositionID();

		this.approvedHuman = false;
		this.approvedBroker = false;
	}

	public Bid(String ticker, double price, int amount, OPType opType,
			Calendar date, List<PositionBean> positionsForSell, StockBean stock) {
		this.ticker = ticker;
		this.price = price;
		this.amount = amount;
		this.opType = opType;
		this.date = date;
		this.positionsForSell = positionsForSell;
		this.stock = stock;
	}

	/*
	 * copy contractor
	 */
	public Bid(Bid bid) {

		this.ticker = bid.ticker;
		this.price = bid.price;
		this.amount = bid.amount;
		this.date = bid.date;
		this.opType = bid.opType;
		this.stock = bid.stock;

		this.taIndicators = bid.taIndicators;
		this.cdlIndicators = bid.cdlIndicators;

		if (opType == OPType.SELL)
			this.positionIdList = bid.positionIdList;

		this.approvedHuman = bid.approvedHuman;
		this.approvedBroker = bid.approvedBroker;
	}

	public String print() {
		String bidString = "Bid Summery for ticker: " + ticker + "\n";
		bidString += "Request: " + opType.toString() + ", Price: " + price
				+ ", Amount: " + amount + "\n";
		bidString += "--------------------------------------------\n";
		bidString += "General stock Information:\n";
		bidString += stock.printHuman();

		return bidString;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
