package com.dieselProjects.beans;

import java.io.Serializable;
import java.util.Calendar;

public class Tick implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String 	ticker;
	public Double 	openPrice;
	public Double 	highPrice;
	public Double 	lowPrice;
	public Double 	closePrice;
	public Integer 	volume;
	public Calendar	date;
	
	public Tick(String 	ticker,
			Double 		openPrice,
			Double 		highPrice,
			Double 		lowPrice,
			Double 		closePrice,
			Integer		volume,
			Calendar 	date
			
	){
		this.ticker		= ticker	;
		this.openPrice	= openPrice	;
		this.closePrice	= closePrice;
		this.highPrice	= highPrice	;
		this.lowPrice	= lowPrice	;
		this.volume		= volume	;
		this.date		= date		;
	}
	
	public void print(){
		System.out.println("Ticker " + ticker + " Date " + date.getTimeInMillis() + " Volume " + volume);
		System.out.println("(open|close|high|low) (" + openPrice + "|" + closePrice + "|" + highPrice + "|" + lowPrice + ")");
		System.out.println("-------------------------------------------------------------------------");
	}

	public Double open(){
		return openPrice;
	}
	public Double high(){
		return highPrice;
	}
	
	public Double low(){
		return lowPrice;
	}
	public Double close(){
		return closePrice;
	}
	public Integer volume(){
		return volume;
	}
	public Calendar date(){
		return date;
	}

	
	
	public void setOpen(Double open){
		this.openPrice = open;
	}
	
	public void setHigh(Double high){
		this.highPrice = high;
	}
	
	public void setLow(Double low){
		this.lowPrice = low;
	}
	public void setClose(Double close){
		this.closePrice = close;
	}
	public void setVolume(Integer volume){
		this.volume = volume;
	}
	public void setDate(Calendar date){
		this.date = date;
	}
	
	
	
}
