package com.dieselProjects.beans;

import java.util.LinkedList;
import java.util.List;

public class NodeResultBean {

	public boolean result;
	public double certainty; // certainty in percents (0 - 100)
	
	// for sell:
	public List<PositionResultBean> positionResults;
	
	public NodeResultBean(boolean result, double certainty){
		this.result = result;
		this.certainty = certainty;
		this.positionResults = new LinkedList<>();
	}
	
	public String print(){
		return result + " " + certainty + " ";
	}
	
}
