package com.dieselProjects.beans;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.LinkedList;

public class OneStockDB implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String ticker = "";
	public String date = ""; // date format dd_MM_yyyy
	public String type = ""; // 1M or 5M
	public LinkedList<Tick> ticks = new LinkedList<Tick>();

	public OneStockDB(String ticker, String date, String type,
			LinkedList<Tick> ticks) {
		this.ticker = ticker;
		this.date = date;
		this.type = type;
		this.ticks = ticks;
	}

	public void store() {

		// Main DataBase (d)
		// -- ticker (d)
		// ---- Type (d)
		// ------date.db (f)

		// check if ticker folder exists:

		File mainDBDir = new File("/disk/workarea/algoTrader");
		File tickerDir = new File(mainDBDir + "/" + ticker + "/" + type);

		if (!tickerDir.exists()) {
			// create ticker directory
			tickerDir.mkdirs();
		}
		File tickerDataBase = new File(tickerDir + "/" + date + ".db");
		try {
			OutputStream dataBaseFile = new FileOutputStream(tickerDataBase);
			OutputStream buffer = new BufferedOutputStream(dataBaseFile);
			ObjectOutput oOutput = new ObjectOutputStream(buffer);
			oOutput.writeObject(this);
			oOutput.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
