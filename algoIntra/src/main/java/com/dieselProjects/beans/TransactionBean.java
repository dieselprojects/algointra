package com.dieselProjects.beans;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.supportCluster.Types.OPType;

public class TransactionBean {
	String ticker;
	OPType transactionOPType;
	Double transactionPrice;
	Integer transactionAmount;
	Calendar transactionDate;

	LinkedList<String> transactionIndicators;
	LinkedList<Double> transactionIndicatorsGrades;
	Double transactionTotalGrade;

	// for sell transaction only, TransactionBean will calculate profit
	// according
	Double transactionBuyPrice;
	Integer transactionBuyAmount;
	String transactionProfit;
	String transactionProfitPercent;
	boolean profit;

	Configuration configuration = Configuration.getInstance();

	// Buy constructor
	public TransactionBean(String ticker, OPType transactionOPType,
			Double transactionPrice, Integer transactionAmount,
			Calendar transactionDate, Double transactionTotalGrade) {
		this.ticker = ticker;
		this.transactionOPType = transactionOPType;
		this.transactionPrice = transactionPrice;
		this.transactionAmount = transactionAmount;
		this.transactionDate = transactionDate;
		this.transactionTotalGrade = transactionTotalGrade;

	}

	// Sell constructor
	public TransactionBean(String ticker, OPType transactionOPType,
			Double transactionPrice, Integer transactionAmount,
			Calendar transactionDate, Double transactionTotalGrade,
			Double transactionBuyPrice, Integer transactionBuyAmount) {
		this.ticker = ticker;
		this.transactionOPType = transactionOPType;
		this.transactionPrice = transactionPrice;
		this.transactionAmount = transactionAmount;
		this.transactionDate = transactionDate;
		this.transactionTotalGrade = transactionTotalGrade;
		this.transactionBuyPrice = transactionBuyPrice;
		this.transactionBuyAmount = transactionBuyAmount;

		// calculate profit:
		Double profitPercent = (transactionPrice / transactionBuyPrice - 1.0) * 100.0;
		transactionProfitPercent = (profitPercent > 0.0) ? new DecimalFormat(
				".##").format(profitPercent) + "%" : "("
				+ new DecimalFormat(".##").format(profitPercent) + "%)";
		Double profitDollar = (transactionPrice - transactionBuyPrice)
				* transactionAmount;
		transactionProfit = (profitDollar > 0) ? new DecimalFormat(".##")
				.format(profitDollar) + "$" : "("
				+ new DecimalFormat(".##").format(profitDollar) + "$)";
		profit = (profitDollar > 0);
	}

	public String print() {
		String returnString = "";

		// parse date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = sdf.format(transactionDate.getTime());

		returnString += "-------------------------------------------------------------\n";

		returnString += date + "# " + ticker + ", " + transactionOPType
				+ ", Price: " + transactionPrice + ", Amount: "
				+ transactionAmount + "\n";
		returnString += (transactionOPType == OPType.SELL) ? ("Profit: "
				+ transactionProfitPercent + ", " + transactionProfit) + "\n\n" : ""
				+ "\n\n";

		// TODO: Add indicators

		return returnString;
	}

}
