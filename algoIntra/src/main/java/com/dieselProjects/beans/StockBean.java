package com.dieselProjects.beans;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.dieselProjects.algoCluster.algoTree.AlgorithmTreeBuy;
import com.dieselProjects.algoCluster.algoTree.AlgorithmTreeSell;
import com.dieselProjects.configurationCluster.Configuration;
import com.dieselProjects.configurationCluster.DynBuyStockCfg;
import com.dieselProjects.configurationCluster.DynSellStockCfg;
import com.dieselProjects.gui.GeneralChart;
import com.dieselProjects.supportCluster.Types.DynBuyMainNodeType;
import com.dieselProjects.supportCluster.Types.StockStateType;
import com.dieselProjects.supportCluster.Types.TickValueType;

public class StockBean {

	Configuration configuration = Configuration.getInstance();
	// Default configuration for buy/sell

	public DynBuyStockCfg dynBuyStockCfg = new DynBuyStockCfg(false);
	public DynSellStockCfg dynSellStockCfg = new DynSellStockCfg(false);
	
	public AlgorithmTreeBuy algoTreeBuy;
	public AlgorithmTreeSell algoTreeSell;

	// ----------------------- //
	// - Main bean variables - //
	// ----------------------- //

	public String ticker;
	public StockStateType state;

	public Calendar lastUpdated;
	public Double currentPrice;
	public Double currentGrade;
	
	public Integer numOfSamples;

	public LinkedList<Tick> ticks;
	public LinkedList<PositionBean> positions;

	// holds back to back buy indication, resets when indicators are no longer back to back
	public Integer back2backBuyIndication = 0;
	public boolean[] lastIterationIndication = new boolean[DynBuyMainNodeType.getSize()];
	
	
	public boolean[] taIndicators;
	public boolean[] cdlIndicators;

	// Bean Additional Variables:
	public GeneralChart generalChart;
	public HashMap<String, Boolean> indicatorsTable = new HashMap<String, Boolean>();

	private int uniqueId = 0;

	// ---------------- //
	// - Constructors - //
	// ---------------- //

	public StockBean(String ticker) {
		this.ticker = ticker;
		this.state = StockStateType.ACTIVE;

		this.lastUpdated = Calendar.getInstance();
		this.currentPrice = 0.0;
		this.currentGrade = 0.0;
		this.numOfSamples = 0;

		this.ticks = new LinkedList<Tick>();
		this.positions = new LinkedList<PositionBean>();

		generalChart = new GeneralChart(this);
	}

	public StockBean(Tick tick) {

		this.ticker = tick.ticker;
		this.state = StockStateType.ACTIVE;

		this.currentGrade = 0.0;
		this.numOfSamples = 0;

		this.ticks = new LinkedList<Tick>();
		this.addTick(tick);

		this.positions = new LinkedList<PositionBean>();

		generalChart = new GeneralChart(this);
	}

	// called by buyBidAlgorithm
	public void stageStockForBuy() {
		this.state = StockStateType.PENDING_BUY;
	}

	public void stockBuy(Bid bid){
		
		this.state = StockStateType.HOLDING;
		
		PositionBean newPosition = new PositionBean(uniqueId++, bid.price, bid.amount,
				bid.date, bid.taIndicators, bid.cdlIndicators);
		positions.add(newPosition);
		
		generalChart.addTransaction(bid, bid.date);
	}
	
	
	public void stageStockForSell(){
		// Currently all positions are going to be sold
		this.state = StockStateType.PENDING_SELL;
	
		// setting all positions for sell
		Iterator<PositionBean> posItr = positions.iterator();
		while (posItr.hasNext()){
			posItr.next().setPositionForSell();
		}
	}
	
	
	public void stockSell(Bid bid){
		this.state = StockStateType.ACTIVE;
		
		// Currently all positions are sold
		Iterator<PositionBean> posItr = positions.iterator();
		while(posItr.hasNext()){
			positions.remove(posItr.next());
		}
		
		generalChart.addTransaction(bid, bid.date);
		
	}
	

	// Set grade:
	// ----------

	/*
	 * setGrade inputs: grade and date description: used for new Algorithm and
	 * generic algorithm
	 */

	public void setGrade(double grade, Calendar date) {
		this.currentGrade = grade;
		this.lastUpdated = date;

		// Currently when selling selling all positions
		if (grade == -1.0) {
			this.setAllPositionForSale();
		}
	}

	// Tick handling:
	// --------------

	/*
	 * addTick:
	 */

	public void addTick(Tick tick) {

		if (this.numOfSamples >= configuration.maximumNumOfSamples) {
			ticks.removeFirst();
			this.numOfSamples--;
		}

		ticks.add(tick);
		currentPrice = tick.close();
		lastUpdated = tick.date;

		this.numOfSamples++;

	}

	public double[] getListAsArray(TickValueType tickValueType) {

		double[] returnedList = new double[ticks.size()];

		for (int i = 0; i < ticks.size(); i++) {
			switch (tickValueType) {
			case OPEN:
				returnedList[i] = ticks.get(i).open();
				break;
			case HIGH:
				returnedList[i] = ticks.get(i).high();
				break;
			case LOW:
				returnedList[i] = ticks.get(i).low();
				break;
			case CLOSE:
				returnedList[i] = ticks.get(i).close();
				break;
			default:
				System.err.println("Invalid tick type");
				System.exit(1);
			}
		}

		return returnedList;
	}

	// Position handling:
	// ------------------

	/*
	 * addPosition Input: PositionBean to be added to Position list
	 */

	public void addPosition(PositionBean positionBean) {
		positions.add(positionBean);
	}

	/*
	 * getPositionID returns linkedList with all Position ID's
	 */
	public List<Integer> getPositionID() {
		List<Integer> positionIDList = new LinkedList<Integer>();
		Iterator<PositionBean> iterator = positions.iterator();
		while (iterator.hasNext()) {
			PositionBean position = iterator.next();
			positionIDList.add(position.positionId);
		}
		return positionIDList;
	}

	/*
	 * removePositions Input: position Id to be removed from list
	 */
	public void removePositions(List<Integer> idList) {
		Iterator<Integer> iterator = idList.iterator();
		while (iterator.hasNext()) {
			Integer id = iterator.next();
			for (int i = 0; i < positions.size(); i++) {
				if (id == positions.get(i).positionId) {
					positions.remove(i);
				}
			}
		}
	}

	/*
	 * setAllPositionForSale iterate on all position and set them all for sale
	 */
	public void setAllPositionForSale() {
		Iterator<PositionBean> itr = positions.iterator();
		while (itr.hasNext()) {
			itr.next().setPositionForSell();
		}
	}

	/*
	 * getPositionsForSale return Position
	 */
	public List<PositionBean> getPositionsForSale() {
		List<PositionBean> sellPositions = new LinkedList<PositionBean>();
		Iterator<PositionBean> iterator = positions.iterator();
		while (iterator.hasNext()) {
			PositionBean position = iterator.next();
			if (position.isPositionForSell() == true)
				sellPositions.add(position);
		}
		return sellPositions;
	}

	/*
	 * getAmountPositionsForSale returns amount of stocks that are for sale
	 */
	public Integer getAmountForSale() {
		Integer sellPositionsAmount = 0;
		Iterator<PositionBean> iterator = positions.iterator();
		while (iterator.hasNext()) {
			PositionBean position = iterator.next();
			if (position.isPositionForSell() == true)
				sellPositionsAmount += position.amount;
		}
		return sellPositionsAmount;
	}

	/*
	 * getSellPositionID Returns all position ID's that are set for sale
	 */
	public List<Integer> getSellPositionIDs() {
		List<Integer> positionIDList = new LinkedList<Integer>();
		Iterator<PositionBean> iterator = positions.iterator();
		while (iterator.hasNext()) {
			PositionBean position = iterator.next();
			if (position.isPositionForSell() == true)
				positionIDList.add(position.positionId);
		}
		return positionIDList;
	}

	public void updatePositionsGrades(List<PositionResultBean> positionResults){
	
		// Currently setting all position for sell:
		Iterator<PositionBean> posItr = positions.iterator();
		while(posItr.hasNext()){
			PositionBean position = posItr.next();
			position.setPositionGrade(-100);
			position.setPositionForSell();
		}
	}
	
	public boolean isPositionsForSale(){
		
		boolean isPositionForSale = false;
		
		Iterator<PositionBean> posItr = positions.iterator();
		while (posItr.hasNext()) {
			if (posItr.next().isPositionForSell() == true){
				isPositionForSale = true;
			}
		}
		
		return isPositionForSale;
	}
	
	
	// -------------------------- //
	// - Print for store & load - //
	// -------------------------- //

	public Double getPrice() {
		return currentPrice;
	}

	// Human readable print:
	// ---------------------
	public String printHuman() {
		return "";
	}

}
