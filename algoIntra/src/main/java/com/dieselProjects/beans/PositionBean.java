package com.dieselProjects.beans;

import java.util.Calendar;

import com.dieselProjects.supportCluster.Types.PositionStateType;

public class PositionBean {

	public int positionId;
	public double price;
	public int amount;
	public Calendar date;
	public boolean[] taIndicators;
	public boolean[] cdlIndicators;
	private PositionStateType positionState = PositionStateType.HOLDING;
	private double positionGrade;

	// stopLostOngoingNode
	public Double stopLostOngoingBuyPrice = 0.0;
	
	public PositionBean(int positionId, double price, int amount, Calendar date) {
		this(positionId,price,amount,date,null,null);
	}

	public PositionBean(int positionId, double price, int amount,
			Calendar date, boolean[] taIndicators, boolean[] cdlIndicators) {
		this.positionId = positionId;
		this.price = price;
		this.amount = amount;
		this.date = date;
		this.taIndicators = taIndicators;
		this.cdlIndicators = cdlIndicators;
		
		this.stopLostOngoingBuyPrice = price;
		
	}
	
	public void setPositionForSell(){
		positionState = PositionStateType.PENDING_SELL;
	}
	
	public boolean isPositionForSell(){
		return (positionState == PositionStateType.PENDING_SELL) ? true : false;
	}
	
	public void setPositionForHold(){
		positionState = PositionStateType.HOLDING;
	}
	
	public void setPositionGrade(double grade){
		this.positionGrade = grade;
	}
	
	public double getPositionGrade(){
		return this.positionGrade;
	}
	
}