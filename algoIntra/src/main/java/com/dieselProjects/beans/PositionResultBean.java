package com.dieselProjects.beans;

public class PositionResultBean {

	public int id;
	public boolean result;
	public double certainty;
	
	public PositionResultBean(int id, boolean result, double certainty){
		this.id = id;
		this.result = result;
		this.certainty = certainty;
	}
	
}
